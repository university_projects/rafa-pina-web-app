<?php
    /////////////////////////////////////////
    /// Gets the schedules of the events which
	/// will be happend in a givin date.
	/// This function is called when the daily
	/// calendar of booking calendar page is being
	/// filled.
	/// If there's 'event_id' field in $_SESSION,
	/// it means that the owner of that event is
	/// updating it's schedelues, so, we must not
	/// get them.
    /////////////////////////////////////////

	require 'connect-db.inc';
	session_start();

	$db = connect_db();
	$date = $_GET ['year'].'-'.$_GET ['month'].'-'.$_GET ['date'];
	if (isset($_SESSION ['booking_calendar_action']))
		$event_id = $_SESSION ['event_id'];
	else
		$event_id = -1;

	$stmt = $db->prepare('SELECT event_schedule.start_hour - 7, event_schedule.end_hour - 7, event_description.name,
						  event_description.id FROM event_schedule
						  INNER JOIN event_description
					   	  ON event_schedule.event_description_id = event_description.id
						  WHERE event_schedule.start_date <= ? AND event_schedule.end_date >= ? AND event_description.state >= 0
						  AND event_description.state <= 3 AND event_description.id != ?');
	$stmt->bind_param('ssi', $date, $date, $event_id);
	$stmt->execute();
	$stmt->bind_result($start_hour, $end_hour, $event_name, $event_id);

	$booked_schedules = new stdClass();

	/// Schedules of the events
	$events = array();
	while ($stmt->fetch())
		array_push($events, array('name' => $event_name, 'start_hour' => $start_hour, 'end_hour' => $end_hour, 'id' => $event_id));
	$stmt->close();
	$booked_schedules->events = $events;

	/// Selected schedules
	$stmt = $db->prepare('SELECT start_hour - 7, end_hour - 7 FROM selected_schedule
						  WHERE start_date <= ? AND end_date >= ?');
	$stmt->bind_param('ss', $date, $date);
	$stmt->execute();
	$stmt->bind_result($start_hour, $end_hour);

	$selected_schedules = array();
	while ($stmt->fetch())
	{
		array_push($selected_schedules, array('start_hour' => $start_hour, 'end_hour' => $end_hour));
	}

	$booked_schedules->selected_schedules = $selected_schedules;

	echo json_encode($booked_schedules);
	$stmt->close();
	$db->close();
?>
