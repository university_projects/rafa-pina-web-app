/////////////////////////////////////////
/// Draws a pie chart on the canvas based on
/// the data passed in slices.
///
/// Slices is an array of objects with name,
/// value and color.
/////////////////////////////////////////
function pie_chart(canvas, slices)
{
	if (typeof(canvas) == "string")
		canvas = document.getElementById(canvas);

	var canvas_context = canvas.getContext("2d");
	canvas_context.clearRect(0, 0, canvas.width, canvas.height);

	var pie_total = 0;
	for (var i = 0; i < slices.length; i++)
		pie_total += slices [i].value;

	const radians_by_value = 360 / pie_total * 0.017453293;
	const pie_radius = canvas.height * 0.4;
	const pie_center = {x: canvas.width * 0.75, y: canvas.height * 0.5};
	var start_angle = 0;
	var color_code = {x: canvas.width * 0.1, y: canvas.height * 0.3};

	for (var i = 0; i < slices.length; i++)
	{
		const slice = slices [i];

		/// Draw pie slice
		var angle = slice.value * radians_by_value;
		canvas_context.fillStyle = slice.color;
		canvas_context.beginPath();
		canvas_context.moveTo(pie_center.x, pie_center.y);
		canvas_context.arc(pie_center.x, pie_center.y, pie_radius, start_angle, start_angle + angle);
		canvas_context.closePath();
		canvas_context.fill();

		start_angle += angle;

		/// Draw color code
		canvas_context.fillStyle = slice.color;
		canvas_context.fillRect(color_code.x, color_code.y, 20, 20);

		canvas_context.fillStyle = "#333";
		canvas_context.font = "15px Relaway";
		canvas_context.fillText(slice.name + " (" + slice.value + ") " + (slice.value * 100 / pie_total).toPrecision(4) + "%",
							 	color_code.x + 25, color_code.y + 17);
		color_code.y += 25;
	}
}

/////////////////////////////////////////
/// Draws a bar chart in a given canvas
/// based on the data in bars.
///
/// bars is an array of objects with name,
/// value and color.
/////////////////////////////////////////
function bar_chart(canvas, bars, max_value)
{
	if (typeof(canvas) == "string")
		canvas = document.getElementById(canvas);

	var canvas_context = canvas.getContext("2d");
	canvas_context.clearRect(0, 0, canvas.width, canvas.height);

	/// Draw axes
	draw_line(canvas_context, {x: canvas.width * 0.15, y: canvas.height * 0.05}, {x: canvas.width * 0.15, y: canvas.height * 0.8});
	draw_line(canvas_context, {x: canvas.width * 0.1, y: canvas.height * 0.75}, {x: canvas.width * 0.9, y: canvas.height * 0.75});

	const pixels_per_value = canvas.height * 0.7 / max_value;
	for (var i = max_value / 10; i <= max_value; i += max_value / 10)
	{
		const y = canvas.height * 0.75 - i * pixels_per_value;
		draw_text(canvas_context, i, canvas.width * 0.1, y  + 7.5);
		draw_line(canvas_context, {x: canvas.width * 0.15, y: y}, {x: canvas.width * 0.9, y: y});
	}

	/// Draw bars and code color
	const offset_x = 25;
	const bar_width = 45;
	var x = canvas.width * 0.15 + offset_x;
	const y = canvas.height * 0.75;
	var code_color_x = canvas.width * 0.1;
	const code_color_y = canvas.height * 0.9;

	for (var i = 0; i < bars.length; i++)
	{
		const bar = bars [i];
		const bar_height = bar.value * pixels_per_value;

		canvas_context.fillStyle = bar.color;
		canvas_context.fillRect(x, y - bar_height, bar_width, bar_height);

		draw_text(canvas_context, bar.value, x + bar.value.toString().length * 15, y - bar_height - 5);

		x += bar_width + offset_x;

		/// Draw code color
		canvas_context.fillStyle = bar.color;
		canvas_context.fillRect(code_color_x, code_color_y, 20, 20);
		draw_text(canvas_context, bar.name, code_color_x + 25, code_color_y + 15);
		code_color_x += 30 + bar.name.length * 12;
	}
}

/////////////////////////////////////////
/// Draws a line in a canvas context from
/// a given point to a given point.
/////////////////////////////////////////
function draw_line(canvas_context, from, to, stroke_style = "#333", line_width = 1)
{
	canvas_context.beginPath();
	canvas_context.strokeStyle = stroke_style;
	canvas_context.lineWidth = line_width;
	canvas_context.moveTo(from.x, from.y);
	canvas_context.lineTo(to.x, to.y);
	canvas_context.stroke();
}

/////////////////////////////////////////
/// Draws text over a canvas context in
/// a given point
/////////////////////////////////////////
function draw_text(canvas_context, text, x, y, fill_style = "#333", font = "15px Relaway")
{
	canvas_context.fillStyle = fill_style;
	canvas_context.font = font;
	canvas_context.fillText(text, x, y);
}
