var user =
{
	name: "",
	type: "",

	/////////////////////////////////////////
	/// Emitted once the user data has been
	/// gotten from server. When the user
	/// is loaded, its properties containt
	/// valid data.
	/////////////////////////////////////////
	onload: new Signal(),

	/////////////////////////////////////////
	/// Loads the information about the user
	/// stored in the server.
	/////////////////////////////////////////
	load: function()
	{
		var httpxml = new XMLHttpRequest();
		httpxml.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var user_info = JSON.parse(this.responseText);
			user.set_name(user_info.name, user_info.is_admin);
			user.onload.emit();
		};
		httpxml.open("GET", "php/get-user-info.php");
		httpxml.send();
	},

	/////////////////////////////////////////
	/// Sets the name of the user and according
	/// to it, sets the user's type.
	///
	/// Changing it changes the displayed name
	/// on the top bar.
	/////////////////////////////////////////
	set_name: function(name, is_admin)
	{
		this.name = name;

		if (is_admin)
		{
			this.type = "admin";
            this.on_login.emit();
		}
		else if (this.name == "Invitado")
		{
			this.on_logout.emit();
			this.type = "invited";
		}
		else
		{
			this.type = "normal";
            this.on_login.emit();
		}

		nav_bar.set_user_name(this.name);
		nav_bar.enable_login((this.type == "invited") ? true : false);
	},

	/////////////////////////////////////////
	/// This signal is emitted after the user
	/// logs in and after the user's type
	/// is changed
	/////////////////////////////////////////
	on_login: new Signal(),

	/////////////////////////////////////////
	/// This signal is emitted after the user
	/// logs out and before the user's type is
	/// changed.
	/////////////////////////////////////////
	on_logout: new Signal()
};
