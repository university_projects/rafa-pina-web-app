<?php
	/////////////////////////////////////////
	/// Gets all the notifications of the
	/// current user. Stores the notifications'
	/// id in $_SESSION ['notifications_id'].
	/////////////////////////////////////////
	require 'connect-db.inc';
	require 'get-activities.inc';

	session_start();
	if (! isset($_SESSION ['user_id']))
		return;

	$user_id = $_SESSION ['user_id'];
	$db = connect_db();
	$notifications = get_activities($db, $user_id, $_SESSION ['user_is_admin'], true);
	$db->close();

	$_SESSION ['notifications_id'] = array();
	foreach ($notifications as $notification)
		array_push($_SESSION ['notifications_id'], $notification->id);

	echo json_encode($notifications);
?>
