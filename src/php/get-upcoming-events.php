<?php
	/////////////////////////////////////////
	// Get a maximum of five upcoming events into an
	// array. These are the properties of the objects:
	// + id
	// + name
	// + image
	// + schedules (array)
	//
	// Each object in schedules has the following
	// properties:
	// + start_date
	// + end_date
	// + start_hour
	// + end_hour
	/////////////////////////////////////////
	require 'connect-db.inc';

	$db = connect_db();
	$stmt = $db->prepare("SELECT event_description.id, event_description.name, event_description.image,
		   						 event_schedule.start_date, event_schedule.end_date, event_schedule.start_hour, event_schedule.end_hour
						  FROM event_description
						  JOIN event_schedule ON event_schedule.event_description_id = event_description.id
						  WHERE event_description.state = 1
						  ORDER BY event_schedule.start_date;");
	$stmt->execute();
	$stmt->bind_result($event_id, $event_name, $event_image, $event_start_date, $event_end_date, $event_start_hour, $event_end_hour);

	$events = array();
	while ($stmt->fetch())
	{
		/// Check if the event has already been added
		$already_added = false;
		foreach ($events as $event)
		{
			// If so, then add the schedule to it
			if ($event->id == $event_id)
			{
				array_push($event->schedules, array('start_date' => $event_start_date, 'end_date' => $event_end_date,
													'start_hour' => $event_start_hour, 'end_hour' => $event_end_hour));
				$already_added = true;
				break;
			}
		}

		// New event and less than 5 upcoming events in the array, add it
		if (! $already_added && count($events) < 5)
		{
			$event = new stdClass();
			$event->id = $event_id;
			$event->name = $event_name;
			$event->image = $event_image;
			$event->schedules = array();
			array_push($events, $event);
			array_push($event->schedules, array('start_date' => $event_start_date, 'end_date' => $event_end_date,
										 	    'start_hour' => $event_start_hour, 'end_hour' => $event_end_hour));
		}
	}

	$stmt->close();
	$db->close();
	echo json_encode($events);
?>
