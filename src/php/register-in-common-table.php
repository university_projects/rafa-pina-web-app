<?php
	require 'connect-db.inc';

	session_start();
	if (! $_SESSION ['user_is_admin'])
	{
		echo ">:(";
		return;
	}

	$registration_type = $_GET ['registration_type'];
	$name = $_GET ['name'];
	$db = connect_db();

	/// Verify that the value does not exist
	if ($registration_type == 'nick')
		$stmt = $db->prepare('SELECT COUNT(*) FROM nick WHERE name = ?');
	else if ($registration_type == 'public-type')
		$stmt = $db->prepare('SELECT COUNT(*) FROM public_type WHERE name = ?');
	else if ($registration_type == 'job-title')
		$stmt = $db->prepare('SELECT COUNT(*) FROM job_title WHERE name = ?');

	$stmt->bind_param('s', $name);
	$stmt->execute();
	$stmt->bind_result($already_exist);
	$stmt->fetch();
	$stmt->close();

	if ($already_exist)
	{
		echo 'already exist';
		$db->close();
		return;
	}

	/// Does not exist?
	if ($registration_type == 'nick')
		$stmt = $db->prepare('INSERT INTO nick VALUES (NULL, ?)');
	else if ($registration_type == 'public-type')
		$stmt = $db->prepare('INSERT INTO public_type VALUES (NULL, ?)');
	else if ($registration_type == 'job-title')
		$stmt = $db->prepare('INSERT INTO job_title VALUES (NULL, ?)');

	$stmt->bind_param('s', $name);
	$stmt->execute();
	$stmt->close();
	$db->close();
	echo 'success';
?>
