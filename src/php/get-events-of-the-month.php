<?php
    /////////////////////////////////////////
    /////////////////////////////////////////
    require 'connect-db.inc';
    require 'get-event-schedules.inc';

    $month = $_GET ['month'];
    $year = date('Y');
    $start_date = $year.'-'.$month.'-01';
    $end_date = $year.'-'.$month.'-'.date('t');

    $db = connect_db();

    /// Get event's id and name
    $stmt = $db->prepare('SELECT event_description.id, event_description.name FROM event_description
                          JOIN event_schedule ON event_description.id = event_schedule.event_description_id
                          WHERE event_description.state >= 1 AND event_description.state <= 3 AND
                                event_schedule.end_date >= ? AND event_schedule.start_date <= ?
                          GROUP BY event_description.id
                          ORDER BY event_schedule.start_date');
    $stmt->bind_param('ss', $start_date, $end_date);
    $stmt->execute();
    $stmt->bind_result($event_id, $event_name);

	/// Store them in an array
    $events_of_the_month = array();
    for ($i = 0; $stmt->fetch(); $i++)
    {
        $events_of_the_month [$i] = new stdClass();
        $events_of_the_month [$i]->id = $event_id;
        $events_of_the_month [$i]->name = $event_name;
        $events_of_the_month [$i]->schedules = array();
    }

    $stmt->close();

    /// Get the schedules of the events
    for ($i = 0; $i < count($events_of_the_month); $i++)
        $events_of_the_month [$i]->schedules = get_event_schedules($db, $events_of_the_month [$i]->id);

    $db->close();
    echo json_encode($events_of_the_month);
?>
