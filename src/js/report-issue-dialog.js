/// NOTE: This file must be included after user.js

var report_issue_dialog =
{
	/////////////////////////////////////////
	/// Shows the report issue dialog. Gives
	/// focus to issue-name input.
	/////////////////////////////////////////
	show: function()
	{
		if (user.type == "invited")
		{
			show_toast("Necesitas iniciar sesión");
			return;
		}

		display("report-issue-dialog-background");
		document.forms ["report-issue-form"] ["issue-name"].focus();
	},

	/////////////////////////////////////////
	/// Sends the report-issue-form data to
	/// the server to report the issue
	/////////////////////////////////////////
	report_issue: function()
	{
		var issue_form = document.forms ["report-issue-form"];
		var issue = new Object();
		issue.type = 1; /// Bug report
		issue.name = issue_form ["issue-name"].value;
		issue.description = issue_form ["issue-description"].value;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText != "ok")
				show_toast(">:(");
			else
				show_toast("Error reportado, gracias", BlueToast);

			issue_form.reset();
			close_dialog("report-issue");
		};
		xmlhttp.open("POST", "php/report-issue.php");
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
		xmlhttp.send("issue=" + JSON.stringify(issue));
	}
};

user.on_login.connect(function() { display("report-issue-section"); });
user.on_logout.connect(function() { conceal("report-issue-section"); });
