<?php
	/////////////////////////////////////////
	/// Verifies that $_GET ['schedules'] are
	/// not booked nor selected by others.
	///
	/// If the schedules are free, store them
	/// in selected_schedule table and $_SESSION
	///
	/// If the schedules are not free, returns
	/// an object with the following structure:
	/// + repited_schedules : array, holds the
	/// repited schedules.
	/////////////////////////////////////////
	require 'connect-db.inc';
	$selected_schedules = json_decode($_GET ['schedules']);

	$db = connect_db();
	$response = new stdClass();
	$response->repited_schedules = array();

	/// Verifies that schedules have not been selected yet
	$stmt = $db->prepare('SELECT COUNT(*) FROM selected_schedule
						  WHERE start_date <= ? AND end_date >= ? AND start_hour <= ? AND end_hour >= ?');

	foreach ($selected_schedules as $selected_schedule)
	{
		$stmt->bind_param('ssii', $selected_schedule->end_date, $selected_schedule->start_date, $selected_schedule->end_hour,
								  $selected_schedule->start_hour);
	    $stmt->execute();
		$stmt->bind_result($repited);
		$stmt->fetch();

		if ($repited)
			array_push($response->repited_schedules, $selected_schedule);
	}

	$stmt->close();

	/// Verifies that schedules have not been booked yet
	$stmt = $db->prepare('SELECT COUNT(*) FROM event_schedule
						  WHERE start_date <= ? AND end_date >= ? AND start_hour <= ? AND end_hour >= ?');
    foreach ($selected_schedules as $selected_schedule)
  	{
  		$stmt->bind_param('ssii', $selected_schedule->end_date, $selected_schedule->start_date, $selected_schedule->end_hour,
  							  	  $selected_schedule->start_hour);
  		$stmt->execute();
  		$stmt->bind_result($booked);
  		$stmt->fetch();

  		if ($booked)
  			array_push($response->repited_schedules, $selected_schedule);
  	}

	if (count($response->repited_schedules))
	{
		$stmt->close();
		$db->close();
		echo json_encode($response);
		return;
	}

	$stmt->close();

	/// No repited nor booked, store them in selected_schedule table and $_SESSION
	session_start();

	$_SESSION ['selected_schedules'] = $_GET ['schedules'];
	$user_id = $_SESSION ['user_id'];
	$stmt = $db->prepare('INSERT INTO selected_schedule VALUES (?, ?, ?, ?, ?)');

	foreach ($selected_schedules as $selected_schedule)
	{
		$stmt->bind_param('ssiii', $selected_schedule->start_date, $selected_schedule->end_date, $selected_schedule->start_hour,
								   $selected_schedule->end_hour, $user_id);
		$stmt->execute();
	}

	$stmt->close();
	$db->close();

	echo json_encode($response);
?>
