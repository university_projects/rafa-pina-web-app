var info_dialog =
{
	/////////////////////////////////////////
	/// Clears the content of the dialog
	/////////////////////////////////////////
	clear: function()
	{
		var content_element = document.getElementById("info-dialog-content");

		while (content_element.hasChildNodes())
			content_element.removeChild(content_element.firstChild);
	},

	/////////////////////////////////////////
	/// Sets the title of the dialog and shows it
	/////////////////////////////////////////
	show: function(title)
	{
		document.getElementById("info-dialog-title").innerHTML = title;
		display("info-dialog-background");
	}
};
