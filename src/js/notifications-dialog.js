// NOTE: This file must me included after user.js
// NOTE: Notifications type 2 to 5 and 51 are pending

const Activity =
{
	/// Activities
	EVENT_CREATION: 1,
	EVENT_MODIFICATION: 2,
	REQUESTED_EVENT_CANCELLATION: 3,
	EVENT_CANCELLATION: 4,
	REJECTED_EVENT_CANCELLATION: 5,
	REQUESTED_PASSWORD_RESET: 6,
	PASSWORD_RESET: 7,
	REJECTED_PASSWORD_RESET: 8,
	PASSWORD_CHANGED: 9,

	/// Requests
	EVENT_CANCELLATION_REQUEST: 51,
	PASSWORD_RESET_REQUEST: 52
};

var notifications_dialog =
{
	notifications: null,	///< Holds the notifications

	/////////////////////////////////////////
	/// Fills and shows the dialog.
	/////////////////////////////////////////
	show: function()
	{
		close_left_menu();
		document.getElementById("notifications-dialog-background").style.display = "block";
		this.fill();
	},

	/////////////////////////////////////////
	/// Fills the notifications-area with
	/// the notifications in zebra rows.
	/// The structure of the row and what
	/// it performs when the user clicks it
	/// depends on the type of the notification.
	///
	/// This function is called when new notifications
	/// are detected but creates the DOM elements
	/// only if the dialog is visible.
	/////////////////////////////////////////
	fill: function()
	{
		if (! is_visible("notifications-dialog-background"))
			return;

		var notifications_area = document.getElementById("notifications-area");

		if (! this.notifications || ! this.notifications.length)
		{
			conceal(notifications_area);
			display("no-notifications-message");
			return;
		}

		remove_child_nodes(notifications_area);

		for (var i = 0; i < this.notifications.length; i++)
		{
			var notification = this.notifications [i];
			var notification_element = document.createElement("div");
			notification_element.className = "scol12";
			notification_element.id = "notification-" + notification.id;

			var notification_text_element = document.createElement("span");

			if (notification.type >= 1 && notification.type <= 9)
			{
				add_class(notification_element, "zebra-row")
				notification_text_element.className = "scol11";
			}

			switch (notification.type)
			{
				case Activity.EVENT_CREATION:
					notification_text_element.innerHTML = "Creaste el evento \"" + notification.event_name + "\"";
					notification_text_element.setAttribute("onclick", "go_to_event(" + notification.event_id + ")");
					notification_text_element.classList.add("zebra-row-clickable");
				break;

				case Activity.EVENT_MODIFICATION:
					notification_text_element.innerHTML = "Editaste el evento \"" + notification.event_name + "\"";
					notification_text_element.setAttribute("onclick", "go_to_event(" + notification.event_id + ")");
					notification_text_element.classList.add("zebra-row-clickable");
				break;

				case Activity.REQUESTED_EVENT_CANCELLATION:
					notification_text_element.innerHTML = "Solicitaste la cancelación del evento \"" + notification.event_name + "\"";
					notification_text_element.setAttribute("onclick", "go_to_event(" + notification.event_id + ")");
					notification_text_element.classList.add("zebra-row-clickable");
				break;

				case Activity.EVENT_CANCELLATION:
					notification_text_element.innerHTML = "El evento \"" + notification.event_name + "\" fue cancelado";
				break;

				case Activity.REJECTED_EVENT_CANCELLATION:
					notification_text_element.innerHTML = "Tu solicitud para cancelar el evento \"" + notification.event_name + "\" fue rechazada";
				break;

				case Activity.REQUESTED_PASSWORD_RESET:
					notification_text_element.innerHTML = "Solicitaste que tu contraseña sea reestablecida";
				break;

				case Activity.PASSWORD_RESET:
					notification_text_element.innerHTML = "Tu contraseña ha sido reestablecida a tu RFC, te recomendamos que la cambies inmediatamente";
				break;

				case Activity.REJECTED_PASSWORD_RESET:
					notification_text_element.innerHTML = "Tu solicitud de reestablecimiento de contraseña fue rechazada";
				break;

				case Activity.PASSWORD_CHANGED:
					notification_text_element.innerHTML = "Cambiaste tu contraseña";
				break;

				case Activity.PASSWORD_RESET_REQUEST:
					add_class(notification_element, "zebra-row-clickable");
					notification_text_element.className = "scol12";
					notification_text_element.innerHTML = "El usuario " + notification.user_name +
														  " solicita que su contraseña sea " + "reestablecida";
					notification_element.setAttribute("data-notification-applicant-id", notification.applicant_id);
					notification_element.setAttribute("data-notification-user-name", notification.user_name);
					notification_text_element.setAttribute("onclick", "notifications_dialog.restore_password(" +
														   notification.id + ")");
					notification_text_element.style.textAlign = "justify";
				break;

				case Activity.EVENT_CANCELLATION_REQUEST:
					add_class(notification_element, "zebra-row-clickable");
					notification_text_element.className = "scol12";
					notification_text_element.innerHTML = "El usuario " + notification.user_name + " solicita que el evento \"" +
														  notification.event_name + "\" sea cancelado";
					notification_text_element.setAttribute("onclick", "notifications_dialog.cancel_event(" + notification.id + ", " +
																	  notification.applicant_id + ", '" + notification.user_name + "', " + "'" +
																	  notification.event_name + "', " + notification.event_id + ")");
					notification_text_element.style.textAlign = "justify";
			}

			notification_text_element.innerHTML += "<br />" + notification.date;
			notification_element.appendChild(notification_text_element);

			/// Add close button
			if (notification.type <= 50)
			{
				var notification_close_button = document.createElement("span");
				notification_close_button.className = "scol1 icon-cancel zebra-row-close-button";
				notification_close_button.setAttribute("onclick", "notifications_dialog.mark_notification_as_read("  +
													   notification.id + ")");
				notification_element.appendChild(notification_close_button);
			}

			notifications_area.appendChild(notification_element);
		}

		if (! is_visible(notifications_area))
			replace_element("no-notifications-message", notifications_area);
	},

	/////////////////////////////////////////
	/// Mark a notification as read.
	/// This function is called by clicking
	/// the x button of notifications type
	/// 1 to 50
	/////////////////////////////////////////
	mark_notification_as_read: function(notification_id)
	{
		var notification_element = document.getElementById("notification-" + notification_id);
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			notifications_dialog.remove_notification(notification_element);
		}
		xmlhttp.open("POST", "php/mark-notification-as-read.php");
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("notification_id=" + notification_id);
	},

	/////////////////////////////////////////
	/// Launches a confirm dialog where the
	/// user can accept or reject the password
	/// reset request.
	///
	/// This function is called when the user clicks
	/// on a notification type 52
	/////////////////////////////////////////
	restore_password: function(notification_id)
	{
		var notification_element = document.getElementById("notification-" + notification_id);
		var applicant_id = notification_element.getAttribute("data-notification-applicant-id");
		var user_name = notification_element.getAttribute("data-notification-user-name");

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == "reset")
				show_toast("Contraseña reestablecida", BlueToast);
			else if (this.responseText == "rejected")
				show_toast("Solicitud rechazada", BlueToast);
			else if (this.responseText == "no request")
				show_toast("La solicitud ya había sido resuelta", DarkToast);

			notifications_dialog.remove_notification(notification_element);
		}
		xmlhttp.open("POST", "php/solve-password-reset-request.php");
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		close_dialog("notifications");
		confirm_dialog.on_accept = function() { xmlhttp.send("applicant_id=" + applicant_id + "&reset=true"); }
		confirm_dialog.on_cancel = function()
		{
			xmlhttp.send("applicant_id=" + applicant_id + "&reset=false");
			confirm_dialog.on_cancel = function() {}
		}
		confirm_dialog.show("Solicitud", "El usuario " + user_name + " solicita que su contraseña sea reestablecida",
							"Reestablecer", "Rechazar");
	},

	////////////////////////////////////////////////
	// Launches a confirm dialog for event cancelling
	// confirmation. Calls to solve-event-cancellation-request.php.
	////////////////////////////////////////////////
	cancel_event: function(notification_id, applicant_id, user_name, event_name, event_id)
	{
		var notification_element = document.getElementById("notification-" + notification_id);
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == "cancelled")
				show_toast("Evento cancelado", BlueToast);
			else if (this.responseText == "rejected")
				show_toast("Solicitud rechazada", BlueToast);
			else if (this.responseText == "no request")
				show_toast("La solicitud ya había sido resuelta", DarkToast);

			notifications_dialog.remove_notification(notification_element);
		}
		xhr.open("POST", "php/solve-event-cancellation-request.php");
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		close_dialog("notifications");
		confirm_dialog.on_accept = function() { xhr.send("applicant_id=" + applicant_id + "&event_id=" + event_id + "&cancel=true"); }
		confirm_dialog.on_cancel = function() { xhr.send("applicant_id=" + applicant_id + "&event_id=" + event_id + "&cancel=false"); }
		confirm_dialog.show("Solicitud", "El usuario " + user_name + " solicita que el evento \"" + event_name + "\" sea cancelado");
	},

	/////////////////////////////////////////
	/// Removes the notification element from
	/// notifications-area.
	///
	/// Its called when the user solves a request
	/// or when clicks the x button.
	/////////////////////////////////////////
	remove_notification: function(notification_element)
	{
		/// Remove it from the notification area of the dialog
		var notifications_area = document.getElementById("notifications-area");
		notifications_area.removeChild(notification_element);

		/// No more notificatios, replace the notifications area by the no-notification-message
		if (! notifications_area.hasChildNodes())
		{
			replace_element(notifications_area, "no-notifications-message");
			document.getElementById("nav-bar-notifications").style.color = "white";
			document.getElementById("left-menu-notifications-icon").style.color = "#555753";
		}

		/// Remove it from the notifications array
		for (var i = 0; i < this.notifications.length; i++)
		{
			if (this.notifications [i].id == notification_element.id.replace("notification-", ""))
			{
				this.notifications.splice(i, 1);
				return;
			}
		}
	},

	/////////////////////////////////////////
	/// Retrieves the notifications of the
	/// user from the server. This function
	/// is the responsible of change the color
	/// of the notifications' icon.
	///
	/// If the dialog is visible, refills
	/// notifications-area.
	///
	/// This function is called if a change in
	/// the notifications are detected.
	/////////////////////////////////////////
	get_notifications: function()
	{
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			notifications_dialog.notifications = JSON.parse(this.responseText);
			notifications_dialog.fill();
			if (! notifications_dialog.notifications.length)
			{
				document.getElementById("nav-bar-notifications").style.color = "white";
				document.getElementById("left-menu-notifications-icon").style.color = "#555753";
			}
			else
			{
				document.getElementById("nav-bar-notifications").style.color = "#e7ff55";
				document.getElementById("left-menu-notifications-icon").style.color = "#f04f57";
			}
		}
		xmlhttp.open("GET", "php/get-notifications.php");
		xmlhttp.send();
	}
};

/// Notifications dialog's deamon
var notifications_dialog_deamon_interval_id;

user.on_login.connect(function()
{
	notifications_dialog.get_notifications();

	notifications_dialog_deamon_interval_id = setInterval(function()
	{
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText != "no changes")
				notifications_dialog.get_notifications();
		}
		xmlhttp.open("GET", "php/check-for-notifications-updates.php");
		xmlhttp.send();

	}, 20000);
});

user.on_logout.connect(function()
{
	clearInterval(notifications_dialog_deamon_interval_id);
});
