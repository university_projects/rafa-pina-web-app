<?php
	////////////////////////////////////////////////
	// Verifies that current user is the creator of
	// current event. Returns error it it's not.
	//
	// Updates event_description and target_audience
	// tables with the given data and logs activity
	// EVENT_MODIFICATION
	////////////////////////////////////////////////
	require 'connect-db.inc';
	require 'log-activity.inc';
	session_start();

	$user_id = $_SESSION ['user_id'];
	$event_id = $_SESSION ['event_id'];
	$db = connect_db();

	/// Verify that user is creator of the event
	$stmt = $db->prepare('SELECT COUNT(*) FROM event_description WHERE id = ? AND user_id = ?');
	$stmt->bind_param('ii', $event_id, $user_id);
	$stmt->execute();
	$stmt->bind_result($is_creator);
	$stmt->fetch();
	$stmt->close();

	if ($is_creator == '0')
	{
		$db->close();
		echo 'error';
		return;
	}

	$event_information = json_decode($_POST ['event_information']);

	/// Update event_description
	$stmt = $db->prepare('UPDATE event_description
						  SET name = ?, description = ?, turnout = ?, image = ?, responsible = ?, equipment_distribution = ?
						  WHERE id = ?');
	$stmt->bind_param('ssissii', $event_information->name, $event_information->description, $event_information->turnout, $event_information->image,
					  			 $event_information->responsible, $event_information->equipment_distribution, $event_id);
	$stmt->execute();
	$stmt->close();

	/// Remove target audiences
	$stmt = $db->prepare('DELETE FROM target_audience WHERE event_description_id = ?');
	$stmt->bind_param('i', $event_id);
	$stmt->execute();
	$stmt->close();

	/// Insert new target audiences
	$stmt = $db->prepare('INSERT INTO target_audience VALUES (?, ?)');
	foreach ($event_information->target_audiences as $target_audience)
	{
		$stmt->bind_param('ii', $target_audience, $event_id);
		$stmt->execute();
	}
	$stmt->close();

	/// Upload new image if needed
	if (isset($_FILES ['custom_image']))
	{
		$file_extension = pathinfo($_FILES ['custom_image'] ['name'], PATHINFO_EXTENSION);
		$final_filename = 'uploads/'.$event_id.'.'.$file_extension;
		move_uploaded_file($_FILES ['custom_image'] ['tmp_name'], '../../assets/event-images/'.$final_filename);

		// Update event_description.image
		$stmt = $db->prepare('UPDATE event_description SET image = ? WHERE id = ?');
		$stmt->bind_param('si', $final_filename, $event_id);
		$stmt->execute();
		$stmt->close();
		echo $final_filename;
	}
	else
	{
		echo 'success';
	}

	log_activity($db, EVENT_MODIFICATION, $user_id, $event_id);
	$db->close();
?>
