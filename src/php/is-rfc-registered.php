<?php
	require 'connect-db.inc';

	$db = connect_db();
	$rfc = $_GET ['rfc'];

	//Check if is an registered rfc
	$stmt = $db->prepare('SELECT COUNT(*) FROM user WHERE rfc = ?');
	$stmt->bind_param('s', $rfc);
	$stmt->execute();
	$stmt->bind_result($result);
	$stmt->fetch();

	echo $result;

	$stmt->close ();
	$db->close ();
?>
