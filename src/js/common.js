/////////////////////////////////////////
/// Tells if an element is visible
/// (i.e., if its dsplayed or not)
/////////////////////////////////////////
function is_visible(element)
{
	if (typeof(element) == "string")
		element = document.getElementById(element);

	return (element.style.display && element.style.display != "none");
}

/////////////////////////////////////////
/// Conceals an element. Sets display to none
/////////////////////////////////////////
function conceal(element)
{
	if (typeof(element) == "string")
		element = document.getElementById(element);

	element.style.display = "none";
}

/////////////////////////////////////////
/// Conceals an element. Sets display to block
/////////////////////////////////////////
function display(element)
{
	if (typeof(element) == "string")
		element = document.getElementById(element);

	element.style.display = "block";
}

/////////////////////////////////////////
/// Removes children of an element
/////////////////////////////////////////
function remove_child_nodes(parent)
{
	if (typeof(parent) == "string")
		parent = document.getElementById(parent);

	while (parent.hasChildNodes())
		parent.removeChild(parent.firstChild);
}

/////////////////////////////////////////
/// Deprecated, use parent.getElementsByClassName(class_name) instead
/////////////////////////////////////////
function get_child_by_class_name(parent, class_name)
{
	if (typeof(parent) == "string")
		parent = document.getElementById(parent);

	for (var i = 0; i < parent.childNodes.length; i++)
	{
		var child = parent.childNodes [i];

		if (child.className != null && child.className.split(" ").indexOf(class_name) >= 0)
			return child;
	}
}

/////////////////////////////////////////
/// Hides an element, sets visibility to hidden
/////////////////////////////////////////
function hide(element)
{
	if (typeof(element) == "string")
		element = document.getElementById(element);

	element.style.display = "block";
	element.style.visibility = "hidden";
}

/////////////////////////////////////////
/// Hides an element, sets visibility to visible
/////////////////////////////////////////
function show(element)
{
	if (typeof(element) == "string")
		element = document.getElementById(element);

	element.style.display = "block";
	element.style.visibility = "visible";
}

/////////////////////////////////////////
/// Replaces a class of an elemento by other
/////////////////////////////////////////
function replace_class(element, replaced_class, replacement_class)
{
	if (typeof(element) == "string")
		element = document.getElementById(element);

	element.className = element.className.replace(replaced_class, replacement_class);
}

/////////////////////////////////////////
/// Hides an element and shows another using
/// a fade in and fade out animation respectively
/////////////////////////////////////////
function replace_element(current_element, new_element)
{
	if (typeof(current_element) == "string")
		current_element = document.getElementById(current_element);

	if (typeof(new_element) == "string")
		new_element = document.getElementById(new_element);

	current_element.className += " fade-in";

	setTimeout(function()
	{
		current_element.style.display = "none";
		replace_class(current_element, "fade-in", "");
		new_element.style.display = "block";
		new_element.className += " fade-out";

		setTimeout(function()
		{
			replace_class(new_element, "fade-out", "");
		}, 500);
	}, 500);
}

/////////////////////////////////////////
/// Adds a class to an element
/////////////////////////////////////////
function add_class(element, new_class)
{
	if (typeof(element) == "string")
		element = document.getElementById(element);

	element.className += " " + new_class;
}

/////////////////////////////////////////
/// Makes an array using a JSON object
/////////////////////////////////////////
function object_to_array(object)
{
	return Object.keys(object).map(function (key) { return object[key]; });
}

/////////////////////////////////////////
/// Verifies if an input number's value is
/// out of range, if it is, sets it to the
/// nearest valid value
/////////////////////////////////////////
function verify_input_number_range(input)
{
	if (!input.value.length || parseInt(input.value) < input.min)
	{
		input.value = input.min;
		return;
	}

	if (parseInt(input.value) > input.max)
		input.value = input.max;
}

/////////////////////////////////////////
/// Sets the max value of a number input.
///
/// If it's current value is out of range,
/// sets it as max
/////////////////////////////////////////
function set_input_number_max(input, max = parseInt(max))
{
	input.max = max;

	if (parseInt(input.value) > max)
		input.value = max;
}

/////////////////////////////////////////
/// Sets the min value of a number input.
///
/// If it's current value is out of range,
/// sets it as min
/////////////////////////////////////////
function set_input_number_min(input, min = parseInt(max))
{
	input.min = min;

	if (parseInt(input.value) < min)
		input.value = min;
}

/////////////////////////////////////////
/// Prevents that the user inputs a non numeric.
/// You can set it to the onkeypress event of
/// an input. It allows to use backspace and the arrows.
/////////////////////////////////////////
function prevent_non_numeric_input(event)
{
	if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57))
		event.preventDefault();
}
