<?php
	require_once 'activity-type.inc';

	/////////////////////////////////////////
	/// Logs an activity of a given type which
	/// was made by a given applicant.
	///
	/// An activity can be related to an event,
	/// so the event's id must be supplied
	///
	/// $db param must be a connection to a db
	/// with no stmt openned
	/////////////////////////////////////////
	function log_activity($db, $type, $applicant_id, $event_id = NULL)
	{
	    $stmt = $db->prepare('INSERT INTO activity VALUES(NULL, ?, 1, NOW(), ?, ?, NULL)');
	    $stmt->bind_param('iii', $type, $applicant_id, $event_id);
	    $stmt->execute();
	    $stmt->close();
	}
?>
