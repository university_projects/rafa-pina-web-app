<?php
	require 'connect-db.inc';

	$db = connect_db();
	$rfc = $_POST ['rfc'];
	$password = $_POST ['password'];

	$stmt = $db->prepare('SELECT user.id, CONCAT(nick.name, ". ", user.name), is_admin
						  FROM user JOIN nick ON user.nick_id = nick.id
						  WHERE rfc = ? AND password = SHA2(?, 256)');
	$stmt->bind_param('ss', $rfc, $password);
	$stmt->execute();
	$stmt->bind_result($id, $name, $is_admin);

	if ($stmt->fetch())
	{
		session_start();
		$_SESSION ['user_id'] = $id;
		$_SESSION ['user_name'] = $name;
		$_SESSION ['user_is_admin'] = $is_admin;

		$user_info = new stdClass ();
		$user_info->name = $name;
		$user_info->is_admin = $is_admin;
		echo json_encode ($user_info);
	}
	else
		echo 'wrong data';

	$stmt->close ();
	$db->close ();
?>
