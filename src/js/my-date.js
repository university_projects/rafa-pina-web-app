function MyDate (year = 2014, month = 11, date = 26, leap_year = false)
{
	this.date = null;
	this.month = null;
	this.year = null;
	this.leap_year = null;

	/////////////////////////////////////////
	/// Sets year, month, date and leap_year properties.
	/////////////////////////////////////////
	this.init = function(year, month, date)
	{
		this.year = parseInt(year);
		this.month = parseInt(month);
		this.date = parseInt(date);
		this.leap_year = is_leap_year(this.year);
	}

	/////////////////////////////////////////
	/// Converts the date to a formatted string
	/// like 'YYYY-MM-DD', example, '2014-11-26'
	/////////////////////////////////////////
	this.to_string = function ()
	{
		return this.year + "-" + this.month + "-" + this.date;
	}

	/////////////////////////////////////////
	/// Fills the members using a formatted string
	/// like 'YYYY-MM-DD'
	/////////////////////////////////////////
	this.from_string = function(string)
	{
		var match = /(\d+)-(\d+)-(\d+)/.exec(string);
		this.year = parseInt(match [1]);
		this.month = parseInt(match [2]);
		this.date = parseInt(match [3]);
		this.leap_year = is_leap_year(this.year);
	}

	/////////////////////////////////////////
	/// Moves the month number by 'number'.
	/// It can't move to months less than 1 (January)
	/// or more than 12 (December).
	///
	/// If the movement is forward, the date is set
	/// to 1, is set to the last day otherwise.
	/////////////////////////////////////////
	this.move_month = function (number)
	{
		if (this.month + number < 1 || this.month + number > 12)
			return;

		this.month += number;
		this.date = (number < 0) ? this.get_number_of_days () : 1;
	}

	/////////////////////////////////////////
	/// Gets the number of day that the
	/// month member has.
	/////////////////////////////////////////
	this.get_number_of_days = function ()
	{
		switch (this.month)
		{
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:  return 31;
			case 4: case 6: case 9: case 11:  return 30;
			case 2: return (leap_year == 1) ? 29 : 28;
		}
	}

	/////////////////////////////////////////
	/// Gets the name of the month
	/// according to the month member.
	/////////////////////////////////////////
	this.get_month_name = function ()
	{
		switch (this.month)
		{
			case 1:  return "Enero";
			case 2:  return "Febrero";
			case 3:  return "Marzo";
			case 4:  return "Abril";
			case 5:  return "Mayo";
			case 6:  return "Junio";
			case 7:  return "Julio";
			case 8:  return "Agosto";
			case 9:  return "Septiembre";
			case 10: return "Octubre";
			case 11: return "Noviembre";
			case 12: return "Diciembre";
		}
	}

	/////////////////////////////////////////
	/// Gets the name of the day from the
	/// date member.
	/////////////////////////////////////////
	this.get_day_name = function ()
	{
		switch (this.get_week_day ())
		{
			case 0: return "Domingo";
			case 1: return "Lunes";
			case 2: return "Martes";
			case 3: return "Miércoles";
			case 4: return "Jueves";
			case 5: return "Viernes";
			case 6: return "Sábado";
		}
	}

	/////////////////////////////////////////
	/// Gets the week day from the date member.
	/////////////////////////////////////////
	this.get_week_day = function ()
	{
		return new Date (this.year, this.month - 1, this.date).getDay ();
	}

	/////////////////////////////////////////
	/// Moves the date by 'number'.
	///
	/// If the resultant date is less than 1,
	/// the month is moved by -1.
	///
	/// If the resultant date is more than the
	/// number of the days of the month, the
	/// month is move by 1.
	/////////////////////////////////////////
	this.move_date = function (number)
	{
		if (this.date + number > this.get_number_of_days ())
			this.move_month (1);
		else if (this.date + number < 1)
			this.move_month (-1);
		else
			this.date += number;
	}

	/////////////////////////////////////////
	/// Tells if this date is equal or minor
	/// than $other. Returns true if it is,
	/// false otherwise.
	/////////////////////////////////////////
	this.is_minor_equal = function (other)
	{
		if (this.year < other.year)
			return true;

		if (this.year == other.year)
		{
			if (this.month < other.month)
				return true;

			if (this.month == other.month)
			{
				if (this.date <= other.date)
					return true;
			}
		}

		return false;
	}

	/////////////////////////////////////////
	/// Returns true if this date is equal than
	/// other. Other must be a MyDate object
	/////////////////////////////////////////
	this.is_equal_than = function(other)
	{
		return this.year == other.year && this.month == other.month && this.date == other.date;
	}

	/////////////////////////////////////////
	/// Return true if this date is more than
	/// other. Other must be a MyDate object
	/////////////////////////////////////////
	this.is_more_than = function(other)
	{
		if (this.year > other.year)
			return true;

		if (this.year == other.year)
		{
			if (this.month > other.month)
				return true;

			if (this.month == other.month)
			{
				if (this.date > other.date)
					return true;
			}
		}

		return false;
	}

	/////////////////////////////////////////
	/// Returns true if this date is minor than
	/// other. Other must be a MyDate object
	/////////////////////////////////////////
	this.is_minor_than = function(other)
	{
		if (this.year < other.year)
			return true;

		if (this.year == other.year)
		{
			if (this.month < other.month)
				return true;

			if (this.month == other.month)
			{
				if (this.date < other.date)
					return true;
			}
		}

		return false;
	}

	this.init(year, month, date);
}

/////////////////////////////////////////
/// Tells if a year is leap or not
/////////////////////////////////////////
function is_leap_year(year)
{
	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}
