function redirect(url)
{
	document.location.href = url;
}

/////////////////////////////////////////
/// Stores the id of the event in $_SESSION ['event-id']
/// and redirects to event.html
/////////////////////////////////////////
function go_to_event(event_id)
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;

		redirect("event.html");
	};
	xmlhttp.open("POST", "php/go-to-event.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("event-id=" +  event_id);
}

const EVENT_STATE =
{
	Finished: 4,
	Unrealized: 5,
	Cancelled: 7
};

const REPORT_TYPE =
{
	Events: 0,
	Users: 1,
	Target_audiences: 2,
	Equipment_distribution: 3
};

const EVENTS_FILTER_TYPE =
{
	Name: 0,
	Created_by: 1,
	Responsable: 2,
	Turnout: 3,
	Equipment_distribution: 4
};

////// Date dropdowns
var start_date_dropdown;
var end_date_dropdown;
var start_month_dropdown;
var end_month_dropdown;

////// Events' report
var events_filter_dropdown;
var equipment_distribution_dropdown;
var events_report_table;

////// Users' report
var users_report_table = null;

var reports_module =
{
	report_type: REPORT_TYPE.Events,
	semester: new Object(),

	events_report_need_update: true,
	users_report_need_update: true,
	target_audiences_report_need_update: true,
	equipment_distribution_report_need_update: true,

	events: [],
	filtered_events_by_dates: [],
	events_filter_type: EVENTS_FILTER_TYPE.Name,

	init: function()
	{
		if (user.type != "admin")
		{
			redirect("homepage.html");
			return;
		}

		user.on_logout.connect(function() { redirect("homepage.html"); });

		start_date_dropdown = new Dropdown("start_date");
		end_date_dropdown = new Dropdown("end_date");
		start_month_dropdown = new Dropdown("start_month");
		end_month_dropdown = new Dropdown("end_month");

		events_filter_dropdown = new Dropdown("events_filter");
		events_filter_dropdown.add_option("Nombre del evento");
		events_filter_dropdown.add_option("Creador");
		events_filter_dropdown.add_option("Responsable");
		events_filter_dropdown.add_option("Asistentes");
		events_filter_dropdown.add_option("Distribución");
		events_filter_dropdown.on_option_changed.connect(function() { reports_module.on_events_filter_changed(); });

		events_report_table = new Table("events-report-table", "events_report_table");
		events_report_table.add_header("Nombre del evento");
		events_report_table.add_header("Creador por");
		events_report_table.add_header("Responsable");
		events_report_table.add_header("Asistentes", "int");
		events_report_table.add_header("Distribución");

		equipment_distribution_dropdown = new Dropdown("equipment_distribution");
		equipment_distribution_dropdown.add_option("Auditorio");
		equipment_distribution_dropdown.add_option("Escuela");
		equipment_distribution_dropdown.add_option("Mesa imperial");
		equipment_distribution_dropdown.add_option("Mesa redonda");
		equipment_distribution_dropdown.add_option("Mesa rusa");
		equipment_distribution_dropdown.add_option("Mesa tipo U");
		equipment_distribution_dropdown.on_option_changed.connect(function()
		{
			events_report_table.apply_filter(reports_module.events_filter_type, equipment_distribution_dropdown.get_selected_value());
		});

		/// Turnout range selector
		var min_turnout_input = document.getElementById("event-min-turnout-filter");
		var max_turnout_input = document.getElementById("event-max-turnout-filter");
		set_input_number_min(min_turnout_input, 15);
		min_turnout_input.max = parseInt(max_turnout_input.value);
		set_input_number_max(max_turnout_input, 90);
		max_turnout_input.min = parseInt(min_turnout_input.value);

		max_turnout_input.addEventListener("change", function() { set_input_number_max(min_turnout_input, max_turnout_input.value); });
		min_turnout_input.addEventListener("change", function() { set_input_number_min(max_turnout_input, min_turnout_input.value); });

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var server_response = JSON.parse(this.responseText);

			/// Semester info
			var semester = server_response.semester;
			reports_module.semester.start_date = new MyDate();
			reports_module.semester.start_date.from_string(semester.start_date);
			reports_module.semester.end_date = new MyDate();
			reports_module.semester.end_date.from_string(semester.end_date);

			/// Fill month dropdowns
			var date = new MyDate();
			date.from_string(semester.start_date);

			while (date.month <= reports_module.semester.end_date.month)
			{
				var month_name = date.get_month_name();
				start_month_dropdown.add_option(month_name, date.month);
				end_month_dropdown.add_option(month_name, date.month);

				date.move_month(1);
			}

			end_month_dropdown.select_option_by_index(dropdown_last_index);

			/// Fill date dropdowns
			date.month = reports_module.semester.start_date.month;
			start_date_dropdown.replace_options_by_range(reports_module.semester.start_date.date, date.get_number_of_days());

			end_date_dropdown.replace_options_by_range(1, reports_module.semester.end_date.date);
			end_date_dropdown.select_option_by_index(dropdown_last_index);

			/// Month and date dropdowns' connections
			start_month_dropdown.on_option_changed.connect(function() { reports_module.changes_in_start_month_dropdown(); },
														   "reports module");
			end_month_dropdown.on_option_changed.connect(function() { reports_module.changes_in_end_month_dropdown(); }, "reports module");
			start_date_dropdown.on_option_changed.connect(function() { reports_module.changes_in_start_date_dropdown(); }, "reports module");
			end_date_dropdown.on_option_changed.connect(function() { reports_module.changes_in_end_date_dropdown(); }, "reports module");

			reports_module.events = server_response.events;
			reports_module.update_filtered_events_by_date();
			events_report_table.sort_by(0);
		}
		xmlhttp.open("GET", "php/get-events-of-the-semester.php");
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// Sets the report which is shown to the user.
	/////////////////////////////////////////
	set_report_type: function(report_type)
	{
		if (this.report_type == report_type)
			return;

		var report_areas = document.getElementsByClassName("report-area");
		conceal(report_areas [this.report_type]);
		this.report_type = report_type;
		replace_class(document.getElementsByClassName("selected-report-type") [0], "selected-report-type", "report-type");
		replace_class(document.getElementsByClassName("report-type") [report_type], "report-type", "selected-report-type");
		display(report_areas [report_type]);

		switch (report_type)
		{
			case REPORT_TYPE.Events: this.generate_events_report(); break;
			case REPORT_TYPE.Users: this.generate_users_report(); break;
			case REPORT_TYPE.Target_audiences: this.generate_target_audiences_report(); break;
			case REPORT_TYPE.Equipment_distribution: this.generate_equipment_distribution_report();
		}
	},

	/////////////////////////////////////////
	/// Generates event's report based on
	/// this.filtered_events_by_dates
	/////////////////////////////////////////
	generate_events_report: function()
	{
		if (! this.events_report_need_update)
			return;

		events_report_table.clear_data();
		for (var i = 0; i < this.filtered_events_by_dates.length; i++)
			events_report_table.add_row(this.filtered_events_by_dates [i].table_data);

		this.events_report_need_update = false;
	},

	/////////////////////////////////////////
	/// Generates the users' report based on
	/// this.filtered_events_by_dates.
	///
	/// It's called when the users selects the
	/// users' report type or when filtered_events_by_dates
	/// are updated.
	/////////////////////////////////////////
	generate_users_report: function()
	{
		if (! this.users_report_need_update)
			return;

		if (! users_report_table)
		{
			users_report_table = new Table("users-report-table", "users_report_table");
			users_report_table.add_header("Nombre");
			users_report_table.add_header("Eventos creados", "int");
			users_report_table.add_header("Eventos realizados", "int");
			users_report_table.add_header("Eventos cancelados", "int");
			users_report_table.add_header("Eventos no realizados", "int");
		}

		var users_report = [];

		for (var i = 0; i < this.filtered_events_by_dates.length; i++)
		{
			var user_name = this.filtered_events_by_dates [i].table_data [1];

			/// Look if the user has been added to users_report
			var user = null;
			for (var i = 0; i < users_report.length; i++)
			{
				if (users_report [i] [0] == user_name)
				{
					user = users_report [i];
					users [1]++; /// Increase created events
					break;
				}
			}

			/// If the user has noot been added to users_report
			if (! user)
			{
				user = [user_name, 1, 0, 0, 0];
				users_report.push(user);
			}

			switch (this.filtered_events_by_dates [i].state)
			{
				case EVENT_STATE.Finished:   user [2]++; break;
				case EVENT_STATE.Cancelled:  user [3]++; break;
				case EVENT_STATE.Unrealized: user [4]++;
			}
		}

		users_report_table.clear_data();
		users_report_table.data = users_report;
		users_report_table.fill();
		users_report_table.ascending_order = true;
		users_report_table.sort_by(0);

		this.users_report_need_update = false;
	},

	/////////////////////////////////////////
	/// Generates target audiences's report based on
	/// this.filtered_events_by_dates
	/////////////////////////////////////////
	generate_target_audiences_report: function()
	{
		if (! this.target_audiences_report_need_update)
			return;

		var target_audiences = [];

		for (var i = 0; i < this.filtered_events_by_dates.length; i++)
		{
			const event_target_audiences = this.filtered_events_by_dates [i].target_audiences;

			for (var j = 0; j < event_target_audiences.length; j++)
			{
				var event_target_audience = event_target_audiences [j];
				var added = false;

				for (var k = 0; k < target_audiences.length; k++)
				{
					if (event_target_audience == target_audiences [k].name)
					{
						target_audiences [k].value++;
						added = true;
						break;
					}
				}

				if (! added)
					target_audiences.push({name: event_target_audience, value: 1, color: ""});
			}
		}

		const colors = ["#ddd", "#28282a", "#123483", "#ff8024", "#b865ec", "#42e98a"];
		for (var i = 0; i < target_audiences.length; i++)
			target_audiences [i].color = colors [i];

		bar_chart("target-audiences-canvas", target_audiences, 10);

		this.target_audiences_report_need_update = false;
	},

	/////////////////////////////////////////
	/// Generates equipment distribution's report based on
	/// this.filtered_events_by_dates
	/////////////////////////////////////////
	generate_equipment_distribution_report: function()
	{
		if (! this.equipment_distribution_report_need_update)
			return;

		var slices = [];

		for (var i = 0; i < this.filtered_events_by_dates.length; i++)
		{
			var event_equipment_distribution = this.filtered_events_by_dates [i].table_data [4];

			/// Look if the equipment_distribution has been added
			var registered_slice = false;
			for (var j = 0; j < slices.length; j++)
			{
				if (slices [j].name == event_equipment_distribution)
				{
					slices [j].value++;
					registered_slice = true;
					break;
				}
			}

			if (! registered_slice)
				slices.push({name: event_equipment_distribution, value: 1, color: ""});
		}

		const colors = ["#ddd", "#28282a", "#123483", "#ff8024", "#b865ec", "#42e98a"];
		for (var i = 0; i < slices.length; i++)
			slices [i].color = colors [i];

		pie_chart("equipment-distribution-canvas", slices);

		this.equipment_distribution_report_need_update = false;
	},

	/////////////////////////////////////////////// Month and date dropdowns

	/////////////////////////////////////////
	/// This function is called when the option
	/// of the start_month dropdown is changed
	///
	/// Clears end_month_dropdown and
	/// fills it from start_month_dropdown's
	/// seleted month til semester's end month
	/////////////////////////////////////////
	changes_in_start_month_dropdown: function()
	{
		var start_month = start_month_dropdown.get_selected_option().data;
		var end_month = end_month_dropdown.get_selected_option().data;
		var date = new MyDate(this.semester.start_date.year, start_month);

		/// Fill the end month dropdown
		end_month_dropdown.on_option_changed.disable("reports module");
		end_month_dropdown.clear();
		while (date.month <= this.semester.end_date.month)
		{
			end_month_dropdown.add_option(date.get_month_name(), date.month);
			date.move_month(1);
		}

		end_month_dropdown.select_option_by_data(end_month);
		end_month_dropdown.on_option_changed.enable("reports module");
		this.fill_start_date_dropdown();
		this.fill_end_date_dropdown();
	},

	/////////////////////////////////////////
	/// This function is called when the option
	/// of the start_month dropdown is changed
	///
	/// Clears start_month_dropdown and
	/// fills it from semester's start month til
	/// end_month_dropdown's selected month
	/////////////////////////////////////////
	changes_in_end_month_dropdown: function()
	{
		var end_month = end_month_dropdown.get_selected_option().data;
		var start_month = start_month_dropdown.get_selected_option().data;
		var date = new MyDate(this.semester.start_date.year, this.semester.start_date.month);

		/// Fill start_month dropdown
		start_month_dropdown.on_option_changed.disable("reports module");
		start_month_dropdown.clear();

		while (date.month <= end_month)
		{
			start_month_dropdown.add_option(date.get_month_name(), date.month);
			date.move_month(1);
		}

		start_month_dropdown.select_option_by_data(start_month);
		start_month_dropdown.on_option_changed.enable("reports module");
		this.fill_end_date_dropdown();
		this.fill_start_date_dropdown();
	},

	/////////////////////////////////////////
	/// Replace the options of the start_date_dropdown
	/// with the range:
	/// from = if semester's first month equal
	/// to selected start month, semester's
	/// start day, else 1.
	/// to = if selected start month equal to
	/// selected end month, selected end day,
	/// else selected start month's number of days.
	/////////////////////////////////////////
	fill_start_date_dropdown: function()
	{
		var start_date = start_date_dropdown.get_selected_value();
		var start_month = start_month_dropdown.get_selected_option().data;
		var from = (start_month == this.semester.start_date.month) ? this.semester.start_date.date : 1;
		var to;

		if (start_month == end_month_dropdown.get_selected_option().data)
			to = end_date_dropdown.get_selected_value();
		else
			to = new MyDate(this.semester.start_date.year, start_month).get_number_of_days();

		start_date_dropdown.on_option_changed.disable("reports module");
		start_date_dropdown.replace_options_by_range(from, to);
		start_date_dropdown.select_option_by_value(start_date);
		start_date_dropdown.on_option_changed.enable("reports module");
		this.update_filtered_events_by_date();
	},

	/////////////////////////////////////////
	/// Replace the options of the end_date_dropdown
	/// with the range:
	/// from = if selected start month equal to
	/// selected end month, selected start day,
	/// else 1.
	/// to = if selected end month equal to
	/// semester's end month, semester's end day,
	/// else selected end month's number of days.
	/////////////////////////////////////////
	fill_end_date_dropdown: function()
	{
		/// Fill the end date dropdown
		var end_month = end_month_dropdown.get_selected_option().data;
		var end_date = end_date_dropdown.get_selected_value();
		var date = new MyDate(this.semester.start_date.year, end_month);
		var from;
		var to;

		if (end_month == start_month_dropdown.get_selected_option().data)
		{
			from = start_date_dropdown.get_selected_value();
			to = date.get_number_of_days();
		}
		else
		{
			from = 1;
			to = (end_month == this.semester.end_date.month) ? this.semester.end_date.date : date.get_number_of_days();
		}

		end_date_dropdown.on_option_changed.disable("reports module")
		end_date_dropdown.replace_options_by_range(from, to);
		end_date_dropdown.select_option_by_value(end_date);
		end_date_dropdown.on_option_changed.enable("reports module")
		this.update_filtered_events_by_date();
	},

	/////////////////////////////////////////
	/// This function is called when a new date
	/// is selected in start_date_dropdown.
	///
	/// If start month and end month are equal,
	/// clears end_date_dropdown's options
	/// and fill it with the range: start_date to the end of the month
	/////////////////////////////////////////
	changes_in_start_date_dropdown: function()
	{
		var end_month = end_month_dropdown.get_selected_option().data;
		if (start_month_dropdown.get_selected_option().data != end_month)
		{
			this.update_filtered_events_by_date();
			return;
		}

		var end_date = end_date_dropdown.get_selected_value();

		var from = start_date_dropdown.get_selected_value();
		var to = (end_month == this.semester.end_date.month) ? this.semester.end_date.date :
															   end_date_dropdown.options [end_date_dropdown.options.length - 1].value;
		end_date_dropdown.on_option_changed.disable("reports module");
		end_date_dropdown.replace_options_by_range(from, to);
		end_date_dropdown.select_option_by_value(end_date);
		end_date_dropdown.on_option_changed.enable("reports module");
		this.update_filtered_events_by_date();
	},

	/////////////////////////////////////////
	/// This function is called when a new date
	/// is selected in end_date_dropdown.
	///
	/// If start month and end month are equal,
	/// clears start_date_dropdown
	/// and fill it with the range: minimun possible date to
	/// selected end date.
	/////////////////////////////////////////
	changes_in_end_date_dropdown: function()
	{
		var start_month = start_month_dropdown.get_selected_option().data;
		if (start_month != end_month_dropdown.get_selected_option().data)
		{
			this.update_filtered_events_by_date();
			return;
		}

		var start_date = start_date_dropdown.get_selected_value();
		var from = (start_month == this.semester.start_date.month) ? this.semester.start_date.date : 1;
		var to = end_date_dropdown.get_selected_value();

		start_date_dropdown.on_option_changed.disable("reports module");
		start_date_dropdown.replace_options_by_range(from, to);
		start_date_dropdown.select_option_by_value(start_date);
		start_date_dropdown.on_option_changed.enable("reports module");
		this.update_filtered_events_by_date();
	},

	/////////////////////////////////////////
	/// This function is called when the event
	/// filter type is changed
	/////////////////////////////////////////
	on_events_filter_changed: function()
	{
		var filter_type = events_filter_dropdown.selected_index;

		if (this.events_filter_type == filter_type)
			return;

		this.events_filter_type = filter_type;
		switch (filter_type)
		{
			case EVENTS_FILTER_TYPE.Name: case EVENTS_FILTER_TYPE.Created_by: case EVENTS_FILTER_TYPE.Responsable:
				conceal("event-turnout-filter");
				conceal("equipment-distribution-dropdown");
				display("events-report-filter-input");
				document.getElementById("events-report-filter-input").value = "";
				events_report_table.apply_filter(filter_type, "");
			break;

			case EVENTS_FILTER_TYPE.Equipment_distribution:
				conceal("event-turnout-filter");
				conceal("events-report-filter-input");
				display("equipment-distribution-dropdown");
				events_report_table.apply_filter(filter_type, equipment_distribution_dropdown.get_selected_value());
			break;

			case EVENTS_FILTER_TYPE.Turnout:
				conceal("events-report-filter-input");
				conceal("equipment-distribution-dropdown");
				display("event-turnout-filter");
				document.getElementById("event-min-turnout-filter").value = "15";
				document.getElementById("event-max-turnout-filter").value = "90";
				events_report_table.apply_range_filter(filter_type, 15, 90);
		}
	},

	/////////////////////////////////////////
	/// This functions is called each time the
	/// date filter is updated.
	/////////////////////////////////////////
	update_filtered_events_by_date: function()
	{
		this.filtered_events_by_dates = [];
		var start_date = new MyDate(this.semester.start_date.year, start_month_dropdown.get_selected_option().data,
									start_date_dropdown.get_selected_value());
		var end_date = new MyDate(start_date.year, end_month_dropdown.get_selected_option().data, end_date_dropdown.get_selected_value());
		var event_start_date = new MyDate();
		var event_end_date = new MyDate();

		for (var i = 0; i < this.events.length; i++)
		{
			var event = this.events [i];

			for (var j = 0; j < event.dates.length; j++)
			{
				event_start_date.from_string(event.dates [j].start_date);
				event_end_date.from_string(event.dates [j].end_date);

				if (event_start_date.is_minor_equal(end_date) && start_date.is_minor_equal(event_end_date))
				{
					this.filtered_events_by_dates.push(event);
					break;
				}
			}
		}

		this.events_report_need_update = true;
		this.users_report_need_update = true;
		this.target_audiences_report_need_update = true;
		this.equipment_distribution_report_need_update = true;

		switch (this.report_type)
		{
			case REPORT_TYPE.Events: this.generate_events_report(); break;
			case REPORT_TYPE.Users:  this.generate_users_report(); break;
			case REPORT_TYPE.Target_audiences: this.generate_target_audiences_report(); break;
			case REPORT_TYPE.Equipment_distribution: this.generate_equipment_distribution_report();
		}
	}
};

user.onload.connect(function() { reports_module.init(); });
