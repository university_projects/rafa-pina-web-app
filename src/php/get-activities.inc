<?php
	require_once 'activity-type.inc';

	////////////////////////////////////////
	// Gets the activities of a given user.
	//
	// Returns an array of objects with the
	// following structure:
	// + id
	// + type
	// + state
	// + date
	// + applicant_id
	// + event_id
	// + event_name (only if event_id is not null)
	// + user_name (only for requests)
	//
	// $db must be a object returned by connect_db()
	// without opened statements.
	// $is_admin tells if user is an admin user;
	// if it is, it will get requests too.
	// $only_notifications determines if the
	// returned activities must be notifications
	////////////////////////////////////////
	function get_activities($db, $user_id, $is_admin, $only_notifications = false)
	{
		/// Get activities' general information
		if ($only_notifications)
		{
			if ($is_admin)
			{
				$stmt = $db->prepare('SELECT id, type, state, date, applicant_id, event_id FROM activity
									  WHERE ((applicant_id = ? AND type <= 50) OR type >= 51) AND state = 1 ORDER BY date DESC');
		    }
			else
			{
				$stmt = $db->prepare('SELECT id, type, state, date, applicant_id, event_id FROM activity
									  WHERE applicant_id = ? AND type <= 50 AND state = 1 ORDER BY date DESC');
			}
		}
		else
		{
			if ($is_admin)
			{
				$stmt = $db->prepare('SELECT id, type, state, date, applicant_id, event_id FROM activity
									  WHERE (applicant_id = ? AND type <= 50) OR type >= 51 ORDER BY date DESC');
		    }
			else
			{
				$stmt = $db->prepare('SELECT id, type, state, date, applicant_id, event_id FROM activity
									  WHERE applicant_id = ? AND type <= 50 ORDER BY date DESC');
			}
		}

		$stmt->bind_param('i', $user_id);
		$stmt->execute();
		$stmt->bind_result($id, $type, $state, $date, $applicant_id, $event_id);

		$activities = array();
		while ($stmt->fetch())
		{
			$activity = new stdClass();
			$activity->id = $id;
			$activity->type = $type;
			$activity->state = $state;
			$activity->date = $date;
			$activity->applicant_id = $applicant_id;
			$activity->event_id = $event_id;
			array_push($activities, $activity);
		}
		$stmt->close();

		/// Get further information
		foreach ($activities as $activity)
		{
			/// Event name
			if (($activity->type >= EVENT_CREATION && $activity->type <= REJECTED_EVENT_CANCELLATION) || $activity->type == EVENT_CANCELLATION_REQUEST)
			{
				$stmt = $db->prepare('SELECT event_description.name FROM event_description
									  JOIN activity ON activity.event_id = event_description.id
									  WHERE activity.id = ?');
				$stmt->bind_param('i', $activity->id);
				$stmt->execute();
				$stmt->bind_result($event_name);
				$stmt->fetch();
				$activity->event_name = $event_name;
				$stmt->close();
			}

			/// Applicant's name
			if ($activity->type >= EVENT_CANCELLATION_REQUEST && $activity->type <= PASSWORD_RESET_REQUEST)
			{
				$stmt = $db->prepare('SELECT user.name FROM user
								  	  JOIN activity ON activity.applicant_id = user.id
								  	  WHERE activity.id = ?');
				$stmt->bind_param('i', $activity->id);
				$stmt->execute();
				$stmt->bind_result($user_name);
				$stmt->fetch();
				$activity->user_name = $user_name;
				$stmt->close();
			}
		}

		return $activities;
	}
?>
