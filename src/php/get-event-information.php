<?php
	require 'connect-db.inc';
	session_start();

	$event_id = $_SESSION ['event_id'];
	$db = connect_db();

	/// Get event's information from event_description
	$event_information = new stdClass();
	$stmt = $db->prepare('SELECT name, description, turnout, image, state, user_id, responsible, equipment_distribution
						  FROM event_description WHERE id = ?');
	$stmt->bind_param('i', $event_id);
	$stmt->execute();
	$stmt->bind_result($event_information->name, $event_information->description, $event_information->turnout, $event_information->image,
					   $event_information->state, $event_information->user_id, $event_information->responsible,
					   $event_information->equipment_distribution);
	$stmt->fetch();
	$stmt->close();

	/// Get event's creator
	$stmt = $db->prepare('SELECT CONCAT(nick.name, ". ", user.name) FROM user JOIN nick ON user.nick_id = nick.id WHERE user.id = ?');
	$stmt->bind_param('i', $event_information->user_id);
	$stmt->execute();
	$stmt->bind_result($event_information->created_by);
	$stmt->fetch();
	$stmt->close();

	/// Get target audiences id
	$stmt = $db->prepare('SELECT public_type_id FROM target_audience WHERE event_description_id = ?');
	$stmt->bind_param('i', $event_id);
	$stmt->execute();
	$stmt->bind_result($target_audience_id);

	$event_information->target_audiences = array();
	while ($stmt->fetch())
		array_push($event_information->target_audiences, $target_audience_id);

	$stmt->close();

	$db->close();

	echo json_encode($event_information);
?>
