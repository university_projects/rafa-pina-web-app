<?php
	/////////////////////////////////////////
	/// Gets the name and id of the events which
	/// names match with the given name
	/////////////////////////////////////////
	require 'connect-db.inc';

	$event_name = '%'.$_GET ['event-name'].'%';
	$db = connect_db();

	$stmt = $db->prepare('SELECT id, name FROM event_description WHERE name LIKE ?');
	$stmt->bind_param('s', $event_name);
	$stmt->execute();
	$stmt->bind_result($id, $name);

	$found_events = array();
	while ($stmt->fetch())
		array_push($found_events, array('id' => $id, 'name' => $name));

	$stmt->close();
	$db->close();
	echo json_encode($found_events);
 ?>
