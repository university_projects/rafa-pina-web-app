<?php
	/////////////////////////////////////////
	/// Gets the current date of the server
	/// and the start date and end date of the
	/// current semester.
	///
	/// If there's no current semester, $date->semester
	/// is filled with 'No semester'
	/////////////////////////////////////////
	require 'connect-db.inc';

	$date = new stdClass();

	/// Server's date
	date_default_timezone_set('America/Mexico_City');
	$date->current_date = new stdClass();
	$date->current_date->day = date('d');
	$date->current_date->month = date('n');
	$date->current_date->year = date('Y');
	$date->current_date->leap_year = date('L');

	/// Calendar state
	session_start();
	$date->calendar_state = (isset($_SESSION ['calendar-state'])) ? 1 : 0;

	/// Semester info
	$db = connect_db();
	$stmt = $db->prepare('SELECT start_date, end_date FROM semester
						  WHERE start_date <= CURRENT_DATE() AND end_date >= CURRENT_DATE()');
	$stmt->execute();
	$stmt->bind_result($start_date, $end_date);

	$date->semester = new stdClass ();
	/// There's current semester
	if ($stmt->fetch())
	{
		$date->semester->start_date = $start_date;
		$date->semester->end_date = $end_date;
		$stmt->close();
		$db->close();
		echo json_encode($date);
		return;
	}

	/// No current semester
	/// Check if it has been registered but it hasn't started
	if ($date->current_date->month == 12)
		$upcoming_semester_date = ($date->current_date->year + 1).'-01-31';
	else
		$upcoming_semester_date = $date->current_date->year.'-08-31';

	$stmt = $db->prepare('SELECT start_date, end_date FROM semester WHERE start_date <= ? AND end_date >= ?');
	$stmt->bind_param('ss', $upcoming_semester_date, $upcoming_semester_date);
	$stmt->execute();
	$stmt->bind_result($start_date, $end_date);

	if (! $stmt->fetch())
	{
		$date->semester = "No semester";
		$stmt->close();
		$db->close();
		echo json_encode($date);
		return;
	}

	$date->semester->start_date = $start_date;
	$date->semester->end_date = $end_date;
	$stmt->close();
	$db->close();
	echo json_encode($date);
?>
