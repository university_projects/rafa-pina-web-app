<?php
	/////////////////////////////////////////
	// Creates an event with the sent data in
	// $_POST. Inserts into event_description,
	// event_schedule and target_audience,
	// logs the activity and if a custom image
	// was sent, stores it.
	/////////////////////////////////////////
    require 'log-activity.inc';
    require 'clear-selected-schedules.inc';
    session_start();

	$db = connect_db();
	$event_information = json_decode($_POST ['event_information']);
    $user_id = $_SESSION ['user_id'];

    //Insert into event description
    $stmt = $db->prepare('INSERT INTO event_description VALUES (NULL, ?, ?, ?, ?, 1, ?, ?, ?)');
    $stmt->bind_param('ssisisi',$event_information->name, $event_information->description, $event_information->turnout, $event_information->image,
                                $user_id, $event_information->responsible, $event_information->equipment_distribution);
    if ($stmt->execute())
    {
        $stmt->close();

        //Get event id
        $stmt= $db->prepare('SELECT LAST_INSERT_ID()');
        $stmt->execute();
		$stmt->bind_result($event_id);
		$stmt->fetch();
		$stmt->close();
		$_SESSION ['event_id'] = $event_id;

        //Insert into target audience
        $stmt = $db->prepare('INSERT INTO target_audience VALUES (?, ?)');
		foreach ($event_information->target_audiences as $audience)
        {
            $stmt->bind_param('ii', $audience, $event_id);
            $stmt->execute();
        }
        $stmt->close();

        //Insert into event schedules
        $event_schedule = json_decode($_SESSION ['selected_schedules']);
        $stmt = $db->prepare('INSERT INTO event_schedule VALUES (NULL, ?, ?, ?, ?, ?)');
		foreach ($event_schedule as $selected_schedule)
        {
            $stmt->bind_param('ssiii', $selected_schedule->start_date, $selected_schedule->end_date, $selected_schedule->start_hour,
    								   $selected_schedule->end_hour, $event_id);
            $stmt->execute();
        }
        $stmt->close();

        //Insert into activity log
        log_activity($db, EVENT_CREATION, $user_id, $event_id);

		// Upload custom file if needed
		if (isset($_FILES ['custom_image']))
		{
			$file_extension = pathinfo($_FILES ['custom_image'] ['name'], PATHINFO_EXTENSION);
			$final_filename = 'uploads/'.$event_id.'.'.$file_extension;
			move_uploaded_file($_FILES ['custom_image'] ['tmp_name'], '../../assets/event-images/'.$final_filename);

			// Update event_description.image
			$stmt = $db->prepare('UPDATE event_description SET image = ? WHERE id = ?');
			$stmt->bind_param('si', $final_filename, $event_id);
			$stmt->execute();
			$stmt->close();
			echo $final_filename;
		}
		else
		{
			echo 'success';
		}
    }
    else
    {
        $stmt->close();
        echo 'error';
    }

    $db->close();
    clear_selected_schedules();
?>
