var start_date_dropdown;
var end_date_dropdown;

var registration_dialog =
{
	init: function(month)
	{
		if (parseInt(month) < 8)
		{
			document.getElementById("start-month-name").innerHTML = "Enero - ";
			document.getElementById("end-month-name").innerHTML = "Julio - ";
		}
		else
		{
			document.getElementById("start-month-name").innerHTML = "Agosto - ";
			document.getElementById("end-month-name").innerHTML = "Diciembre - ";
		}

		start_date_dropdown = new Dropdown("start_date");
		end_date_dropdown = new Dropdown("end_date");

		for (i = 1; i <= 31; i++)
		{
			start_date_dropdown.add_option(i);
			end_date_dropdown.add_option(i);
		}
	},

	show: function(registration_type)
	{
		var form;

		if (registration_type == "semester")
		{
			form = document.forms ["register-semester"];
			document.forms ["register-nick-public-type-job-title"].style.display = "none";
			form.setAttribute("action", "javascript: ;");
		}
		else
		{
			form = document.forms ["register-nick-public-type-job-title"];
			document.forms ["register-semester"].style.display = "none";
			form.setAttribute("action", "javascript: registration_dialog.register('" + registration_type + "')");
		}

		form.reset();
		form.style.display = "block";
		document.getElementById("registration-dialog-background").style.display = "block";

		/// Set dialog title, label's text and button's text
		var dialog_title_element = document.getElementById("registration-dialog-title");
		var label = document.getElementById("registration-dialog-label-for-name");
		if (registration_type == "nick")
		{
			dialog_title_element.innerHTML = "Añadir nick";
			label.innerHTML = "Nick";
			form ["name"].focus();
		}
		else if (registration_type == "public-type")
		{
			dialog_title_element.innerHTML = "Añadir tipo de público";
			label.innerHTML = "Tipo de público";
			form ["name"].focus();
		}
		else if (registration_type == "job-title")
		{
			dialog_title_element.innerHTML = "Añadir puesto laboral";
			label.innerHTML = "Puesto laboral";
			form ["name"].focus();
		}
		else
			dialog_title_element.innerHTML = "Añadir semestre";
	},

	register: function(registration_type)
	{
		var name = document.forms ["register-nick-public-type-job-title"] ["name"].value;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			close_dialog("registration");

			if (this.responseText == "success")
				show_toast("Operación exitosa", BlueToast);
			else if (this.responseText == "already exist")
				show_toast("Lo sentimos, el valor a registrar ya existe", DarkToast);
			else
				show_toast(this.responseText, DarkToast);
		}
		xmlhttp.open("GET", "php/register-in-common-table.php?registration_type=" + registration_type + "&name=" + name);
		xmlhttp.send();
	},

	register_semester: function()
	{
		var start_date = start_date_dropdown.get_selected_value();
		var end_date = end_date_dropdown.get_selected_value();

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			close_dialog("registration");

			if (this.responseText == "success")
			{
				show_toast("Operación exitosa", BlueToast);
				document.getElementById("register-semester-admin-button").style.display = "none";
			}
			else
			{
				show_toast(this.responseText, DarkToast);
			}
		}
		xmlhttp.open("GET", "php/register-semester.php?start_date=" + start_date + "&end_date=" + end_date);
		xmlhttp.send();
	}
};
