var event_image_dialog =
{
    image_containers: [],			/// Dialog image container HTML elements
	custom_image_name: "custom",	/// It's returned by get selected image when selected image is custom
	default_images: 6,				/// It's used to determine if the selected image is custom or not
    selected_image: 0,				/// Current selected image

	init: function()
	{
		this.image_containers = document.getElementsByClassName("event-image-dialog-image-container");
	},

	////////////////////////////////////////
	// Shows the dialog. The first time,
	// inits image_containers
	////////////////////////////////////////
    show: function()
	{
        display("event-image-dialog-background");
    },

    ////////////////////////////////////////
    // Highlights the current selected image
	// in the dialog and changes the event's image
    ////////////////////////////////////////
    select_image: function(index, forced = false)
	{
		if (this.selected_image == index && ! forced)
		{
			close_dialog("event-image");
			return;
		}

		this.image_containers [this.selected_image].style.backgroundColor = "white";
    	this.image_containers [index].style.backgroundColor = "#0b89bf";
    	this.selected_image = index;

		document.getElementById("event-image").src = this.image_containers [this.selected_image].getElementsByTagName("img") [0].src;
		close_dialog("event-image");
	},

	////////////////////////////////////////
	// Sets a custom image. It's called after
	// the user selects an image in the file dialog
	// launched by clicking the upload image
	// button. Verifies that the selected image's size
	// is 900x300 pixels. If the image's size
	// is correct, selects it.
	////////////////////////////////////////
	set_custom_image: function(upload_input)
	{
		var file = upload_input.files [0];

		if (! file)
			return;

		var fileReader = new FileReader();
		fileReader.onload = function()
		{
			var selected_image = new Image();
			selected_image.onload = function()
			{
				if (this.width != 900 || this.height != 300)
				{
					show_toast("La imagen no cumple con el tamaño de 900x300");
					return;
				}

				event_image_dialog.set_custom_container_image(selected_image.src);
			};
			selected_image.src = this.result;
		};
		fileReader.readAsDataURL(file);
	},

	////////////////////////////////////////
	// Sets custom container image. If it
	// doesn't exist, creates it. Also allow
	// set custom image name
	////////////////////////////////////////
	set_custom_container_image: function(image_source, custom_image_name = "custom")
	{
		var custom_image;

		if (this.image_containers.length == this.default_images)
		{
			/// Create the image container if it hasn't been created yet
			var custom_image_container = document.createElement("div");
			custom_image_container.className = "event-image-dialog-image-container";
			custom_image_container.id = "event-image-dialog-custom-image-container";

			custom_image = document.createElement("img");
			custom_image.setAttribute("onclick", "event_image_dialog.select_image(6)");

			custom_image_container.appendChild(custom_image);
			document.getElementById("event-image-dialog-body").appendChild(custom_image_container);
		}
		else
		{
			custom_image = document.getElementById("event-image-dialog-custom-image-container").getElementsByTagName("img") [0];
		}

		custom_image.src = image_source;
		event_image_dialog.select_image(6, true);
		this.custom_image_name = custom_image_name;
	},

	////////////////////////////////////////
	// Tells if the selected image is custom,
	// i.e., uploaded by the user
	////////////////////////////////////////
	is_custom_image: function()
	{
		return this.selected_image == 6;
	},

	////////////////////////////////////////
	// Gets the name of the selected image
	////////////////////////////////////////
    get_selected_image_name: function()
    {
		if (this.is_custom_image())
			return this.custom_image_name;

		return this.image_containers [this.selected_image].getElementsByTagName("img") [0].src.replace(/^.*[\\\/]/, '');
    }
};
