//////////////////////////////////////////////
// Converts a schedule in event-schedule notation
// to a string with the following structure:
// "Del " start_date " de " start_month " al "
// end_date " de " end_month " del " year
//////////////////////////////////////////////
function schedule_date_to_string(schedule)
{
	/// The dates
	if (schedule.start_date == schedule.end_date)
	{
		var date = new MyDate();
		date.from_string(schedule.start_date);
		return date.get_day_name() + " " + date.date + " de " + date.get_month_name() + " del " + date.year;
	}

	var start_date = new MyDate();
	start_date.from_string(schedule.start_date);
	var end_date = new MyDate();
	end_date.from_string(schedule.end_date);

	if (start_date.month == end_date.month)
	{
		return "Del " + start_date.get_day_name() + " " + start_date.date + " al " + end_date.get_day_name() + " " +
			   end_date.date + " de " + start_date.get_month_name() + " del " + start_date.year;
	}

	return "Del " + start_date.get_day_name() + " " + start_date.date + " de " + start_date.get_month_name() +
		   " al " + end_date.get_day_name() + " " + end_date.date + " de " + end_date.get_month_name() + " del " + start_date.year;
}

//////////////////////////////////////////////
// Converts a schedule's time in event-schedule
// notation to a string with the following structure:
// "De " start_hour ":00 a" end_hour ":00 horas"
//////////////////////////////////////////////
function schedule_time_to_string(schedule)
{
	return "De " + schedule.start_hour + ":00 a " + (schedule.end_hour + 1) + ":00 horas";
}

//////////////////////////////////////////////
// Converts a schedule in event-schedule notation
// to a array of SelectedSchedule objects
//////////////////////////////////////////////
function event_schedule_to_selected_schedules(event_schedule)
{
	var selected_schedules = [];
	var date = new MyDate();
	var end_date = new MyDate();
	date.from_string(event_schedule.start_date);
	end_date.from_string(event_schedule.end_date);
	var end_hour = parseInt(event_schedule.end_hour);

	while (date.is_minor_equal(end_date))
	{
		var selected_schedule = new SelectedSchedule(date.year, date.month, date.date);
		selected_schedules.push(selected_schedule);

		for (var schedule = parseInt(event_schedule.start_hour); schedule <= end_hour; schedule++)
			selected_schedule.schedules.push(schedule);

		date.move_date(1);
	}

	return selected_schedules;
}
