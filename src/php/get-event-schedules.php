<?php
    /////////////////////////////////////////
    /// Gets the schedules of the event.
    /// If not event_id is passed in $_GET,
    /// it tries to return the selected schedules
    /// of the user, i.e., the selected schedules
	/// which where selected by the user in the calendar.
    /////////////////////////////////////////
    require 'connect-db.inc';

    session_start();
    if (! isset($_SESSION ['event_id']) && ! isset($_SESSION ['user_id']))
    {
        echo 'no data';
        return;
    }

	/// When the user is creating an event
    if (! isset($_SESSION ['event_id']))
    {
		echo json_encode(json_decode($_SESSION ['selected_schedules']));
		return;
	}

	/// When the user is just seeing event's information
    $event_id = $_SESSION ['event_id'];
	$db = connect_db();

    $stmt = $db->prepare('SELECT start_date, end_date, start_hour, end_hour FROM event_schedule WHERE event_description_id = ?');
    $stmt->bind_param('i', $event_id);
    $stmt->execute();
    $stmt->bind_result($start_date, $end_date, $start_hour, $end_hour);

	$event_schedules = array();
    while ($stmt->fetch())
	{
        array_push($event_schedules, array('start_date' => $start_date, 'end_date' => $end_date, 'start_hour' => $start_hour,
				   									 'end_hour' => $end_hour));
	}

    $stmt->close();
    $db->close();
    echo json_encode($event_schedules);
?>
