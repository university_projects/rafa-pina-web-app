<?php
	/////////////////////////////////////////
	/// Requests reset password. Logs the
	/// REQUESTED_PASSWORD_RESET for the applicant
	/// and PASSWORD_RESET_REQUEST for admins.
	///
	/// If the request had already been made,
	/// returns 'request already registered'
	/////////////////////////////////////////
	require 'connect-db.inc';
	require 'log-activity.inc';

	$db = connect_db();
	$rfc_email = $_POST ['rfc_email'];

	/// Retrieve applicant id
	$stmt = $db->prepare('SELECT id FROM user WHERE rfc = ? OR email = ?');
	$stmt->bind_param('ss', $rfc_email, $rfc_email);
	$stmt->execute();
	$stmt->bind_result($applicant_id);

	if (! $stmt->fetch())
	{
		echo 'wrong data';
		$stmt->close();
		$db->close();
		return;
	}

	$stmt->close();

	/// Verify that the request hasn't been made yet
	$stmt = $db->prepare('SELECT COUNT(*) FROM activity WHERE type = 52 AND state = 1 AND applicant_id = ?');
	$stmt->bind_param('i', $applicant_id);
	$stmt->execute();
	$stmt->bind_result($request_already_registered);
	$stmt->fetch();

	if (! $request_already_registered)
	{
		$stmt->close();
		log_activity($db, REQUESTED_PASSWORD_RESET, $applicant_id, NULL);
		log_activity($db, PASSWORD_RESET_REQUEST, $applicant_id, NULL);
		echo 'success';
	}
	else
		echo 'request already registered';

	$db->close();

?>
