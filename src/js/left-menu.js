var left_menu_top = null;

function show_left_menu()
{
	var left_menu = document.getElementById("left-menu-modal");

	if (! left_menu_top)
	{
		left_menu_top = document.getElementById("nav-bar-mobile").offsetHeight;
		left_menu.style.top = left_menu_top + "px";
	}

	left_menu.style.display = "block";
}

function close_left_menu()
{
	var left_menu = document.getElementById("left-menu");
	left_menu.className += " hide";

	setTimeout(function()
	{
		left_menu.className = left_menu.className.replace("hide", "");
		document.getElementById("left-menu-modal").style.display = "none";
	}, 300);
}
