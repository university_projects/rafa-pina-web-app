/// NOTE: This file must be included after user.js

var redirection_reason = "manual";		/// Tells if the user is redirecting manually or if he's beign redirecting by the web app

/////////////////////////////////////////
/// Redirects to url
/////////////////////////////////////////
function redirect(url, reason = "manual")
{
	redirection_reason = reason;
	document.location.href = url;
}

/////////////////////////////////////////
/// If there are unsaved changes, request
/// confirmation to the user before
/// to redirect.
/////////////////////////////////////////
window.onbeforeunload = function(event)
{
	if (redirection_reason == "manual" &&
	   (user_control.state == user_control_state.REGISTERING ||
	    (user_control.state == user_control_state.MODIFYING_PROFILE && user_control.changes_in_tab())))
		event.returnValue = "ask";
}

/////////////////////////////////////////
/// Clear the variables of $_SESSION used
/// by this module in order to avoid that
/// the user can come back
/////////////////////////////////////////
window.onunload = function()
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;
	}
	xmlhttp.open("GET", "php/get-out-of-user-control.php");
	xmlhttp.send();
}

/////////////////////////////////////////
/// Stores the id of the event in $_SESSION ['event-id']
/// and redirects to event.html
///
/// If the user is registering or if it's
/// modifying a profile and has unsaved changes,
/// launches a confirm dialog for redirecting confirmation
/////////////////////////////////////////
function go_to_event(event_id)
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;

		redirect("event.html");
	};
	xmlhttp.open("POST", "php/go-to-event.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	if (user_control.state == user_control_state.REGISTERING ||
	    (user_control.state == user_control_state.MODIFYING_PROFILE && user_control.changes_in_tab()))
	{
		confirm_dialog.on_accept = function() { xmlhttp.send("event-id=" + event_id); };
		confirm_dialog.show("Redireccionando", "Los cambios sin guardar se perderán. ¿Desea continuar?");
	}
	else
	{
		xmlhttp.send("event-id=" + event_id);
	}
}

/////////////////////////////////////////
/// Posible states of the user control module
/////////////////////////////////////////
const user_control_state =
{
	NOTHINS: 0,
	REGISTERING: 1,
	MODIFYING_PROFILE: 2
};

var job_title_dropdown = null;							///< Dropdown to select the job title
var nick_dropdown = null;								///< Dropdown to select the nick
var is_admin_toggle_button = null;						///< ToggleButton for the is_admin property
var notifications_by_email_toggle_button = null;		///< ToggleButton to enable/disable notifications by email

/////////////////////////////////////////
//7 User control module
/////////////////////////////////////////
var user_control =
{
	state: user_control_state.NOTHING,	///< State of the module
	form_tabs_controller: null,			///< TabController to handle the tabs of the form
	user_data_backup: null,				///< Backup of the user which is being modified

	/////////////////////////////////////////
	/// Inits the module.
	/// Sets the callback for on_log_out and
	/// gets module's state
	/////////////////////////////////////////
	init: function()
	{
		nav_bar.on_log_out = function()
		{
			confirm_dialog.on_accept = function()
			{
				nav_bar.on_log_out = function() { redirect("homepage.html", "internal"); return true; }
				nav_bar.log_out();
			}

			if (user_control.state == user_control_state.REGISTERING)
			{
				confirm_dialog.show("Cerrando sesión", "El registro no se ha completado, al cerrrar la sesión se perderá el avance. " +
									"¿Desea continuar?");
				return false;
			}

			if (user_control.state == user_control_state.MODIFYING_PROFILE && user_control.changes_in_tab())
			{
				confirm_dialog.show("Cerrando sesión", "Hay cambios sin guardar, al cerrar sesión estos se perderán. ¿Desea cerrar sesión?");
				return false;
			}

			redirect("homepage.html", "internal");
			return true;
		}

		/// Get module's state
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			/// Shouldn't be here??
			if (this.responseText == "get out of here")
			{
				redirect("homepage.html", "internal");
				return;
			}

			/// Nothing state
			if (this.responseText == "0")
			{
				user_control.set_state(user_control_state.NOTHING);
				user_control.form_tabs_controller = new TabController();
				return;
			}

			/// If the module has been init, returns
			if (user_control.form_tabs_controller)
				return;

			user_control.set_state(user_control_state.MODIFYING_PROFILE, true, parseInt(this.responseText));

			if (user.type == "admin")
			{
				user_control.form_tabs_controller = new TabController();
			}
			else
			{
				user_control.form_tabs_controller = new TabController(0, 2);

				conceal("working-info-tab");
				conceal("is_admin-toggle-button");
				replace_class("personal-info-tab", "scol4", "scol6");
				replace_class("account-info-tab", "scol4", "scol6");
				add_class("account-info-tab", "last-tab");
				conceal("come-back-button");
			}
		}
		xmlhttp.open("GET", "php/get-user-control-state.php");
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// Sets the state of the module.
	///
	/// Params:
	/// forced Tells if it's necessary look for
	/// unsaved changes
	/// user_id Is used to fill the form if the
	/// state is MODIFYING_PROFILE
	/////////////////////////////////////////
	set_state: function(state, forced = false, user_id = -1)
	{
		/// If the change is not forced, look for unsaved changes, if there are, launch a confirm dialog
		if (! forced && state == user_control_state.NOTHING)
		{
			switch (this.state)
			{
				case user_control_state.REGISTERING:
					confirm_dialog.on_accept = function() { user_control.set_state(state, true); }
					confirm_dialog.show("Cancelar registro", "El registro de usuario no se ha completado, ¿Desea cancelarlo?");
				return;

				case user_control_state.MODIFYING_PROFILE:
					if (this.changes_in_tab())
					{
						confirm_dialog.on_accept = function()
						{
							user_control.user_data_backup = null;
							user_control.set_state(state, true);
						}
						confirm_dialog.show("Cambios sin guardar", "¿Desea descartar los cambios realizados?");
						return;
					}
			}
		}

		/// Set the state
		this.state = state;
		var form = document.forms ["user-data-form"];

		/// Hide/Show the elements which are part or not of the new state
		switch (this.state)
		{
			case user_control_state.NOTHING:
				form ["name"].setCustomValidity('');
				form ["father-last-name"].setCustomValidity('');
				form ["mother-last-name"].setCustomValidity('');
				form ["rfc"].setCustomValidity('');
				form ["email"].setCustomValidity('');
				form.reset();

				conceal("main-form");
				display("state-selector-section");
			return;

			case user_control_state.REGISTERING:
				this.select_tab(0);
				document.forms ["user-data-form"].setAttribute("action", "javascript: user_control.register_user()");

				conceal("discard-changes-button");
				conceal("save-changes-button");
				conceal("state-selector-section");
				conceal("change-password-button");
				display("main-form");
			return;

			case user_control_state.MODIFYING_PROFILE:
				if (user.type == "admin")
				{
					/// Verify is_admin_toggle_button is off
					if (! is_admin_toggle_button)
					{
						is_admin_toggle_button = new ToggleButton("is_admin");
						is_admin_toggle_button.on_toggle.connect(function() { user_control.show_changes_buttons(); });
					}
					else if (is_admin_toggle_button.is_on())
					{
						is_admin_toggle_button.toggle();
					}

					if (! job_title_dropdown)
						this.get_job_titles_and_nicks();
				}

				/// Verify notifications_by_email_toggle_button is off
				if (! notifications_by_email_toggle_button)
				{
					notifications_by_email_toggle_button = new ToggleButton("notifications_by_email");
					notifications_by_email_toggle_button.on_toggle.connect(function() { user_control.show_changes_buttons(); });
				}
				else if (notifications_by_email_toggle_button.is_on())
				{
					notifications_by_email_toggle_button.toggle();
				}

				conceal("state-selector-section");
				conceal("prev-form-section-button");
				conceal("next-form-section-button");
				conceal("register-user-button");

				conceal(document.forms ["user-data-form"] ["password"]);
				conceal("password-label");
				conceal("password-comment");

				display("main-form");
				document.forms ["user-data-form"].setAttribute("action", "javascript: user_control.save_changes()");
				this.fill_form(user_id);
			return;
		}
	},

	/////////////////////////////////////////
	/// Select a tab of the form.
	///
	/// Show/Hide elements of the current and the
	/// new tab. Also, look for changes and do
	/// something according to the state.
	///
	/// This function is called by clicking on
	/// the tab to be selected or by user_control.move_tab
	/////////////////////////////////////////
	select_tab: function(index)
	{
		/// If index out of scope
		if (index < 0 || index > 2)
			return;

		switch (this.state)
		{
			case user_control_state.REGISTERING:
				// is going to the next step and the current tab is invalid
				if (index > this.form_tabs_controller.selected_index && ! this.validate_tab(this.form_tabs_controller.selected_index))
					return;

				switch (index)
				{
					case 0:
						conceal("register-user-button");
						hide("prev-form-section-button");
						show("next-form-section-button");
						this.form_tabs_controller.select_tab(index);
					return;

					case 1:
						if (! is_admin_toggle_button)
							is_admin_toggle_button = new ToggleButton("is_admin");

						if (! notifications_by_email_toggle_button)
						{
							notifications_by_email_toggle_button = new ToggleButton("notifications_by_email");
							notifications_by_email_toggle_button.toggle();
						}

						conceal("register-user-button");
						show("prev-form-section-button");
						show("next-form-section-button");
						this.form_tabs_controller.select_tab(index);
					return;

					case 2:
						if (! job_title_dropdown)
							this.get_job_titles_and_nicks();

						show("prev-form-section-button");
						conceal("next-form-section-button");
						display("register-user-button");
						this.form_tabs_controller.select_tab(index);
				}
			return;

			case user_control_state.MODIFYING_PROFILE:
				if (this.form_tabs_controller.selected_index != null && this.changes_in_tab())
				{
					confirm_dialog.on_accept = function()
					{
						user_control.discard_tab_changes();
						user_control.select_tab(index);
					}

					confirm_dialog.show("Cambios sin guardar", "Hay cambios sin guardar en la sección actual, ¿Desea continuar?");
					return;
				}

				this.form_tabs_controller.select_tab(index);
			return;
		}
	},

	/////////////////////////////////////////
	/// Moves the current tab to the left or
	/// to the right.
	///
	/// This function is called by clicking
	/// the prev and next tab buttons
	/////////////////////////////////////////
	move_tab: function(offset)
	{
		this.select_tab(this.form_tabs_controller.selected_index + offset);
	},

	/////////////////////////////////////////
	/// Verifies that a given tab is valid
	///
	/// Returns true if it's valid, false otherwise.
	/// This function is used while selecting
	/// a tab in the REGISTERING state.
	/////////////////////////////////////////
	validate_tab: function(index)
	{
		var form = document.forms ["user-data-form"];
		var valid = true;

		switch (index)
		{
			case 0:
				valid = form ["name"].checkValidity();
				valid = form ["father-last-name"].checkValidity();
				valid = form ["mother-last-name"].checkValidity();
			return valid;

			case 1:
				valid = form ["rfc"].checkValidity();
				valid = form ["email"].checkValidity();
			return valid;

			case 2: return true;
		}
	},

	/////////////////////////////////////////
	/// Tells if there are changes in a given
	/// tab.
	///
	/// Returns true if there are, false otherwise.
	///
	/// This function is used to determine if it's
	/// necessary or not launch a confirm dialog
	/// while selecting a tab in MODIFYING_PROFILE state.
	/////////////////////////////////////////
	changes_in_tab: function(index = -1)
	{
		if (index == -1)
			index = this.form_tabs_controller.selected_index;

		var form = document.forms ["user-data-form"];

		switch (index)
		{
			case 0:
				return this.user_data_backup.name != form ["name"].value ||
					   this.user_data_backup.father_last_name != form ["father-last-name"].value ||
					   this.user_data_backup.mother_last_name != form ["mother-last-name"].value;

			case 1:
				if (user.type != "admin")
					return this.user_data_backup.rfc != form ["rfc"].value ||
						   this.user_data_backup.email != form ["email"].value ||
						   this.user_data_backup.notifications_by_email != notifications_by_email_toggle_button.is_on();

				return this.user_data_backup.rfc != form ["rfc"].value ||
					   this.user_data_backup.email != form ["email"].value ||
					   this.user_data_backup.is_admin != is_admin_toggle_button.is_on() ||
					   this.user_data_backup.notifications_by_email != notifications_by_email_toggle_button.is_on();

			case 2:
				if (user.type != "admin")
					return false;

				return this.user_data_backup.job_title.id != job_title_dropdown.get_selected_option().data ||
					   this.user_data_backup.nick.id != nick_dropdown.get_selected_option().data;
		}
	},

	/////////////////////////////////////////
	/// Restores the data (using user_data_backup)
	/// of a given tab.
	///
	/// This function is called by clicking the
	/// discard changes button or while a user
	/// is selecting a tab and the current one has
	/// changes.
	/////////////////////////////////////////
	discard_tab_changes: function(index = -1)
	{
		if (index == -1)
			index = this.form_tabs_controller.selected_index;

		if (! this.changes_in_tab(index))
		{
			show_toast("No hay cambios");
			return;
		}

		var form = document.forms ["user-data-form"];

		switch (index)
		{
			case 0:
				form ["name"].value = this.user_data_backup.name;
				form ["father-last-name"].value = this.user_data_backup.father_last_name;
				form ["mother-last-name"].value = this.user_data_backup.mother_last_name;
			break;

			case 1:
				form ["rfc"].value = this.user_data_backup.rfc;
				form ["email"].value = this.user_data_backup.email;

				if (notifications_by_email_toggle_button.is_on() != this.user_data_backup.notifications_by_email)
					notifications_by_email_toggle_button.toggle();

				if (user.type == "admin" && is_admin_toggle_button.is_on() != this.user_data_backup.is_admin)
					is_admin_toggle_button.toggle();
			break;

			case 2:
				if (job_title_dropdown.get_selected_option().data != this.user_data_backup.job_title.id)
					job_title_dropdown.select_option_by_data(this.user_data_backup.job_title.id);

				if (nick_dropdown.get_selected_option().data != this.user_data_backup.nick.id)
					nick_dropdown.select_option_by_data(this.user_data_backup.nick.id);
		}

		show_toast("Cambios descartados", BlueToast);
	},

	/////////////////////////////////////////
	/// Retrieves the job titles and nicks from
	/// the server.
	///
	/// With them, fills the dropdowns.
	/////////////////////////////////////////
	get_job_titles_and_nicks: function()
	{
		if (user.type != "admin")
			return;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var response = JSON.parse(this.responseText);

			/// Fill job_title_dropdown
			job_title_dropdown = new Dropdown("job_title");
			var job_titles = response.job_titles;

			for (var i = 0; i < job_titles.length; i++)
			{
				var job_title = job_titles [i];
				job_title_dropdown.add_option(job_title.name, job_title.id);
			}

			job_title_dropdown.add_editable_option("Añadir puesto laboral", "-1");
			job_title_dropdown.on_option_changed.connect(function() { user_control.show_changes_buttons(); });

			/// Fill nick_dropdown
			nick_dropdown = new Dropdown("nick");
			var nicks = response.nicks;

			for (var i = 0; i < nicks.length; i++)
			{
				var nick = nicks [i];
				nick_dropdown.add_option(nick.name, nick.id);
			}

			nick_dropdown.add_editable_option("Añadir carrera profesional", "-1");
			nick_dropdown.on_option_changed.connect(function() { user_control.show_changes_buttons(); });
		}
		xmlhttp.open("GET", "php/get-job-titles-and-nicks.php");
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// Register an user in the server. Admin-Only.
	/////////////////////////////////////////
	register_user: function()
	{
		if (user.type != "admin")
			return;

		var form = document.forms ["user-data-form"];
		var user_data = new Object();

		/// Personal data
		user_data.name = form ["name"].value;
		user_data.father_last_name = form ["father-last-name"].value;
		user_data.mother_last_name = form ["mother-last-name"].value;

		/// Account data
		user_data.rfc = form ["rfc"].value;
		user_data.email = form ["email"].value;
		user_data.notifications_by_email = notifications_by_email_toggle_button.is_on();
		user_data.is_admin = is_admin_toggle_button.is_on();

		/// Working data
		user_data.job_title = new Object();
		var selected_job_title = job_title_dropdown.get_selected_option();
		user_data.job_title.id = selected_job_title.data;
		user_data.job_title.name = selected_job_title.value;

		user_data.nick = new Object();
		var selected_nick = nick_dropdown.get_selected_option();
		user_data.nick.id = selected_nick.data;
		user_data.nick.name = selected_nick.value;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == "success")
				show_toast("Usuario registrado", BlueToast);
			else
				show_toast(this.responseText, DarkToast);

			user_control.set_state(user_control_state.NOTHING, true);
		}
		xmlhttp.open("POST", "php/register-user.php");
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("user_data=" + JSON.stringify(user_data));
	},

	/////////////////////////////////////////
	/// Fills the form with the data which corresponds
	/// to user_id.
	/////////////////////////////////////////
	fill_form: function(user_id)
	{
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var user_data = JSON.parse(this.responseText);
			user_control.user_data_backup = JSON.parse(this.responseText);

			var form = document.forms ["user-data-form"];
			form ["name"].value = user_data.name;
			form ["father-last-name"].value = user_data.father_last_name;
			form ["mother-last-name"].value = user_data.mother_last_name;
			form ["rfc"].value = user_data.rfc;
			form ["email"].value = user_data.email;

			if (user_data.notifications_by_email)
				notifications_by_email_toggle_button.toggle();

			if (user_data.own_profile)
				display("change-password-button");

			if (user.type != "admin")
			{
				user_control.select_tab(0);
				return;
			}

			if (user_data.is_admin)
				is_admin_toggle_button.toggle();

			job_title_dropdown.select_option_by_data(user_data.job_title.id);
			nick_dropdown.select_option_by_data(user_data.nick.id);
			user_control.select_tab(0);
		}
		xmlhttp.open("GET", "php/get-user-data.php?user_id=" + user_id);
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// Save the changes mades to user_data_backup.
	/////////////////////////////////////////
	save_changes: function(tab_index = -1)
	{
		if (tab_index == -1)
			tab_index = this.form_tabs_controller.selected_index;

		if (! this.changes_in_tab(tab_index))
		{
			show_toast("No hay cambios", DarkToast);
			return;
		}

		if (! this.validate_tab(tab_index))
			return;

		var form = document.forms ["user-data-form"];
		switch (tab_index)
		{
			case 0:
				this.user_data_backup.name = form ["name"].value;
				this.user_data_backup.father_last_name = form ["father-last-name"].value;
				this.user_data_backup.mother_last_name = form ["mother-last-name"].value;
			break;

			case 1:
				this.user_data_backup.rfc = form ["rfc"].value;
				this.user_data_backup.email = form ["email"].value;
				this.user_data_backup.notifications_by_email = notifications_by_email_toggle_button.is_on();

				if (user.type == "admin")
					this.user_data_backup.is_admin = is_admin_toggle_button.is_on();
			break;

			case 2:
				if (user.type != "admin")
					break;

				this.user_data.job_title.id = job_title_dropdown.get_selected_option().data;
				this.user_data.job_title.name = job_title_dropdown.get_selected_value();

				this.user_data.nick.id = nick_dropdown.get_selected_option().data;
				this.user_data.nick.name = nick_dropdown.get_selected_value();
		}

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText != "success")
			{
				show_toast(this.responseText, DarkToast);
				return;
			}

			show_toast("Cambios guardados", BlueToast);
			user.load()
		}
		xmlhttp.open("GET", "php/modify-user.php?user_data=" + JSON.stringify(this.user_data_backup));
		xmlhttp.send();
	},

	is_rfc_registered: function()
	{
		var rfc = document.forms ["user-data-form"] ["rfc"];

		if(	user_control.state == user_control_state.MODIFYING_PROFILE && user_control.user_data_backup.rfc == rfc.value)
		{
			show_toast("Este rfc pertenece al usuario actual", DarkToast);
			return;
		}

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (parseInt(this.responseText))
			{
				rfc.value = "";
				document.forms ["user-data-form"] ["password"].value = "";
				show_toast("Este rfc ya se encuentra registrado", DarkToast);
			}
		}
		xmlhttp.open("GET", "php/is-rfc-registered.php?rfc=" + rfc.value);
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// Shows the discard-changes-button
	/// and the save-changes-button if the curren
	/// tab has changes, otherwise hides them.
	/// MODIFYING_PROFILE state.
	/////////////////////////////////////////
	show_changes_buttons: function()
	{
		if (user_control.state != user_control_state.MODIFYING_PROFILE)
			return;

		if (this.changes_in_tab())
		{
			display("discard-changes-button");
			display("save-changes-button");
		}
		else
		{
			conceal("discard-changes-button");
			conceal("save-changes-button");
		}
	}
};

user.onload.connect(function() { user_control.init(); });
