var event_schedules_dialog =
{
	//////////////////////////////////////////////
	/// Shows the dialog. Clears the schedules area
	/// and then fills it with the selected schedules.
	//////////////////////////////////////////////
	show: function()
	{
		remove_child_nodes("event-schedules-area");
		display("event-schedules-dialog-background");

		var event_schedules_area = document.getElementById("event-schedules-area");
		var event_schedules = calendar.group_selected_schedules();

		for (var i = 0; i < event_schedules.length; i++)
		{
			var event_schedule = event_schedules [i];

			var event_schedule_element = document.createElement("div");
			event_schedule_element.className = "scol12 zebra-row";

			var event_schedule_text = document.createElement("div");
			event_schedule_text.className = "scol11";
			event_schedule_text.setAttribute("data-event-schedule", JSON.stringify(event_schedule));

			var event_schedule_date = document.createElement("span");
			event_schedule_date.innerHTML = schedule_date_to_string(event_schedule);

			var event_schedule_time = document.createElement("span");
			event_schedule_time.innerHTML = schedule_time_to_string(event_schedule);

			event_schedule_text.appendChild(event_schedule_date);
			event_schedule_text.appendChild(document.createElement("br"));
			event_schedule_text.appendChild(event_schedule_time);

			var event_schedule_delete_button = document.createElement("span");
			event_schedule_delete_button.className = "scol1 icon-cancel zebra-row-close-button";
			event_schedule_delete_button.setAttribute("onclick", "event_schedules_dialog.deselect_schedule(" + i + ")");

			event_schedule_element.appendChild(event_schedule_text);
			event_schedule_element.appendChild(event_schedule_delete_button);
			event_schedules_area.appendChild(event_schedule_element);
		}
	},

	//////////////////////////////////////////////
	/// Deselects a schedule in event-schedule
	/// notation. Removes the event schedule element
	/// from event-scheduules-area and calls to
	/// calendar.deselect_event_schedule().
	//////////////////////////////////////////////
	deselect_schedule: function(schedule_index)
	{
		var event_schedules_area = document.getElementById("event-schedules-area");
		var schedule_element = event_schedules_area.childNodes [schedule_index].firstChild;

		calendar.deselect_event_schedule(JSON.parse(schedule_element.getAttribute("data-event-schedule")));

		/// Delete the event schedule element from the event schedules area of the dialog
		event_schedules_area.removeChild(event_schedules_area.childNodes [schedule_index]);

		if (! event_schedules_area.hasChildNodes())
		{
			close_dialog("event-schedules");
		}
		else
		{
			for (var i = 0; i < event_schedules_area.childNodes.length; i++)
			{
				event_schedules_area.childNodes [i].childNodes [1].setAttribute("onclick",
																				"event_schedules_dialog.deselect_schedule(" + i + ")");
			}
		}
	}
};
