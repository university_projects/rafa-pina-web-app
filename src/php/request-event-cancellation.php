<?php
	////////////////////////////////////////////////
	// Verifies that current user is the creator of
	// current event. If it's not returns 'error'.
	//
	// Sets event state as 6, logs REQUESTED_EVENT_CANCELLATION
	// activity for user and EVENT_CANCELLATION_REQUEST
	// for admins. Returns 'ok'
	////////////////////////////////////////////////
	require 'connect-db.inc';
	require 'log-activity.inc';
	session_start();

	$user_id = $_SESSION ['user_id'];
	$event_id = $_SESSION ['event_id'];
	$db = connect_db();

	// Verify that user is creator of event
	$stmt = $db->prepare('SELECT COUNT(*) FROM event_description WHERE id = ? AND user_id = ?');
	$stmt->bind_param('ii', $event_id, $user_id);
	$stmt->execute();
	$stmt->bind_result($result);
	$stmt->fetch();
	$stmt->close();

	if ($result == '0')
	{
		$db->close();
		echo 'error';
		return;
	}

	/// Update event_description
	$stmt = $db->prepare('UPDATE event_description SET state = 6 WHERE id = ?');
	$stmt->bind_param('i', $event_id);
	$stmt->execute();
	$stmt->close();

	log_activity($db, REQUESTED_EVENT_CANCELLATION, $user_id, $event_id);
	log_activity($db, EVENT_CANCELLATION_REQUEST, $user_id, $event_id);
	$db->close();

	echo 'ok';
?>
