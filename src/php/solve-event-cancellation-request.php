<?php
	////////////////////////////////////////////////
	// Verifies that request has not been solved yet.
	// If so, returns 'no request'.
	//
	// According to $_POST ['cancel'], if it's true
	// cancels the event, logs EVENT_CANCELLATION activity
	// and returns 'cancelled'. If it's false, sets event's
	// state as normal, logs REJECTED_EVENT_CANCELLATION
	// activity and returns 'rejected'.
	////////////////////////////////////////////////
	require 'connect-db.inc';
	require 'log-activity.inc';
	require 'solve-activity.inc';
	session_start();

	if (! $_SESSION ['user_is_admin'])
	{
 		echo '>:(';
 		return;
	}

	$applicant_id = $_POST ['applicant_id'];
	$event_id = $_POST ['event_id'];
	$db = connect_db();

 	/// Verify that the applicant request is not solved
 	$stmt= $db->prepare('SELECT id FROM activity WHERE applicant_id = ? AND event_id = ? AND state = 1 AND type = 51');
 	$stmt->bind_param('ii', $applicant_id, $event_id);
 	$stmt->execute();
 	$stmt->bind_result($activity_id);

 	if (! $stmt->fetch())
 	{
 		$stmt->close();
 		$db->close();
 		echo 'no request';
 		return;
 	}

	$stmt->close();

	/// Solve the request
	if ($_POST ['cancel'] == 'true')
	{
		/// Cancel the event
		log_activity($db, EVENT_CANCELLATION, $applicant_id, $event_id);
		$stmt = $db->prepare('UPDATE event_description SET state = 7 WHERE id = ?');
		echo 'cancelled';
	}
	else
	{
		log_activity($db, REJECTED_EVENT_CANCELLATION, $applicant_id, $event_id);
		$stmt = $db->prepare('UPDATE event_description SET state = 1 WHERE id = ?');
		echo 'rejected';
	}

	$stmt->bind_param('i', $event_id);
	$stmt->execute();
	$stmt->close();

	solve_activity($db, $activity_id, $_SESSION ['user_id']);
	$db->close();
?>
