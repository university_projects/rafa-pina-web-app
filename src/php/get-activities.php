<?php
	////////////////////////////////////////
	// Gets all the activities which belong
	// to the user with the given id
	////////////////////////////////////////
	require 'connect-db.inc';
	require 'get-activities.inc';
	session_start();

	$user_id = $_SESSION ['user_id'];
	$db = connect_db();
	echo json_encode(get_activities($db, $user_id, $_SESSION ['user_is_admin']));
	$db->close();
?>
