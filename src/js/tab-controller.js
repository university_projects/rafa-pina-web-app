function TabController(first_index = 0, tab_number = -1)
{
	this.tabs = [];
	this.tabs_content = [];
	this.selected_index = null;

	this.init = function()
	{
		var tab_elements = document.getElementsByClassName("tab");
		var tab_content_elements = document.getElementsByClassName("tab-content");
		tab_number = (tab_number == -1) ? tab_elements.length : tab_number;

		for (var i = first_index; i < first_index + tab_number; i++)
		{
			this.tabs.push(tab_elements [i]);
			this.tabs_content.push(tab_content_elements [i]);
		}
	}

	this.select_tab = function(index)
	{
		if (this.selected_index == index || index < 0 || index >= this.tabs.length)
			return;

		if (this.selected_index != null)
		{
			replace_class(this.tabs [this.selected_index], "active-tab", "tab");
			this.tabs_content [this.selected_index].style.display = "none";
		}

		this.selected_index = index;

		replace_class(this.tabs [this.selected_index], "tab", "active-tab");
		this.tabs_content [this.selected_index].style.display = "block";
	}

	this.init();
}
