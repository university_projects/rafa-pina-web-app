var change_password_dialog =
{
	/////////////////////////////////////////
	/// Shows the dialog, resets the form
	/// and sets focus to current-password field
	/////////////////////////////////////////
	show: function()
	{
		display("change-password-dialog-background");

		var form = document.forms ["change-password-form"];
		form.reset();

		form ["current-password"].setCustomValidity('');
		form ["new-password"].setCustomValidity('');
		form ["confirm-new-password"].setCustomValidity('');
		form ["current-password"].focus();
	},

	/////////////////////////////////////////
	/// Verifies that new-password and confirm-new-password
	/// fields' content is the same. If it's not
	/// the same, clear both fieldsm launches a toast
	/// and sets focus on new-password field
	/////////////////////////////////////////
	verify_confirm_new_password: function()
	{
		var form = document.forms ["change-password-form"];
		var new_password_element = form ["new-password"];
		var confirm_new_password_element = form ["confirm-new-password"];

		if (new_password_element.value != confirm_new_password_element.value)
		{
			new_password_element.value = "";
			confirm_new_password_element.value = "";
			show_toast("Las contraseñas no coinciden", DarkToast);
			new_password_element.focus();
			return false;
		}

		return true;
	},

	/////////////////////////////////////////
	/// Sends the form's data to change-password.php
	/////////////////////////////////////////
	change_password: function()
	{
		var form = document.forms ["change-password-form"];

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == "wrong current password")
			{
				show_toast("La contraseña actual es incorrecta", DarkToast);
				change_password_dialog.show();
			}
			else if (this.responseText == "success")
			{
				show_toast("Contraseña cambiada", BlueToast);
				close_dialog("change-password");
			}
			else
			{
				show_toast(this.responseText, DarkToast);
				close_dialog("change-password");
			}
		}
		xmlhttp.open("POST", "php/change-password.php");
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("current_password=" + form ["current-password"].value +
					 "&new_password=" + form ["new-password"].value);
	}
};
