const RedirectionReason =
{
	Manual: 0,
	Internal: 1
};

var redirection_reason = RedirectionReason.Manual;

/////////////////////////////////////////
/// If there are selected schedules
/// launches a browser dialog for manual redirecting
/// confirmation.
/////////////////////////////////////////
window.onbeforeunload = function(event)
{
	if (redirection_reason == RedirectionReason.Manual && calendar.are_there_selected_schedules())
		event.returnValue = "ask";
}

/////////////////////////////////////////
/// Redirects to another web page. It's used
/// internally and if it's a manual redirection
/// (i.e., the user clicks over the home
/// icon or something like that), launches
/// a dialog for redirecting confirmation
/////////////////////////////////////////
function redirect(url)
{
	if (redirection_reason == RedirectionReason.Manual && calendar.are_there_selected_schedules())
	{
		confirm_dialog.on_accept = function()
		{
			redirection_reason = RedirectionReason.Internal;
			calendar.deselect_selected_schedules();
			document.location.href = url;
		}
		confirm_dialog.show("Redireccionando", "Los horarios seleccionados se perderán. ¿Desea continuar?");
		return;
	}

	document.location.href = url;
}

/////////////////////////////////////////
/// This function is part of the logout process.
/// If there are selected schedules, launches a
/// dialog for logging out confirmation
/////////////////////////////////////////
function booking_calendar_log_out()
{
	if (calendar.are_there_selected_schedules())
	{
		confirm_dialog.on_accept = function()
		{
			nav_bar.on_log_out = function() { return true; }
			nav_bar.log_out();
			nav_bar.on_log_out = function() { return booking_calendar_log_out(); }
		}
		confirm_dialog.show("Cerrando sesión", "Los horarios seleccionados se perderán. ¿Desea continuar?");
		return false;
	}

	return true;
}

/////////////////////////////////////////
/// Stores the event's id in $_SESSION ['event-id']
/// and redirects to event.html.
///
/// If there are selected schedules, launches
/// a confirm dialog for redirecting confirmation
/////////////////////////////////////////
function go_to_event(event_id)
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;

		calendar.deselect_selected_schedules();
		redirect("event.html");
	};
	xmlhttp.open("POST", "php/go-to-event.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	if (calendar.are_there_selected_schedules())
	{
		confirm_dialog.on_accept = function() { xmlhttp.send("event-id=" + event_id); };
		confirm_dialog.show("Redireccionando", "Los horarios seleccionados se perderán. ¿Desea continuar?");
	}
	else
	{
		xmlhttp.send("event-id=" +  event_id);
	}
}
