/////////////////////////////////////////
/// Possible states of calendar
/////////////////////////////////////////
const CalendarState =
{
	SELECTING: 0,	/// Selecting schedules for a new event
	UPDATING:  1	/// Updating schedules of a existing event
};

/////////////////////////////////////////
/// Struct used to represent selected schedules
/// of a specific date.
/////////////////////////////////////////
function SelectedSchedule(year, month, date)
{
	this.date = year + "-" + month + "-" + date;	/// Date which schedules are selected
	this.schedules = [];							/// Array of schedules which are selected
	this.month = month;								/// The month of the
	this.day = date;
}

var calendar =
{
	/////////////////////////////////////////
	/// Holds the start date and end date of
	/// the semester in progress.
	/////////////////////////////////////////
	semester:
	{
		start_date: new MyDate(),
		end_date: new MyDate()
	},

	selected_schedules: [],
	state: null,					/// State of the calendar
	calendar_date: new MyDate(),	/// Calendar's date
	current_date: new MyDate(),		/// Server's current date

	/////////////////////////////////////////
	/// Holds the calendar's month when the user
	/// accessed to the daily planer.
	///
	/// It's used to determine if it's needed
	/// refresh the monthly calendar once the
	/// user comes back from daily planner.
	/////////////////////////////////////////
	entry_month_to_daily_planner: 0,

	/////////////////////////////////////////
	/// Connects functions for login and logout
	/// processes, retrieves the semester's info,
	/// gets server's date and fills the monthly calendar.
	///
	/// Semester's info is used to initialize
	/// the calendar's properties.
	/////////////////////////////////////////
	init: function()
	{
		// Connections to reposition the calendar after log in and log out
		user.on_login.connect(function() {  calendar.on_login();  });
		user.on_logout.connect(function() {  calendar.on_logout();  });
		nav_bar.on_log_out = function() { return booking_calendar_log_out(); }

		// Retrieve the semester info and get the server's date
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var calendar_info = JSON.parse(this.responseText);

			if (calendar_info.semester == "No semester")
			{
				show_toast("Lo sentimos, no hay semestre disponible", DarkToast);
				return;
			}

			calendar.semester.start_date.from_string(calendar_info.semester.start_date);
			calendar.semester.end_date.from_string(calendar_info.semester.end_date);

			var server_date = calendar_info.current_date;
			calendar.calendar_date.init(server_date.year, server_date.month, server_date.day);
			calendar.current_date.init(server_date.year, server_date.month, server_date.day);
			calendar.state = calendar_info.state;

			calendar.get_events_of_the_month();
		}
		xmlhttp.open("GET", "php/get-calendar-info.php");
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// Gets the scheduled events of the calendar's
	/// month from the server and fill the
	/// monthly calendar and "events of the
	/// month" area.
	/////////////////////////////////////////
	get_events_of_the_month: function()
	{
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var events_of_the_month = JSON.parse(this.responseText);
			var booked_schedules = [];
			calendar.fill_monthly_calendar(booked_schedules);
			calendar.fill_events_of_the_month(events_of_the_month);
		}
		xmlhttp.open("GET", "php/get-events-of-the-month.php?month=" + calendar.calendar_date.month);
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// Hides daily planner components and
	/// shoes monthly calendar components.
	/// Verifies if it's needed to refresh
	/// the monthly calendar.
	/////////////////////////////////////////
	show_monthly_calendar: function()
	{
		document.getElementById("daily-planner").style.display = "none";
		document.getElementById("daily-planner-buttons").style.display = "none";
		document.getElementById("monthly-calendar").style.display = "block";
		document.getElementById("events-of-the-month-section").style.display = "block";

		if (this.entry_month_to_daily_planner != this.calendar_date.month)
			this.get_events_of_the_month();

		this.highlight_selected_schedules_in_monthly_calendar();
	},

	/////////////////////////////////////////
	/// Highlights the selected schedules in
	/// the monthly calendar, i.e., set the
	/// day's text in red for that ones where
	/// the user has selected schedules.
	/////////////////////////////////////////
	highlight_selected_schedules_in_monthly_calendar: function()
	{
		if (! is_visible("monthly-calendar"))
			return;

		var week_elements = document.getElementsByClassName("week");

		for (var i = 0; i < week_elements.length; i++)
		{
			var day_elements = week_elements [i].childNodes;
			for (var j = 0; j < day_elements.length; j++)
			{
				var day_element = day_elements [j];
				if (! day_element.classList.contains("disabled-day"))
				{
					var date = this.calendar_date.year + "-" + this.calendar_date.month + "-" + day_element.innerHTML;
					var are_there_selected_schedules_in_the_day = false;

					for (var k = 0; k < this.selected_schedules.length; k++)
					{
						if (date == this.selected_schedules [k].date)
						{
							day_element.classList.add("day-selected-schedules");
							are_there_selected_schedules_in_the_day = true;
							break;
						}
					}

					if (! are_there_selected_schedules_in_the_day)
						day_element.classList.remove("day-selected-schedules");
				}
			}
		}
	},

	/////////////////////////////////////////
	/// Fills monthly calendar. Enabled days
	/// and disabled days are set based on
	/// this.semester data.
	///
	/// Also, enables or disbales next and prev
	/// month button based on the same data and
	/// calls to highlight_selected_schedules_in_monthly_calendar()
	/////////////////////////////////////////
	fill_monthly_calendar: function(booked_schedules)
	{
		/// Sets month's name
		document.getElementById("monthly-calendar-month-name").innerHTML = this.calendar_date.get_month_name() + " " +
																		   this.calendar_date.year;

   		/// Month's days-related info
		var first_enabled_day = 1;
   		var first_week_first_day = new Date(this.calendar_date.year, this.calendar_date.month - 1, 1).getDay() + 1;
		var day = (first_week_first_day == 1) ? 1 : -1;
   		var number_of_days = this.calendar_date.get_number_of_days();
   		var last_enabled_day = number_of_days;
   		var number_of_weeks = parseInt(Math.ceil((number_of_days - (7 - first_week_first_day)) / 7)) + 1;

		/// Show/Hide Prev month button
		var prev_month_button = document.getElementById("monthly-calendar-prev-button");
		if ((user.type != "admin" && this.current_date.month == this.calendar_date.month) ||
			(user.type == "admin" && this.calendar_date.month == this.semester.start_date.month))
		{
			prev_month_button.setAttribute("onclick", "");
			prev_month_button.style.visibility = "hidden";
			first_enabled_day = (user.type == "admin") ? this.semester.start_date.date : this.calendar_date.date;
		}
		else
		{
			prev_month_button.setAttribute("onclick", "calendar.move_month(-1)");
			prev_month_button.style.visibility = "visible";
		}

		/// Show/Hide next month button
		var next_month_button = document.getElementById("monthly-calendar-next-button");
		if (this.calendar_date.month == this.semester.end_date.month)
		{
			next_month_button.setAttribute("onclick", "");
			next_month_button.style.visibility = "hidden";
			last_enabled_day = this.semester.end_date.date;
		}
		else
		{
			next_month_button.setAttribute("onclick", "calendar.move_month(1)");
			next_month_button.style.visibility = "visible";
		}

		/// Fill the monthly calendar
		var week_elements = document.getElementsByClassName("week");

		for (var i = 0; i < 6; i++)
		{
			var week_element = week_elements [i];

			for (var week_day = 1; week_day <= 7; week_day++)
			{
				var day_element = week_element.children [week_day - 1];
				day_element.className = "";

				/// Enabled days
				if (day >= first_enabled_day && day <= last_enabled_day)
				{
					day_element.innerHTML = day;
					day_element.setAttribute("onclick", "calendar.show_daily_planner(" + day + ")");

					if (this.calendar_date.month == this.current_date.month && this.current_date.date == day)
						day_element.classList.add("current-day");
					else
						day_element.classList.add("high-availability");

					day++;
				}
				else if (day < first_enabled_day)
				{
					/// Empty elements of the first week
					if (day == -1)
					{
						if (week_day < first_week_first_day)
						{
							day_element.innerHTML = "&#10240;";
							day_element.className = "";
							day_element.setAttribute("onclick", "");
						}
						else
						{
							day = 1;
							week_day--;
						}
					}
					else /// Disabled days before the first_enabled_day
					{
						day_element.innerHTML = day;
						day_element.className = "disabled-day";
						day_element.setAttribute("onclick", "");
						day++;
					}
				}
				else // day > last_enabled_day
				{
					if (day <= number_of_days)
					{
						day_element.innerHTML = day;
						day_element.className = "disabled-day";
						day++;
					}
					else
					{
						day_element.innerHTML = "&#10240;";
						day_element.className = "";
					}

					day_element.setAttribute("onclick", "");
				}
			}
		}

		this.highlight_selected_schedules_in_monthly_calendar();
	},

	/////////////////////////////////////////
	/// Move the month of this.calendar_date
	/// (if it's not out of bounds, i.e., <= 0
	/// or > 12) and calls to get_events_of_the_month().
	///
	/// Also, sets this.calendar_date.date as the
	/// minimum possible based on user's type
	/// and this.semester and this.current_date
	/////////////////////////////////////////
	move_month: function(offset)
	{
		if (this.calendar_date.month + offset <= 0 || this.calendar_date.month + offset > 12)
			return;

		this.calendar_date.month += offset;

		if (user.type == "admin")
		{
			if (this.calendar_date.month == this.semester.start_date.month)
				this.calendar_date.date = this.semester.start_date.date;
			else
				this.calendar_date.date = 1;
		}
		else
		{
			if (this.calendar_date.month == this.current_date.month)
				this.calendar_date.date = this.current_date.date;
			else
				this.calendar_date.date = 1;
		}

		this.get_events_of_the_month();
	},

	/////////////////////////////////////////
	/// This function is executed after login.
	/// Enables prev date and prev month buttons
	/// if it's needed. See diagrams/booking-calendar-login
	/////////////////////////////////////////
	on_login: function()
	{
		if (user.type != "admin")
			return;

		if (is_visible("monthly-calendar"))
		{
			if (this.calendar_date.month != this.current_date.month || this.current_date.month == this.semester.start_date.month)
				return;

			/// Enable prev month button
			var prev_month_button = document.getElementById("monthly-calendar-prev-button");
			prev_month_button.style.visibility = "visible";
			prev_month_button.setAttribute("onclick", "calendar.move_month(-1)");

			/// Enable past dates of the month
			var week_elements = document.getElementsByClassName("week");
			for (var i = 0; i < week_elements.length; i++)
			{
				var week_day_elements = week_elements [i].childNodes;
				for (var j = 0; j < week_day_elements.length; j++)
				{
					var day_element = week_day_elements [j];
					if (day_element.classList.contains("disabled-day"))
					{
						day_element.className = "high-availability";
						day_element.setAttribute("onclick", "calendar.show_daily_planner(" + parseInt(day_element.innerHTML) + ")");
					}
				}
			}
		}
		else // daily planner visible
		{
			if (! this.calendar_date.is_equal_than(this.current_date) || this.calendar_date.is_equal_than(this.semester.start_date))
				return;

			/// Enable prev date button
			var prev_date_button = document.getElementById("daily-planner-prev-date-button");
			prev_date_button.style.visibility = "visible";
			prev_date_button.setAttribute("onclick", "calendar.move_date(-1)");
		}
	},

	/////////////////////////////////////////
	/// This function is executed after logout.
	/// Deselects selected schedules, hide prev
	/// date and prev month buttons if it's needed
	/// and changes the date of the calendar
	/// if it's needed.
	/// See diagrams/booking-calendar-logout
	/////////////////////////////////////////
	on_logout: function()
	{
		this.deselect_selected_schedules();

		if (user.type != "admin" || this.calendar_date.month > this.current_date.month)
			return;

		if (is_visible("monthly-calendar"))
		{
			if (this.calendar_date.month == this.current_date.month)
			{
				/// Set disabled past dates
				var week_elements = document.getElementsByClassName("week");
				var reach_current_day = false;
				for (var i = 0; i < week_elements.length; i++)
				{
					var week_day_elements = week_elements [i].childNodes;

					for (var j = 0; j < week_day_elements.length; j++)
					{
						var day_element = week_day_elements [j];

						if (day_element.innerHTML != "&#10240;")
						{
							if (parseInt(day_element.innerHTML) != this.current_date.date)
							{
								day_element.className = "disabled-day";
							}
							else
							{
								reach_current_day = true;
								break;
							}
						}
					}

					if (reach_current_day)
						break;
				}

				/// Disable prev month button
				var prev_month_button = document.getElementById("monthly-calendar-prev-button");
				prev_month_button.style.visibility = "hidden";
				prev_month_button.setAttribute("onclick", "");
			}
			else
			{
				this.calendar_date.month = this.current_date.month;
				this.calendar_date.date = this.current_date.date;
				this.get_events_of_the_month();
			}
		}
		else // Daily planner is visible
		{
			if (this.calendar_date.date < this.current_date.date)
			{
				this.calendar_date.month = this.current_date.month;
				this.calendar_date.date = this.current_date.date;
				this.fill_daily_planner();
			}

			/// Disable prev date button
			var prev_date_button = document.getElementById("daily-planner-prev-date-button");
			prev_date_button.style.visibility = "hidden";
			prev_date_button.setAttribute("onclick", "");
		}
	},

	/////////////////////////////////////////
	/// Fills the events of the month area.
	/// If events_of_the_month is an empty
	/// array a message is displayed instead.
	/////////////////////////////////////////
	fill_events_of_the_month: function(events_of_the_month)
	{
		remove_child_nodes("events-of-the-month");

		if (! events_of_the_month.length)
		{
			document.getElementById("no-events-of-the-month-message").style.display = "block";
			return;
		}

		document.getElementById("no-events-of-the-month-message").style.display = "none";
		var events_element = document.getElementById("events-of-the-month");

		for (var i = 0; i < events_of_the_month.length; i++)
		{
			var event = events_of_the_month [i];
			var event_element = document.createElement("div");
			event_element.className = "row zebra-row-clickable";
			event_element.innerHTML = event.name;
			event_element.setAttribute("onclick", "go_to_event('" + event.id + "')")
			events_element.appendChild(event_element);
		}
	},

	/////////////////////////////////////////
	/// Shows/Hides the events of the month
	/// area. It depends if it's hidden or not.
	/////////////////////////////////////////
	toggle_events_of_the_month: function()
	{
		var events_area = document.getElementById("events-of-the-month-area");

		if (! is_visible(events_area)) /// Show
		{
			display(events_area);
			replace_class("events-of-the-month-toggle-icon", "icon-down", "icon-up");
		}
		else /// Hide
		{
			conceal(events_area);
			replace_class("events-of-the-month-toggle-icon", "icon-up", "icon-down");
		}
	},

	/////////////////////////////////////////
	/// Shows daily planner components. Updates
	/// entry_month_to_daily_planner and
	/// calls to fill_daily_planner().
	/////////////////////////////////////////
	show_daily_planner: function(date)
	{
		document.getElementById("events-of-the-month-section").style.display = "none";
		document.getElementById("monthly-calendar").style.display = "none";
		document.getElementById("daily-planner-buttons").style.display = "block";
		document.getElementById("daily-planner").style.display = "block";

		this.calendar_date.date = date;
		this.entry_month_to_daily_planner = this.calendar_date.month;
		this.fill_daily_planner();
	},

	/////////////////////////////////////////
	/// Sets the daily planner's date, shows/hides
	/// prev and next date buttons, request
	/// the events of the daily planner's date
	/// to the server and fills the daily planner
	/// using them.
	/////////////////////////////////////////
	fill_daily_planner: function()
	{
		/// Set date
		document.getElementById("daily-planner-date").innerHTML = this.calendar_date.get_day_name() + " " +
																   this.calendar_date.date + " de " +
																   this.calendar_date.get_month_name() + " del " + this.calendar_date.year;

		/// Show/Hide next and previous date buttons
		var next_date_button = document.getElementById("daily-planner-next-date-button");
		if (this.calendar_date.is_equal_than(this.semester.end_date))
		{
			next_date_button.style.visibility = "hidden";
			next_date_button.setAttribute("onclick", "");
		}
		else
		{
			next_date_button.style.visibility = "visible";
			next_date_button.setAttribute("onclick", "calendar.move_date(1)");
		}

		var prev_date_button = document.getElementById("daily-planner-prev-date-button");
		if ((user.type != "admin" && this.calendar_date.is_equal_than(this.current_date)) ||
			(user.type == "admin" && this.calendar_date.is_equal_than(this.semester.start_date)))
		{
			prev_date_button.style.visibility = "hidden";
			prev_date_button.setAttribute("onclick", "");
		}
		else
		{
			prev_date_button.style.visibility = "visible";
			prev_date_button.setAttribute("onclick", "calendar.move_date(-1)");
		}

		/// daily planner content
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var booked_schedules = JSON.parse(this.responseText);
			var events = booked_schedules.events;
			var selected_schedules_by_others = booked_schedules.selected_schedules;
			var schedule_elements = document.getElementsByClassName("daily-planner-schedule");
			var is_today = calendar.calendar_date.is_equal_than(calendar.current_date);
			var is_past_date = calendar.calendar_date.is_minor_than(calendar.current_date);
			var hour = new Date().getHours() + 1;

			for (var schedule = 0; schedule < 13; schedule++)
			{
				var schedule_element = schedule_elements [schedule];
				schedule_element.className = "row daily-planner-schedule";
				schedule_element.setAttribute("onclick", "");
				schedule_element.setAttribute("onmouseover", "");
				var available_schedule = true;

				/// Is booked the schedule?
				for (var i = 0; i < events.length; i++)
				{
					if (events [i].start_hour <= schedule && events [i].end_hour >= schedule)
					{
						schedule_element.childNodes [1].innerHTML = events [i].name;
						schedule_element.className += " booked-schedule";
						schedule_element.setAttribute("onclick", "go_to_event(" + events [i].id + ")")
						available_schedule = false;
						break;
					}
				}

				/// Is unavailable the schedule?
				if (available_schedule && (is_past_date || (is_today && schedule + 7 < hour)))
				{
					schedule_element.childNodes [1].innerHTML = "Horario no disponible";
					schedule_element.className += " unavailable-schedule";
					available_schedule = false;
				}

				///// Is selected the schedule?
				if (available_schedule)
				{
					var date = calendar.calendar_date.to_string();
					for (var i = 0; i < calendar.selected_schedules.length; ++i)
					{
						var selected_schedule = calendar.selected_schedules [i];
						if (selected_schedule.date == date)
						{
							for (var j = 0; j < selected_schedule.schedules.length; ++j)
							{
								if (selected_schedule.schedules [j] == schedule)
								{
									schedule_element.childNodes [1].innerHTML = "Horario seleccionado";
									schedule_element.className += " selected-schedule";
									schedule_element.setAttribute("onclick", "calendar.deselect_schedule(" + schedule + ")");
									available_schedule = false;
									break;
								}
							}
						}
					}
				}

				/// Selected schedules by other users
				for (var i = 0; i < selected_schedules_by_others.length; i++)
				{
					if (selected_schedules_by_others [i].start_hour <= schedule && selected_schedules_by_others [i].end_hour >= schedule)
					{
						schedule_element.childNodes [1].innerHTML = "Horario seleccionado por otro usuario";
						schedule_element.className += " selected-schedule-by-another";
						available_schedule = false;
						break;
					}
				}

				/// Not booked nor unavailable nor selected, the it's available
				if (available_schedule)
				{
					schedule_element.childNodes [1].innerHTML = "Horario disponible";
					schedule_element.className += " available-schedule";
					schedule_element.setAttribute("onclick", "calendar.select_schedule(" + schedule + ")");
					schedule_element.setAttribute("onmouseover", "if (mouse.is_down) calendar.select_schedule(" + schedule + ")");
				}
			}
		}
		xmlhttp.open("GET", "php/get-events-by-date.php?year=" + this.calendar_date.year + "&month=" + this.calendar_date.month + "&date=" +
							this.calendar_date.date);
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// Moves calendar_date by offset. After
	/// that, it calls to fill_daily_planner().
	/// It's called when the user clicks over
	/// the prev or next date buttons
	/////////////////////////////////////////
	move_date: function(offset)
	{
		this.calendar_date.move_date(offset);
		this.fill_daily_planner();
	},

	/////////////////////////////////////////
	/// Selects the schedule element in schedule_index.
	/// Changes the onclick function of the
	/// schedule, mark it as selected (graphically)
	/// and adds it to selected_schedules
	/////////////////////////////////////////
	select_schedule: function(schedule_index)
	{
		/// Invited user can't select schedules
		if (user.type == "invited")
		{
			show_toast("Debes iniciar sesión para poder seleccionar horarios para tu evento", DarkToast);
			return;
		}

		/// Verfify it has not been added yet
		var date = this.calendar_date.year + "-" + this.calendar_date.month + "-" + this.calendar_date.date;
		for (var i = 0; i < this.selected_schedules.length; i++)
		{
			var selected_schedule = this.selected_schedules [i];

			if (selected_schedule.date == date)
				for (var j = 0; j < selected_schedule.schedules.length; j++)
					if (selected_schedule.schedules [j] == schedule_index)
						return;
		}

		/// Add it
		var added = false;
		for (var i = 0; i < this.selected_schedules.length; ++i)
		{
			if (this.selected_schedules [i].date == date)
			{
				this.selected_schedules [i].schedules.push(parseInt(schedule_index));
				added = true;
				break;
			}
		}

		if (! added)
		{
			var new_length = this.selected_schedules.push(new SelectedSchedule(this.calendar_date.year, this.calendar_date.month,
																			   this.calendar_date.date));
			this.selected_schedules [new_length - 1].schedules.push(parseInt(schedule_index));
		}

		if (new_length == 1)
			document.getElementById("selected-schedules-buttons").style.display = "block";

		/// Highlight the element
		var schedule_element = document.getElementsByClassName("daily-planner-schedule") [schedule_index];
		schedule_element.childNodes [1].innerHTML = "Horario seleccionado";
		replace_class(schedule_element, "available-schedule", "selected-schedule");
		schedule_element.setAttribute("onclick", "calendar.deselect_schedule(" + schedule_index + ")");
	},

	/////////////////////////////////////////
	/// Deselects the schedule element in schedule_index.
	/// Marks it as available (graphically),
	/// removes it from selected_schedules and
	/// changes the onmouseover and onclick functions.
	/////////////////////////////////////////
	deselect_schedule: function(schedule_index)
	{
		var date = this.calendar_date.year + "-" + this.calendar_date.month + "-" + this.calendar_date.date;

		/// Set the schedule schedule element as available
		if (date == this.calendar_date.to_string())
		{
			var schedule_element = document.getElementsByClassName("daily-planner-schedule") [schedule_index];
			schedule_element.childNodes [1].innerHTML = "Horario disponible";
			replace_class(schedule_element, "selected-schedule", "available-schedule");
			schedule_element.setAttribute("onclick", "calendar.select_schedule(" + schedule_index + ")");
			schedule_element.setAttribute("onmouseover", "if (mouse.is_down) calendar.select_schedule(" + schedule_index + ")");
		}

		/// Remove it from the selected_schedules array
		for (var i = 0; i < this.selected_schedules.length; ++i)
		{
			if (this.selected_schedules [i].date == date)
			{
				this.selected_schedules [i].schedules.splice(this.selected_schedules [i].schedules.indexOf(parseInt(schedule_index)), 1);

				if (! this.selected_schedules [i].schedules.length)
					this.selected_schedules.splice(i, 1);

				if (this.selected_schedules.length == 0)
					document.getElementById("selected-schedules-buttons").style.display = "none";

				return;
			}
		}
	},

	/////////////////////////////////////////
	/// Deselects a event schedule.
	/////////////////////////////////////////
	deselect_event_schedule: function(event_schedule)
	{
		this.deselect_schedules(event_schedule_to_selected_schedules(event_schedule));
	},

	/////////////////////////////////////////
	/// Deselects an array of SelectedSchedule
	/// objects. Removes them from selected_schedules
	/// and mark them as available (graphically).
	/////////////////////////////////////////
	deselect_schedules: function(schedules)
	{
		/// Set as available the schedules of the daily planner
		if (is_visible("daily-planner"))
		{
			var daily_planner_date = this.calendar_date.year + "-" + this.calendar_date.month + "-" + this.calendar_date.date;

			for (var i = 0; i < schedules.length; i++)
			{
				var schedule = schedules [i];
				if (schedule.date == daily_planner_date)
				{
					var daily_planner_schedule_elements = document.getElementsByClassName("daily-planner-schedule");

					for (var j = 0; j < schedule.schedules.length; j++)
					{
						var daily_planner_schedule_index = schedule.schedules [j] - 7;
						var daily_planner_schedule_element = daily_planner_schedule_elements [daily_planner_schedule_index];
						replace_class(daily_planner_schedule_element, "selected-schedule", "available-schedule");
						daily_planner_schedule_element.setAttribute("onclick",
																	 "calendar.select_schedule(" + daily_planner_schedule_index + ")");
						daily_planner_schedule_element.childNodes [1].innerHTML = "Horario disponible";
					}

					break;
				}
			}
		}

		/// Remove the schedules from the selected_schedules array
		for (var i = 0; i < schedules.length; i++)
		{
			for (var j = 0; j < this.selected_schedules.length;)
			{
				var schedule = schedules [i];
				var selected_schedule = this.selected_schedules [j];

				if (schedule.date == selected_schedule.date)
				{
					for (var k = 0; k < schedule.schedules.length; k++)
					{
						selected_schedule.schedules.splice(selected_schedule.schedules.indexOf(schedule.schedules [k]), 1);
					}
				}

				if (! selected_schedule.schedules.length)
					this.selected_schedules.splice(j, 1);
				else
					j++;
			}
		}

		if (! this.selected_schedules.length)
			document.getElementById("selected-schedules-buttons").style.display = "none";

		if (is_visible("monthly-calendar"))
			this.highlight_selected_schedules_in_monthly_calendar();
	},

	/////////////////////////////////////////
	/// Selects all the available schedules in
	/// daily planner of the current date.
	/////////////////////////////////////////
	select_all_available_schedules: function()
	{
		var schedule_elements = document.getElementsByClassName("daily-planner-schedule");
		var are_there_available_schedules = false;
		for (var i = 0; i < schedule_elements.length; i++)
		{
			if (schedule_elements [i].classList.contains("available-schedule"))
			{
				this.select_schedule(i);
				are_there_available_schedules = true;
			}

		}

		if (! are_there_available_schedules)
			show_toast("No hay horarios disponibles en ésta fecha", DarkToast);
	},

	/////////////////////////////////////////
	/// Deselects the selected schedules.
	/// If daily planner is visible, sets
	/// as available the selected schedules.
	/// If monthly calendar is visible, marks
	/// all days as if there's no selected
	/// schedules
	/////////////////////////////////////////
	deselect_selected_schedules: function()
	{
		this.selected_schedules = [];
		conceal("selected-schedules-buttons");

		/// deselect schedules of the current date in the daily planner
		if (is_visible("daily-planner"))
		{
			var schedule_elements = document.getElementsByClassName("daily-planner-schedule");
			for (var i = 0; i < schedule_elements.length; i++)
			{
				if (schedule_elements [i].classList.contains("selected-schedule"))
					this.deselect_schedule(i);
			}
		}
		else /// deselect schedules of the current date in monthly calendar
		{
			this.highlight_selected_schedules_in_monthly_calendar();
		}
	},

	/////////////////////////////////////////
	/// Tells if 2 dates are in row. Returns
	/// true if they are, false otherwise.
	///
	///	2 dates are consecutive if their months
	/// are the same and the days number between them
	/// is 1. Also, their months could be differents
	/// but the months number between them must be 1
	/// and the date2's day must be 1 and the date1's
	/// day must be the last of its month.
	/////////////////////////////////////////
	are_consecutive_dates: function(d1, d2)
	{
		var date1 = new MyDate(this.calendar_date.year, d1.month, d1.day, this.calendar_date.leap_year);
		var date2 = new MyDate(this.calendar_date.year, d2.month, d2.day, this.calendar_date.leap_year);
		return ((date1.month == date2.month && date2.date - date1.date == 1) ||
				(date2.month - date1.month == 1 && date2.date == 1 && date1.date == date1.get_number_of_days()));
	},

	/////////////////////////////////////////
	/// Groups the selected schedules into
	/// a JSON array object with the following
	/// structure: 'date: $date, start_hour: $start_hour, end_hour: $end_hour'
	/// which corresponds to event_schedule table
	/////////////////////////////////////////
	group_selected_schedules: function()
	{
		var event_schedules = [];

		for (var i = 0; i < this.selected_schedules.length; ++i)
		{
			var selected_schedule = this.selected_schedules [i];
			selected_schedule.schedules.sort(function(a, b) { return a - b; });

			var start_hour = selected_schedule.schedules [0];
			var end_hour = start_hour;

			for (var j = 1; j < selected_schedule.schedules.length; ++j)
			{
				if (selected_schedule.schedules [j] - end_hour == 1)
					end_hour = selected_schedule.schedules [j];
				else
				{
					event_schedules.push({"date": selected_schedule, "start_hour": start_hour + 7,
										  "end_hour": end_hour + 7});
					end_hour = start_hour = selected_schedule.schedules [j];
				}
			}

			event_schedules.push({"date": selected_schedule, "start_hour": start_hour + 7, "end_hour": end_hour + 7});
		}

		/// Sort the event' schedules according to their dates
		event_schedules.sort(function(a, b)
		{
		  	var date1 = new MyDate();
			date1.from_string(a.date.date);

			var date2 = new MyDate();
			date2.from_string(b.date.date);

			if (date1.month < date2.month)
				return -1;
			else if (date1.month > date2.month)
				return 1;
			else	/// the same month
			{
				if (date1.date < date2.date)
					return -1;
				if (date1.date > date2.date)
					return 1;

				return 0;
			}
		});

		var grouped_schedules = [];
		for (var i = 0; i < event_schedules.length; ++i)
		{
			var start_date = event_schedules [i].date;
			var end_date = start_date;

			for (var j = i + 1; j < event_schedules.length;)
			{
				if (this.are_consecutive_dates(end_date, event_schedules [j].date) &&
					event_schedules [i].start_hour == event_schedules [j].start_hour &&
					event_schedules [i].end_hour == event_schedules [j].end_hour)
				{
					end_date = event_schedules [j].date;
					event_schedules.splice(j, 1);
				}
				else
					j++;
			}

			grouped_schedules.push({"start_date": start_date.date, "end_date": end_date.date, "start_hour":
								    event_schedules [i].start_hour, "end_hour": event_schedules [i].end_hour});
		}

		return grouped_schedules;
	},

	/////////////////////////////////////////
	/// Tells if there are selected schedules.
	/// True if there are.
	/////////////////////////////////////////
	are_there_selected_schedules: function()
	{
		return this.selected_schedules.length;
	},

	/////////////////////////////////////////
	/// Closes the event-schedules dialog
	/// and requests booking schedules confirmation
	/// to the user by a confirm dialog
	/////////////////////////////////////////
	confirm_schedules_reservation: function()
	{
		close_dialog("event-schedules");
		confirm_dialog.on_accept = function() { calendar.book_schedules(); }
		confirm_dialog.show("Confirmación", "¿Desea reservar los horarios seleccionados?");
	},

	/////////////////////////////////////////
	/// Sends the selected schedules to the
	/// server to try to book them.
	/// If they haven't been selected by another
	/// user, redirects to the event page.
	/// Deslects the schedules which where
	/// selected by another and launches a
	/// dialog to notify to the user about this.
	/////////////////////////////////////////
	book_schedules: function()
	{
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var repited_schedules = JSON.parse(this.responseText).repited_schedules;

			if (repited_schedules.length)
			{
				info_dialog.clear();
				var info_dialog_content = document.getElementById("info-dialog-content");

				var message_element = document.createElement("div");
				message_element.innerHTML = "Los siguientes horarios ya no están disponibles (o parte de ellos) y han sido deseleccionados";

				info_dialog_content.appendChild(message_element);
				info_dialog_content.appendChild(document.createElement("br"));

				var repited_schedules_area = document.createElement("div");
				repited_schedules_area.className = "row scroll-area";

				for (var i = 0; i < repited_schedules.length; i++)
				{
					var repited_schedule = repited_schedules [i];
					var repited_schedule_element = document.createElement("div");
					repited_schedule_element.className = "scol12 zebra-row";

					var repited_schedule_date = document.createElement("span");
					repited_schedule_date.className = "row";
					repited_schedule_date.innerHTML = schedule_date_to_string(repited_schedule);

					var repited_schedule_time = document.createElement("span");
					repited_schedule_time.className = "row";
					repited_schedule_time.innerHTML = schedule_time_to_string(repited_schedule);

					repited_schedule_element.appendChild(repited_schedule_date);
					repited_schedule_element.appendChild(repited_schedule_time);
					repited_schedules_area.appendChild(repited_schedule_element);

					calendar.deselect_event_schedule(repited_schedule);
				}

				info_dialog_content.appendChild(repited_schedules_area);
				info_dialog.show("Error");

				if (is_visible("daily-planner"))
					calendar.fill_daily_planner();
			}
			else
			{
				redirection_reason = RedirectionReason.Internal;
				redirect('event.html');
			}
		}
		xmlhttp.open("GET", "php/book-schedules.php?schedules=" + JSON.stringify(this.group_selected_schedules()));
		xmlhttp.send();
	}
}

user.onload.connect(function() { calendar.init(); });
