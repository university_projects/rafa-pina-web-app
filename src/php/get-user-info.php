<?php
	/////////////////////////////////////////
	/// Gets the current user's name
	/////////////////////////////////////////
	session_start();
	$user_info = new stdClass();

	if (isset ($_SESSION ['user_name']))
	{
		$user_info->name =  $_SESSION ['user_name'];
		$user_info->is_admin = $_SESSION ['user_is_admin'];
	}
	else
	{
		$user_info->name = 'Invitado';
		$user_info->is_admin = false;
	}

	echo json_encode($user_info);
?>
