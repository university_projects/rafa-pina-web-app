var upcoming_events =
{
	events: [],								/// Hold the upcoming events
	current_event_index: null,				/// Index of current event in events[]

	/////////////////////////////////////////
	/// Gets upcoming events from server and
	/// fills upcoming events area with them.
	/// Also sets a interval to move it automatically
	/////////////////////////////////////////
	init: function()
	{
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			upcoming_events.events = JSON.parse(this.responseText);

			// Create event selectors
			var event_selectors_area = document.getElementById("upcoming-events-selectors-area");
			for (var i = 0; i < upcoming_events.events.length; i++)
			{
				var selector = document.createElement("span");
				selector.className = "dot";
				selector.setAttribute("onclick", "upcoming_events.set_event(" + i + ")");
				event_selectors_area.appendChild(selector);
			}

			upcoming_events.set_event(0);
			setInterval(function() { upcoming_events.move_event(1); }, 5000);
		}
		xhr.open("GET", "php/get-upcoming-events.php");
		xhr.send();
	},

	/////////////////////////////////////////
	/// Sets current event by a given index.
	/// It must be in range [0, this.events.length).
	/// Changes selected-dot.
	/////////////////////////////////////////
	set_event: function(index)
	{
		if (this.current_event_index == index || index < 0 || index >= this.events.length)
			return;

		var current_event;
		if (this.current_event_index == null)
		{
			current_event = this.events [index];
			this.current_event_index = index;
		}
		else
		{
			replace_class(document.getElementsByClassName("selected-dot") [0], "selected-dot", "dot");
			this.current_event_index = index;
			current_event = this.events [index];
		}

		// Current event image and name
		var event_image_element = document.getElementById("upcoming-event-event-image");
		event_image_element.src = "../assets/event-images/" + current_event.image;
		event_image_element.setAttribute("onclick", "go_to_event(" + current_event.id + ")");
		document.getElementById("upcoming-event-event-name").innerHTML = current_event.name;

		// Set event schedules
		var event_schedules_area = document.getElementById("upcoming-event-event-schedules");
		event_schedules_area.innerHTML = "";
		for (var i = 0; i < current_event.schedules.length; i++)
		{
			var event_schedule = current_event.schedules [i];
			event_schedules_area.innerHTML += schedule_date_to_string(event_schedule);
			event_schedules_area.innerHTML += " - " + schedule_time_to_string(event_schedule);
			event_schedules_area.innerHTML += "<br />";
		}

		replace_class(document.getElementsByClassName("dot") [index], "dot", "selected-dot");
	},

	/////////////////////////////////////////
	/// Moves current event by offset
	/////////////////////////////////////////
	move_event: function(offset)
	{
		var new_index = this.current_event_index + offset;

		if (new_index < 0)
			new_index = this.events.length - 1;
		else if (new_index >= this.events.length)
			new_index = 0;

		this.set_event(new_index);
	}
}
