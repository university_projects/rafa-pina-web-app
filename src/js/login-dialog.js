var login_dialog =
{
	/////////////////////////////////////////
	/// Shows the dialog. Shows the login-form
	/// and sets focus to the rfc field
	/////////////////////////////////////////
	show: function()
	{
		close_left_menu();
		if(is_visible('reset-password-form'))
			replace_element('reset-password-form', 'login-form');

		display("login-dialog-background");
		var form = document.forms ["login-form"];
		form ["rfc"].focus();
		form.reset();
	},

	/////////////////////////////////////////
	/// Tries to log in using the login-form data
	/////////////////////////////////////////
	log_in: function()
	{
		var rfc = document.forms ["login-form"] ["rfc"].value;
		var password = document.forms ["login-form"] ["password"].value;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == "wrong data")
			{
				show_toast("Datos incorrectos", DarkToast);
				login_dialog.show();
				return;
			}

			var user_info = JSON.parse (this.responseText);
			user.set_name(user_info.name, user_info.is_admin);
			show_toast("Bienvenido " + user_info.name, BlueToast);
			close_dialog("login");
		}
		xmlhttp.open("POST", "php/log-in.php");
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("rfc=" + rfc + "&password=" + password);
	},

	/////////////////////////////////////////
	/// Sends the reset password request to the
	/// server with the reset-password-form data
	/////////////////////////////////////////
	reset_password_request: function()
	{
		var rfc_email = document.forms ["reset-password-form"] ["rfc-email"].value;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == "wrong data")
			{
				show_toast("Datos incorrectos", DarkToast);
				document.forms ["reset-password-form"].reset();
				return;
			}

			if (this.responseText == "request already registered")
			{
				show_toast("Tu solicitud ya había sido registrada", DarkToast);
				document.forms ["reset-password-form"].reset();
				return;
			}

			if (this.responseText == "success")
			{
				show_toast("Su solicitud ha sido registrada", BlueToast);
				close_dialog("login");
			}
		}
		xmlhttp.open("POST", "php/request-password-reset.php");
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("rfc_email=" + rfc_email);
	}
};
