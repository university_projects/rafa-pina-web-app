<?php
	require 'connect-db.inc';

	$user_id = $_GET ['user_id'];
	$db = connect_db();
	session_start();
	$_SESSION ['user_to_be_modified'] = $user_id;


	$stmt = $db->prepare('SELECT rfc, name, father_last_name, mother_last_name, email, is_admin,
								 notifications_by_email, job_title_id, nick_id
						  FROM user WHERE id = ?');
	$stmt->bind_param('i', $user_id);
	$stmt->execute();
	$stmt->bind_result($rfc, $name, $father_last_name, $mother_last_name, $email, $is_admin, $notifications_by_email,
					   $job_title_id, $nick_id);
	$stmt->fetch();
	$stmt->close();
	$db->close();

	$user_data = new stdClass();
	$user_data->rfc = $rfc;
	$user_data->name = $name;
	$user_data->father_last_name = $father_last_name;
	$user_data->mother_last_name = $mother_last_name;
	$user_data->email = $email;
	$user_data->notifications_by_email = $notifications_by_email;
	$user_data->own_profile = ($user_id == $_SESSION ['user_id']);

	/// Verify if we need to send admin-only data
	if (! $_SESSION ['user_is_admin'])
	{
		echo json_encode($user_data);
		return;
	}

	$user_data->is_admin = $is_admin;
	$user_data->job_title = new stdClass();
	$user_data->job_title->id = $job_title_id;
	$user_data->job_title->name = "";
	$user_data->nick = new stdClass();
	$user_data->nick->id = $nick_id;
	$user_data->nick->name = "";

	echo json_encode($user_data);
?>
