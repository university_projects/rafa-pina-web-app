function ToggleButton(name, type = "toggle")
{
	this.type = type;
	this.on = false;
	this.name = name;
	this.on_toggle = new Signal();

	this.is_on = function()
	{
		return this.on;
	}

	this.toggle = function()
	{
		var element = get_child_by_class_name(this.name + "-" + this.type + "-button", this.type + "-button-icon");

		if (this.on)
		{
			replace_class(element, "icon-" + this.type + "-on", "icon-" + this.type + "-off");
			element.style.color = "#777";
			this.on = false;
		}
		else
		{
			replace_class(element, "icon-" + this.type + "-off", "icon-" + this.type + "-on");
			element.style.color = "#0b84db";
			this.on = true;
		}

		this.on_toggle.emit();
	}
}
