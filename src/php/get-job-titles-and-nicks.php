<?php
	require 'connect-db.inc';

	/// Verify that it's an admin user
	session_start();
	if (! $_SESSION ['user_is_admin'])
	{
		echo '>:(';
		return;
	}

	$db = connect_db();

	$response = new stdClass();
	$response->job_titles = array();
	$response->nicks = array();

	/// Get job titles
	$stmt = $db->prepare('SELECT * FROM job_title');
	$stmt->execute();
	$stmt->bind_result($id, $name);

	while ($stmt->fetch())
		array_push($response->job_titles, array('id' => $id, 'name' => $name));

	$stmt->close();

	/// Get nicks
	$stmt = $db->prepare('SELECT * FROM nick');
	$stmt->execute();
	$stmt->bind_result($id, $name);

	while ($stmt->fetch())
		array_push($response->nicks, array('id' => $id, 'name' => $name));

	$stmt->close();
	$db->close();
	echo json_encode($response);
?>
