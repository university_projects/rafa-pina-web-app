<?php
	require 'connect-db.inc';

	session_start();
	if (! $_SESSION ['user_is_admin'])
	{
		echo '>:(';
		return;
	}

	$year = date('Y');
	if (date('n') < 8)
	{
		$start_date = $year.'-1-'.$_GET ['start_date'];
		$end_date = $year.'-7-'.$_GET ['end_date'];
	}
	else
	{
		$start_date = $year.'-8-'.$_GET ['start_date'];
		$end_date = $year.'-12-'.$_GET ['end_date'];
	}

	$db = connect_db();
	$stmt = $db->prepare('INSERT INTO semester VALUES (NULL, ?, ?)');
	$stmt->bind_param('ss', $start_date, $end_date);
	$stmt->execute();
	$stmt->close();
	$db->close();

	echo 'success';
?>
