/// NOTE: this file must be included after user.js

////////////////////////////////////////
// Redirects to a given url
////////////////////////////////////////
function redirect(url) { document.location.href = url; }

////////////////////////////////////////
// Stores the event's id and redirects
// to event page
////////////////////////////////////////
function go_to_event(event_id)
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;

		redirect("event.html");
	};
	xmlhttp.open("POST", "php/go-to-event.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("event-id=" +  event_id);
}


////////////////////////////////////////
// Enums for content's type of activities
// area
////////////////////////////////////////
const ContentType =
{
	All: 0,
	Notifications: 1,
	Seen: 2,
	Requests: 3,
	PasswordChange: 4,
	Events: 5
};

////////////////////////////////////////
// Dropdown used to select the content's type
// of activities area
////////////////////////////////////////
var content_type_dropdown;

////////////////////////////////////////
// Objects used to control the page
////////////////////////////////////////
var activity_log =
{
	activities: [],					// List of activities of the user
	filtered_activities: [],		// Filtered activities by the selected content type
	content_type: ContentType.All,	// Specifies the activities that must be displayed in activities' area

	////////////////////////////////////////
	// It's called once the page has been loaded.
	// Inits content_type_dropdown, gets user's
	// activities and fills the activities's area
	////////////////////////////////////////
	init: function()
	{
		/// Init content_type_dropdown
		content_type_dropdown = new Dropdown("content_type");
		content_type_dropdown.add_option("Todos", ContentType.All);
		content_type_dropdown.add_option("Sin ver", ContentType.Notifications);
		content_type_dropdown.add_option("Vistas", ContentType.Seen);
		content_type_dropdown.add_option("Solicitudes", ContentType.Requests);
		content_type_dropdown.add_option("Cambio de contraseña", ContentType.PasswordChange);
		content_type_dropdown.add_option("Eventos", ContentType.Events);
		content_type_dropdown.on_option_changed.connect(function()
		{
			activity_log.set_content_type(content_type_dropdown.get_selected_option().data);
		});

		/// Get activities
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			// Fill activities' area
			activity_log.activities = JSON.parse(this.responseText);
			activity_log.fill_activities_area();
		};
		xmlhttp.open("GET", "php/get-activities.php");
		xmlhttp.send();
	},

	////////////////////////////////////////
	// Sets content_type and calls to fill_activities_area()
	// It's called when content_type_dropdown's
	// option is changed
	////////////////////////////////////////
	set_content_type: function(content_type)
	{
		if (this.content_type == content_type)
			return;

		this.content_type = content_type;
		this.fill_activities_area();
	},

	////////////////////////////////////////
	// Clears activities' area and fills
	// it using filtered_activities
	////////////////////////////////////////
	fill_activities_area: function()
	{
		/// Update filtered_activities
		this.filtered_activities = [];

		switch (this.content_type)
		{
			case ContentType.All:
				this.filtered_activities = this.activities;
			break;

			case ContentType.Notifications:
				for (var i = 0; i < this.activities.length; i++)
				{
					var activity = this.activities [i];
					if (activity.state == 1)
						this.filtered_activities.push(activity);
				}
			break;

			case ContentType.Seen:
				for (var i = 0; i < this.activities.length; i++)
				{
					var activity = this.activities [i];
					if (activity.state == 0)
						this.filtered_activities.push(activity);
				}
			break;

			case ContentType.Requests:
				for (var i = 0; i < this.activities.length; i++)
				{
					var activity = this.activities [i];
					if (activity.type >= 51 || activity.type == Activity.REQUESTED_EVENT_CANCELLATION ||
						activity.type == Activity.REQUESTED_PASSWORD_RESET)
						this.filtered_activities.push(activity);
				}
			break;

			case ContentType.PasswordChange:
				for (var i = 0; i < this.activities.length; i++)
				{
					var activity = this.activities [i];
					if (activity.type == Activity.PASSWORD_CHANGED)
						this.filtered_activities.push(activity);
				}
			break;

			case ContentType.Events:
				for (var i = 0; i < this.activities.length; i++)
				{
					var activity = this.activities [i];
					if ((activity.type >= Activity.EVENT_CREATION && activity.type <= Activity.REJECTED_EVENT_CANCELLATION) ||
						activity.type == Activity.EVENT_CANCELLATION_REQUEST)
						this.filtered_activities.push(activity);
				}
		}

		/// Fill activities area
		var activities_area = document.getElementById("activities-area");
		remove_child_nodes(activities_area);

		if (! this.filtered_activities.length)
		{
			conceal("activities-area");
			display("no-activities-area");
			return;
		}

		conceal("no-activities-area");
		display("activities-area");

		for (var i = 0; i < this.filtered_activities.length; i++)
		{
			var activity = this.filtered_activities [i];
			var activity_element = document.createElement("div");
			activity_element.className = "zebra-row";

			switch (activity.type)
			{
				case Activity.EVENT_CREATION:
					activity_element.innerHTML = "Creaste el evento \"" + activity.event_name + "\"";
					activity_element.setAttribute("onclick", "go_to_event(" + activity.event_id + ")");
					activity_element.className = "zebra-row-clickable";
				break;

				case Activity.EVENT_MODIFICATION:
					activity_element.innerHTML = "Editaste el evento \"" + activity.event_name + "\"";
					activity_element.setAttribute("onclick", "go_to_event(" + activity.event_id + ")");
					activity_element.classList.add("zebra-row-clickable");
				break;

				case Activity.REQUESTED_EVENT_CANCELLATION:
					activity_element.innerHTML = "Solicitaste la cancelación del evento \"" + activity.event_name + "\"";
					activity_element.setAttribute("onclick", "go_to_event(" + activity.event_id + ")");
					activity_element.classList.add("zebra-row-clickable");
				break;

				case Activity.EVENT_CANCELLATION:
					activity_element.innerHTML = "El evento \"" + activity.event_name + "\" fue cancelado";
				break;

				case Activity.REJECTED_EVENT_CANCELLATION:
					activity_element.innerHTML = "Tu solicitud para cancelar el evento \"" + activity.event_name + "\" fue rechazada";
				break;

				case Activity.REQUESTED_PASSWORD_RESET:
					activity_element.innerHTML = "Solicitaste que tu contraseña sea reestablecida";
				break;

				case Activity.PASSWORD_RESET:
					activity_element.innerHTML = "Tu contraseña ha sido reestablecida a tu RFC, te recomendamos que la cambies inmediatamente";
				break;

				case Activity.REJECTED_PASSWORD_RESET:
					activity_element.innerHTML = "Tu solicitud de reestablecimiento de contraseña fue rechazada";
				break;

				case Activity.PASSWORD_CHANGED:
					activity_element.innerHTML = "Cambiaste tu contraseña";
				break;

				case Activity.PASSWORD_RESET_REQUEST:
					activity_element.innerHTML = "El usuario " + activity.user_name + " solicita que su contraseña sea reestablecida";
				break;

				case Activity.EVENT_CANCELLATION_REQUEST:
					activity_element.innerHTML = "El usuario " + activity.user_name + " solicita que el evento \"" +
												 activity.event_name + "\" sea cancelado";
			}

			activity_element.innerHTML += "<br />" + activity.date;
			activities_area.appendChild(activity_element);
		}
	}
};

////////////////////////////////////////
// Once user's data has been loaded,
// verifies that it's not a guest user
// and calls to activity_log.init()
////////////////////////////////////////
user.onload.connect(function()
{
	if (user.type == "invited")
		redirect("homepage.html");

	activity_log.init();
});

////////////////////////////////////////
// When user logs out, redirects to
// homepage
////////////////////////////////////////
user.on_logout.connect(function() { redirect("homepage.html"); });
