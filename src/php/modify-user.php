<?php
	require 'connect-db.inc';
	require 'register-in-common-table.inc';

	$user_data = json_decode($_GET ['user_data']);

	/// Verify permissions
	session_start();
	if (isset ($user_data->is_admin) && ! $_SESSION ['user_is_admin'])
	{
		echo '>:(';
		return;
	}

	$db = connect_db();
	$user_to_be_modified = $_SESSION ['user_to_be_modified'];

	/// Modify user data without admin permissions
	if (! $_SESSION ['user_is_admin'])
	{
		$stmt = $db->prepare('UPDATE user
							  SET rfc = ?, name = ?, father_last_name = ?, mother_last_name = ?, email = ?, notifications_by_email = ?
							  WHERE id = ?');
		$stmt->bind_param('sssssii', $user_data->rfc, $user_data->name, $user_data->father_last_name, $user_data->mother_last_name,
									$user_data->email, $user_data->notifications_by_email, $user_to_be_modified);
	}
	else /// Modify user data wi
	{
		/// Custom job title?
		if ($user_data->job_title->id == -1)
			register_in_common_table($db, 'job-title', $user_data->job_title);

		if ($user_data->nick->id == -1)
			register_in_common_table($db, 'nick', $user_data->nick);

		$stmt = $db->prepare('UPDATE user
							  SET rfc = ?, name = ?, father_last_name = ?, mother_last_name = ?, email = ?, notifications_by_email = ?,
							  	  is_admin = ?, job_title_id = ?, nick_id = ?
							  WHERE id = ?');
		$stmt->bind_param('sssssiiiii', $user_data->rfc, $user_data->name, $user_data->father_last_name, $user_data->mother_last_name,
									   $user_data->email, $user_data->notifications_by_email, $user_data->is_admin,
									   $user_data->job_title->id, $user_data->nick->id, $user_to_be_modified);
	}


	$stmt->execute();
	$stmt->close();

	/// Update user's cache data
	if ($user_to_be_modified == $_SESSION ['user_id'])
	{
		if (! isset($user_data->nick->name))
		{
			$stmt = $db->prepare('SELECT nick.name FROM nick
								  JOIN user ON user.nick_id = nick.id
								  WHERE user.id = ?');
			$stmt->bind_param('i', $user_to_be_modified);
			$stmt->execute();
			$stmt->bind_result($nick_name);
			$stmt->fetch();
			$stmt->close();
			$_SESSION ['user_name'] = $nick_name.'. '.$user_data->name;
		}
		else
		{
			$_SESSION ['user_name'] = $user_data->nick->name.'. '.$user_data->name;
		}

		if ($_SESSION ['user_is_admin'])
			$_SESSION ['user_is_admin'] = $user_data->is_admin;
	}

	$db->close();
	echo 'success';
?>
