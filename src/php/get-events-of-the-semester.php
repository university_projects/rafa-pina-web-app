<?php
	require 'connect-db.inc';

	$db = connect_db();

	/// Get info of the current semester
	$stmt = $db->prepare('SELECT start_date, end_date FROM semester WHERE start_date <= NOW() AND end_date >= NOW()');
	$stmt->execute();
	$stmt->bind_result($semester_start_date, $semester_end_date);

	/// If there's no registered semester, return error
	if (! $stmt->fetch())
	{
		$stmt->close();
		$db->close();
		echo 'no semester';
		return;
	}

	$response = new stdClass();
	$response->semester = new stdClass();
	$response->semester->start_date = $semester_start_date;
	$response->semester->end_date = $semester_end_date;
	$stmt->close();

	$response->events = array();

	/// Get all the events of the semester
	//$stmt = $db->prepare('SELECT ');
	$event = new stdClass();
	$event->table_data = array('Primer evento', 'ISC. Alberto Castro Estrada', 'Alberto Castro Estrada', 30, 'Mesa rusa');
	$event->state = 4;
	$event->dates = array();
	$event->target_audiences = array();
	array_push($event->target_audiences, 'Alumnos');
	array_push($event->target_audiences, 'Docentes');
	$start_date = '2018-01-25';
	$end_date = '2018-02-14';
	array_push($event->dates, array('start_date' => $start_date, 'end_date' => $end_date));
	array_push($response->events, $event);

	/// Event 2
	$event = new stdClass();
	$event->table_data = array('Ultimo evento', 'ISC. Mildred Samantha Hernandez Torres', 'Mildred Samantha Hernandez Torres', 26, 'Auditorio');
	$event->state = 7;
	$event->dates = array();
	$event->target_audiences = array();
	array_push($event->target_audiences, 'Externos');
	array_push($event->target_audiences, 'Docentes');
	$start_date = '2018-03-26';
	$end_date = '2018-03-30';
	array_push($event->dates, array('start_date' => $start_date, 'end_date' => $end_date));
	array_push($response->events, $event);

	$db->close();
	echo json_encode($response);
?>
