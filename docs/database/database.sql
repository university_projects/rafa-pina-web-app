CREATE DATABASE rafael_pina_gonzalez;
USE rafael_pina_gonzalez;

CREATE TABLE nick (
    id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name NVARCHAR (20) NOT NULL
);

CREATE TABLE public_type (
    id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name NVARCHAR (25) NOT NULL
);

CREATE TABLE job_title (
    id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name NVARCHAR (50) NOT NULL
);

CREATE TABLE user (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    rfc NVARCHAR (13) NOT NULL,
    name NVARCHAR (25) NOT NULL,
    father_last_name NVARCHAR (15) NOT NULL,
    mother_last_name NVARCHAR (15) NOT NULL,
    password NVARCHAR (64) NOT NULL,
    email NVARCHAR (30) NOT NULL,
    is_admin TINYINT (1) UNSIGNED NOT NULL,
	notifications_by_email TINYINT (1) UNSIGNED NOT NULL,
    job_title_id TINYINT UNSIGNED NOT NULL,
    nick_id TINYINT UNSIGNED NOT NULL,
    FOREIGN KEY (job_title_id) REFERENCES job_title (id),
    FOREIGN KEY (nick_id) REFERENCES nick (id)
);

CREATE TABLE event_description (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name NVARCHAR (100) NOT NULL,
    description NVARCHAR (500) NOT NULL,
    turnout TINYINT UNSIGNED NOT NULL,
    image NVARCHAR (20) NOT NULL,
    state TINYINT (1) UNSIGNED NOT NULL,
    user_id INT UNSIGNED NOT NULL,
    responsible NVARCHAR (50),
    equipment_distribution TINYINT UNSIGNED NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE event_schedule (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    start_hour TINYINT UNSIGNED NOT NULL,
    end_hour TINYINT UNSIGNED NOT NULL,
    event_description_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (event_description_id) REFERENCES event_description (id)
);

CREATE TABLE target_audience (
    public_type_id TINYINT UNSIGNED NOT NULL,
    event_description_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (public_type_id) REFERENCES public_type (id),
    FOREIGN KEY (event_description_id) REFERENCES event_description (id)
);

CREATE TABLE activity (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    type TINYINT (1) UNSIGNED NOT NULL,
    state TINYINT (1) UNSIGNED NOT NULL,
    date DATETIME NOT NULL,
    applicant_id INT UNSIGNED NOT NULL,
    event_id INT UNSIGNED NULL,
    solver_id INT UNSIGNED NULL,
    FOREIGN KEY (applicant_id) REFERENCES user (id)
);

CREATE TABLE semester (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL
);

CREATE TABLE selected_schedule (
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    start_hour TINYINT UNSIGNED NOT NULL,
    end_hour TINYINT UNSIGNED NOT NULL,
    user_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE issue (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	type TINYINT NOT NULL,
	name NVARCHAR (30) NOT NULL,
	description NVARCHAR (500) NOT NULL,
	state TINYINT NOT NULL,
	reporter_id INT UNSIGNED NOT NULL,
	FOREIGN KEY (reporter_id) REFERENCES user (id)
);

SET GLOBAL event_scheduler = ON;

DELIMITER $$
CREATE EVENT mark_events_as_finished
ON SCHEDULE EVERY 1 HOUR
STARTS CURRENT_TIMESTAMP
DO
BEGIN
	CREATE TEMPORARY TABLE finished_events_id AS
		(SELECT event_description.id FROM event_description
		 INNER JOIN event_schedule ON event_schedule.event_description_id = event_description.id
		 WHERE (event_schedule.start_date < CURRENT_DATE() OR
		 	   (event_schedule.start_date = CURRENT_DATE() AND event_schedule.start_hour <= HOUR(NOW()))) AND
		 	    event_description.state = 1);
	
	UPDATE event_description SET state = 4 WHERE id IN (SELECT * FROM finished_events_id);
	DROP TABLE finished_events_id;
END;$$
DELIMITER ;

/* The next code must be inserted only in testing versions of the web app */
INSERT INTO nick VALUES (NULL, 'ISC');
INSERT INTO job_title VALUES (NULL, 'Alumno');
INSERT INTO public_type VALUES (NULL, 'Alumnos'), (NULL, 'Docentes'), (NULL, 'Externos');
INSERT INTO user VALUES
(NULL, 'beto', 'Alberto', 'Castro', 'Estrada', SHA2('123', 256), 'albertocisc26@gmail.com', 1, 1, 1, 1),
(NULL, 'sam', 'Mildred Samantha', 'Hernández', 'Torres', SHA2('123', 256), 'milysam05@hotmail.com', 1, 1, 1, 1);
