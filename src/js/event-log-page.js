/// NOTE: this file must be included after user.js

////////////////////////////////////////
// Redirects to a given url
////////////////////////////////////////
function redirect(url) { document.location.href = url; }

////////////////////////////////////////
// Stores the event's id and redirects
// to event page
////////////////////////////////////////
function go_to_event(event_id)
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;

		redirect("event.html");
	};
	xmlhttp.open("POST", "php/go-to-event.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("event-id=" + event_id);
}

////////////////////////////////////////
// Gets user's events and fills the
// event's area using them
////////////////////////////////////////
function fill_events_area()
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;

		var events = JSON.parse(this.responseText);

		if (! events.length)
		{
			conceal("events-area");
			display("no-events-message");
			return;
		}

		var events_area = document.getElementById("events-area");

		for (var i = 0; i < events.length; i++)
		{
			var event = events [i];
			var event_element = document.createElement("div");
			event_element.className = "scol12 zebra-row-clickable";
			event_element.innerHTML = event.name;
			event_element.setAttribute("onclick", "go_to_event(" + event.id + ")");
			events_area.appendChild(event_element);
		}
	};
	xmlhttp.open("GET", "php/get-events-by-user.php");
	xmlhttp.send();
}

////////////////////////////////////////
// Redirects to homepage once the user
// logs out
////////////////////////////////////////
user.on_logout.connect(function() { redirect("homepage.html"); });
