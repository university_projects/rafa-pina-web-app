/////////////////////////////////////////
/// Class to use dropdowns.
/// \param name Is used to determine the
/// id of the html element where the text
/// of the current option will be displayed.
/// The id must be name + "_dropdown". Also
/// it's used to determine to which html
/// element the options will be appended as
/// children. The id of that element must be
/// name + "_dropdown_content". And finally, is used
/// to determine the dropdown js object, i.e.,
/// your object of this class must be named
/// name + "_dropdown".
///
/// \param dropdown_launcher_attribute determine
/// what is the js attribute where the value of
/// the selected option must be displayed. For example
/// for button, span and div tags it's 'innerHTML'.
/////////////////////////////////////////
function Dropdown(name, dropdown_launcher_attribute = "innerHTML")
{
    /////////////////////////////////////////
    /// An option for a dropdown. Objects of
    /// this type are created every that a dropdown
    /// object adds an option.
    /////////////////////////////////////////
    function Option(element_id, value, data)
    {
        this.element = document.getElementById(element_id);
        this.value = value;
        this.data = data;

        /////////////////////////////////////////
        /// Changes the visual style of the widget.
        /// If it's selected, it's changed to unselected
        /////////////////////////////////////////
        this.toggle = function()
        {
            if (this.element.classList.contains("dropdown-element"))
				replace_class(this.element, "dropdown-element", "dropdown-selected-element");
            else
				replace_class(this.element, "dropdown-selected-element", "dropdown-element");
        }

        this.edit = function()
        {
            this.element.contentEditable = true;
            this.element.innerHTML = "";
            this.element.focus();
        }

        this.update_value = function()
        {
            this.element.contentEditable = false;
            this.value = this.element.innerHTML;
        }
    }

    this.dropdown_launcher = document.getElementById(name + "-dropdown");
    this.dropdown_launcher_attribute = dropdown_launcher_attribute;
    this.dropdown_content = document.getElementById(name + "-dropdown-content");
    this.selected_index = -1;
    this.options = [];
    this.enabled = true;
	this.js_object_name = name + "_dropdown";
	this.on_option_changed = new Signal(); /// This signal is sent when the selected option is changed

    /////////////////////////////////////////
    /// Adds an option to the dropdown.
    /// \param value Is the text that the option
    /// will have.
    /// \param data Can be used to store some
    /// data in the option object.
    /////////////////////////////////////////
    this.add_option = function(value, data = "")
    {
        var option_element = document.createElement("span");
        this.dropdown_content.appendChild(option_element);
        option_element.id = this.dropdown_content.id + "-" + value;
        option_element.innerHTML = value;
        option_element.className = "scol12 dropdown-element";
        option_element.setAttribute("onclick", this.js_object_name + ".select_option_by_index(" + this.options.length + ")");
        this.options.push(new Option(this.dropdown_content.id + "-" + value, value, data));

        if (this.options.length == 1)
            this.select_option_by_index(0);
    }

    /////////////////////////////////////////
    /// Adds an editable option to the dropdown.
    ///
    /// \param value Is the text that the option
    /// will have.
    /// \param data Can be used to store some
    /// data in the option object.
    /////////////////////////////////////////
    this.add_editable_option = function(value, data = "")
    {
        var option_container = document.createElement("span");
        option_container.classNAme = "scol12";

        var option_element = document.createElement("span");
        option_element.innerHTML = value;
        option_element.className = "scol10 dropdown-element";
        option_element.id = this.dropdown_content.id + "-" + value;
        option_element.setAttribute("onblur", this.js_object_name + ".options [" + this.options.length + "].update_value()");
        option_element.setAttribute("onclick", this.js_object_name + ".options [" + this.options.length + "].update_value();" +
                                               this.js_object_name + ".select_option_by_index(" + this.options.length + ");");

        var edit_button = document.createElement("span");
        edit_button.className = "scol2 icon-edit dropdown-element-edit-button";
        edit_button.onclick = function() { option.edit(); }

        option_container.appendChild(option_element);
        option_container.appendChild(edit_button);
        this.dropdown_content.appendChild(option_container);

        var option = new Option(option_element.id, value, data);
        this.options.push(option);
    }

	/////////////////////////////////////////
	/// Remove all the options of the dropdown
	/////////////////////////////////////////
	this.clear = function()
	{
		while (this.dropdown_content.hasChildNodes())
			this.dropdown_content.removeChild(this.dropdown_content.firstChild);

		this.selected_index = -1;
		this.options = [];
	}

    /////////////////////////////////////////
    /// Selects the option in param index position.
    /// Normally, this function is called when the
    /// user clicks on a option of the dropdown.
    /////////////////////////////////////////
    this.select_option_by_index = function(index)
    {
        if (this.selected_index != -1)
            this.options [this.selected_index].toggle();

        this.selected_index = (index != dropdown_last_index) ? index : this.options.length - 1;
        this.options [this.selected_index].toggle();
        this.dropdown_launcher [this.dropdown_launcher_attribute] = this.options [this.selected_index].value;
        this.dropdown_content.style.display = "none";
		this.on_option_changed.emit();
    }

	/////////////////////////////////////////
    /// Selects an option with value equal to
    /// the param value.
    /////////////////////////////////////////
	this.select_option_by_value = function(value)
	{
		for (var i = 0; i < this.options.length; i++)
		{
			if (this.options [i].value == value)
			{
				this.select_option_by_index(i);
				return;
			}
		}
	}

    /////////////////////////////////////////
    /// Selects an option with data equal to
    /// the param data.
    /////////////////////////////////////////
    this.select_option_by_data = function(data)
    {
        for (var i = 0; i < this.options.length; i++)
        {
            var option = this.options [i];
            if (option.data == data)
            {
                this.select_option_by_index(i);
                return;
            }
        }
    }

    /////////////////////////////////////////
    /// Returns the object Option of the selected
    /// index.
    /////////////////////////////////////////
    this.get_selected_option = function()
    {
		if (this.selected_index == -1)
			return null;

        return this.options [this.selected_index];
    }

	/////////////////////////////////////////
	/// Shortcut to get the value of the selected
	/// option.
	/////////////////////////////////////////
	this.get_selected_value = function()
	{
		if (this.selected_index == -1)
			return null;

		return this.get_selected_option().value;
	}

    /////////////////////////////////////////
    /// Sets enabled the dropdown, a disabled
    /// dropdown don't deplyo the options on click.
    /////////////////////////////////////////
    this.set_enabled = function(enabled)
    {
        this.enabled = enabled;
    }

    /////////////////////////////////////////
    /// Shows the options of the dropdown.
    /// This function is called when the user
    /// click on the dropdown launcher element
    /////////////////////////////////////////
    this.show_content = function()
    {
        if (! this.enabled)
            return;

        this.dropdown_content.style.display = "block";
    }

	/////////////////////////////////////////
	/// Replaces all the options of a dropdown
	/// by all the integer values starting in
	/// from and finishing in to
	/////////////////////////////////////////
	this.replace_options_by_range = function(from, to)
	{
		this.clear();

		for (var i = from; i <= to; i++)
			this.add_option(i);
	}
}

/////////////////////////////////////////
/// Function used to
/////////////////////////////////////////
function hide_dropdown(dropdown_name)
{
    setTimeout(function() { document.getElementById(dropdown_name + "-dropdown-content").style.display = "none"; }, 50);
}

/////////////////////////////////////////
/// Const which can be used to select
/// the last option of an dropdown using
/// the select_option_by_index function
/////////////////////////////////////////
const dropdown_last_index = -2;
