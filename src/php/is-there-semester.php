<?php
	require 'connect-db.inc';

	$date = date('Y').'-'.date('n').'-'.date('d');
	$db = connect_db();

	/// Check if there is a semester in progress
	$stmt = $db->prepare('SELECT COUNT(*) FROM semester WHERE start_date <= ? AND end_date >= ?');
	$stmt->bind_param('ss', $date, $date);
	$stmt->execute();
	$stmt->bind_result($there_is_semester);
	$stmt->fetch();
	$stmt->close();
	$db->close();

	if ($there_is_semester)
		echo 'there is';
	else /// No semester??, return the current month
		echo date('n');

?>
