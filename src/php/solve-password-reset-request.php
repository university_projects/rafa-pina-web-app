<?php
	/////////////////////////////////////////
	/// Solves the activities type 52.
	///
	/// Requires in $_POST:
	/// + applicant_id
	/// + reset If it's true, the password
	/// of the applicant is reset, else the
	/// request is rejected.
	/////////////////////////////////////////
	require 'connect-db.inc';
	require 'log-activity.inc';
	require 'solve-activity.inc';

	session_start();
	if (! $_SESSION ['user_is_admin'])
	{
		echo '>:(';
		return;
	}

	$applicant_id = $_POST ['applicant_id'];

	/// Verify that the applicant has requested password reset
	$db = connect_db();

	$stmt= $db->prepare('SELECT id FROM activity WHERE applicant_id = ? AND state = 1 AND type = 52');
	$stmt->bind_param('i', $applicant_id);
	$stmt->execute();
	$stmt->bind_result($activity_id);

	if (! $stmt->fetch())
	{
		$stmt->close();
		$db->close();
		echo 'no request';
		return;
	}

	$stmt->close();

	/// Solve the request
	if ($_POST ['reset'] == 'true')
	{
		/// Reset the password
		$stmt = $db->prepare('UPDATE user SET password = SHA2(rfc, 256) WHERE id = ?');
		$stmt->bind_param('i', $applicant_id);
		$stmt->execute();
		$stmt->close();
		log_activity($db, PASSWORD_RESET, $applicant_id);
		echo 'reset';
	}
	else
	{
		log_activity($db, REJECTED_PASSWORD_RESET, $applicant_id);
		echo 'rejected';
	}

	solve_activity($db, $activity_id, $_SESSION ['user_id']);
	$db->close();
?>
