<?php
	////////////////////////////////////////////////
	// Tells if the current user is creator of
	// current event.
	////////////////////////////////////////////////
	require 'connect-db.inc';
	session_start();

	if (! isset($_SESSION ['user_id']) || ! isset($_SESSION ['event_id']))
	{
		echo 'no';
		return;
	}

	$event_id = $_SESSION ['event_id'];
	$db = connect_db();

	$stmt = $db->prepare('SELECT user_id FROM event_description WHERE id = ?');
	$stmt->bind_param('i', $event_id);
	$stmt->execute();
	$stmt->bind_result($event_creator_id);
	$stmt->fetch();
	$stmt->close();
	$db->close();

	echo ($event_creator_id == $_SESSION ['user_id']) ? 'yes' : 'no';
?>
