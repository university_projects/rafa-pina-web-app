<?php
	/////////////////////////////////////////
	/// Close the current session
	/////////////////////////////////////////
	session_start();
	unset($_SESSION ['user_name']);
	unset($_SESSION ['user_id']);
	unset($_SESSION ['user_is_admin']);
	unset($_SESSION ['notifications_id']);
?>
