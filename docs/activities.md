# What is an activity?
An activity could be:

+ A determined action performed by the user. (Activity)
+ A requested action from a normal user to the admins. (Request)

## The state of an activity.
It's used to determine if an activity is an notification or not. There are 2 states:

+ 0: The activity has been marked as read or the request has already been solved.
+ 1: The activity hasn't been marked as read or the request hasn't been solved.

## When the activities are created?

1. When a user creates an event: An activity (1) for the user with the message "Creaste el evento " + $event_name, an email with the event's information will be sent to the user too.
2. When a user edits an event: An activity (2) for the user with the message "Editaste el evento " + $event_name, an email with the event's information will be sent to the user too.
3. When a user requests the cancellation of an event: An activity (3) for the user with the message "Solicitaste la cancelación del evento " + $event_name, an email will be sent with the same message. A request (51) with the message "Solicitud de cancelación del evento " + $event_name, an email will be sent with the same message to all the admin users.
4. When an admin accepts an event cancellation request: An activity (4) for the user with the message "Tu evento " + $event_name + " fue cancelado", an email will be sent with the same message. The request (51) created when the user made the request must be marked as solved, an email will be sent to the admins with the message "El evento " + $event_name + " fue cancelado".
5. When an admin rejects an event cancellation request: An activity (5) for the user with the message "Tu evento " + $event_name + " no fue cancelado", an email will be sent with the same message. The request created (51) when the user made the request must be marked as solved, an email will be sent to the admins with the message "El evento " + $event_name + " no fue cancelado".
6. When an user requests password resetting: An activity (6) for the user with the message "Solicitaste que tu contraseña sea reestablecida", an email will be sent with the same message. Also, a request (52) with the message "El usuario " + $user_name + " solicita que su contraseña sea reestablecida" is created, an email will be sent with the same message.
7. When an admin resets the password of an user: The request (52) is marked as solved and an activity (7) for the user with the message "Tu contraseña fue reestablecida" is created. Also, an email with the same message is sent.
8. When an admin rejects the request (52), it is marked as solved and an activity (8) for the user is created with the message "Tu contraseña no fue reestablecida". Also, an email with the same message is sent.
9. When an user changes its password: Logs an activity (9) for the user, the message is "Cambiaste tu contraseña". Also, an email with the same message is sent;

## What is a notification?
A notificacion is an activity which state is 1.

## Which notifications belongs to which user?
Activities which type is in the range [1, 50] are only for the applicant.
Activities which type is in the range [51, 255] are only for admins.

## What action does a notification execute?

+ Type 1, 2, 3, 4, 5: If the user clicks over the element, he/she will be redirected to the page of the event. The notification stops being a notification when the user click over it or over its 'x' button.
+ Type 6, 7, 8, 9: The notification stops being a notification when the user click over over its 'x' button.
+ Type 51: If the user clicks over the element, he/she will be redirected to the page of the event. The request is solved when an admin accepts or rejects it.
+ Type 52: If the user clicks over the element, a confirm dialog will be launched. The request is solved when an admin restores the password.
