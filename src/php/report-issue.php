<?php
	/////////////////////////////////////////
	/// Registers a given issue into issue table.
	/// The given issue must be a JSON object
	/// with the following fields: type, name
	/// and description.
	/////////////////////////////////////////
	require 'connect-db.inc';
	session_start();

	/// Check if somebody is playing dirty
	if (! isset($_SESSION ['user_id']) || ! isset($_POST ['issue']))
	{
		echo '>:(';
		return;
	}

	$reporter_id = $_SESSION ['user_id'];
	$issue = json_decode($_POST ['issue']);
	$db = connect_db();

	$stmt = $db->prepare('INSERT INTO issue VALUES (NULL, ?, ?, ?, 1, ?)');
	$stmt->bind_param('issi', $issue->type, $issue->name, $issue->description, $reporter_id);
	$stmt->execute();
	$stmt->close();
	$db->close();

	echo 'ok';
?>
