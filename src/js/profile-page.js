function redirect(url)
{
	document.location.href = url;
}

/////////////////////////////////////////
/// Stores the id of the event in $_SESSION ['event-id']
/// and redirects to event.html
/////////////////////////////////////////
function go_to_event(event_id)
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;

		redirect("event.html");
	};
	xmlhttp.open("POST", "php/go-to-event.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("event-id=" +  event_id);
}

var profile_page =
{
	/////////////////////////////////////////
	/// Init the profile page. If user's type is
	/// admin, asks to the server if there's is a
	/// semester. If there's not, shows the register
	/// semester button
	/////////////////////////////////////////
	init: function()
	{
		user.on_logout.connect(function() { redirect('homepage.html'); });

		if (user.type == "admin")
		{
			document.getElementById("admin-buttons").style.display = "block";

			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function()
			{
				if (this.readyState != 4 || this.status != 200)
					return;

				if (this.responseText == "there is")
					return;

				document.getElementById("register-semester-admin-button").style.display = "block";
				registration_dialog.init(this.responseText);
			}
			xmlhttp.open("GET", "php/is-there-semester.php");
			xmlhttp.send();
		}
	},

	/////////////////////////////////////////
	/// Redirects to user-control.html with
	/// the data of the user.
	/////////////////////////////////////////
	edit_profile: function()
	{
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			redirect('user-control.html');
		}
		xmlhttp.open("GET", "php/go-to-edit-profile.php");
		xmlhttp.send();
	}
};

user.onload.connect(function() { profile_page.init(); });
