var manual_redirection = true; 		// Tells if the redirection is manual (refresh, etc) or internal (go to another app's page)

//////////////////////////////////////////////////
// Redirects to the given url.
//
// If page's state is CREATING, launches a confirm
// dialog to confirm the redirection. If page's state
// is EDITING and the user has made changes, launches
// a confirm dialog too. If accepts and the users is
// redirecting to event's page, just gets the
// the information of that event and fills the page
// with that info, else just redirects
////////////////////////////////////////////////////
function redirect(url, event_id = null)
{
	var is_redirecting_to_event = function()
	{
		if (event_id == null)
			return false;

		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			event_page.get_event_schedules();
			event_page.get_event_information();
			conceal("form-section");
		}
		xhr.open("POST", "php/go-to-event.php");
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.send("event-id=" + event_id);
		close_dialog("notifications");
		return true;
	}

	var clear_event_id = function()
	{
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (! is_redirecting_to_event())
			{
				manual_redirection = false;
				document.location.href = url;
			}
		}
		xhr.open("GET", "php/clear-event-id.php");
		xhr.send();
	}

	if (event_page.state == EventModuleState.CREATING)
	{
		close_dialog("notifications");
		confirm_dialog.on_accept = function()
		{
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function()
			{
				if (this.readyState != 4 || this.status != 200)
					return;

				if (! is_redirecting_to_event())
				{
					manual_redirection = false;
					document.location.href = url;
				}
			}
			xhr.open("GET", "php/clear-selected-schedules.php");
			xhr.send();
		}
		confirm_dialog.show("Confirmar redirección", "Aún no ha terminado de crear su evento. Su avance se perderá. ¿Quiere continuar?");
	}
	else if (event_page.state == EventModuleState.EDITING && event_page.has_unsaved_changes())
	{
		close_dialog("notifications");
		confirm_dialog.on_accept = function () { clear_event_id(); event_page.discard_changes(true); }
		confirm_dialog.show("Confirmar redirección", "Los cambios sin guardar se perderán. ¿Quiere continuar?");
	}
	else
	{
		clear_event_id();
	}
}

//////////////////////////////////////////////////
// Launches a confirm dialog when the user is
// creating or editing an event.
//////////////////////////////////////////////////
window.onbeforeunload = function(event)
{
	if (! manual_redirection)
		return;

	if (event_page.state == EventModuleState.CREATING || (event_page.state == EventModuleState.EDITING && event_page.has_unsaved_changes()))
		event.returnValue = "ask";
}

//////////////////////////////////////////////////
// Clears selected schedules if user is creating an
// event or clears event's id if it's just seeing an event.
//////////////////////////////////////////////////
window.onunload = function()
{
	if (! manual_redirection)
		return;

	if (event_page.state == EventModuleState.CREATING)
	{
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "php/clear-selected-schedules.php", false);
		xhr.send();
	}
	else
	{
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "php/clear-event-id.php", false);
		xhr.send();
	}
}

//////////////////////////////////////////////////
// It's called when the user is logging out, if the
// event's state is CREATING launches a confirm
// dialog to confirm the logout.
// If the event's state is EDITING and the user has
// made changes, launches a confirm dialog to confirm
// the logout.
//////////////////////////////////////////////////
function event_page_log_out()
{
	if (event_page.state == EventModuleState.CREATING)
	{
		confirm_dialog.on_accept = function()
		{
			nav_bar.on_log_out = function() { return true; }
			nav_bar.log_out();

			user.on_logout.connect(function()
			{
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function()
				{
					if (this.readyState != 4 || this.status != 200)
						return;

					manual_redirection = false;
					document.location.href = "homepage.html";
				}
				xhr.open("GET", "php/clear-selected-schedules.php");
				xhr.send();
			});
		}
		confirm_dialog.show("Cerrando sesión", "Aún no ha terminado de crear su evento. Su avance se perderá. ¿Quiere continuar?");
		return false;
	}

	if (event_page.state == EventModuleState.EDITING)
	{
		if (event_page.has_unsaved_changes())
		{
			confirm_dialog.on_accept = function()
			{
				event_page.discard_changes(true);
				nav_bar.on_log_out = function() { return true; }
				nav_bar.log_out();
				nav_bar.on_log_out = function() { return event_page_log_out(); }
			}
			confirm_dialog.show("Cierre de sesión", "Los cambios sin guardar ser perderán al cerrar la sesión. ¿Quiere continuar?");
			return false;
		}

		event_page.discard_changes();
	}

	return true;
}

//////////////////////////////////////////////////
// Calls to redirect() with an empty url and the
// given event_id
//////////////////////////////////////////////////
function go_to_event(event_id)
{
	redirect("", event_id);
}

const EventModuleState =
{
	CREATING:  0,	/// Creating a new event
	VIEWING:   1,	/// Viewing the information of an event
	EDITING:   2	///Modifying the information of an event
};

var event_page =
{
    selected_equipment_distribution: 1,		// Id of the selected equipment distribution
	target_audiences_checkboxes: [],		// Array of the target audiences checbox icons
	state: null,							// State of the page
	event_information: null,				// Event information, it's filled if the user is viewing an event or after a event creation
	more_info_shown: false,					// Tells if the more info section is shown

	/////////////////////////////////////////////
	// Gets the state of the page. No state causes
	// redirects to homepage.
	// Gets the public types and creates checkboxes for
	// each one.
	// If page's state is VIEWING, gets the event's
	// information from the server and fills up the
	// form.
	// If page's state is CREATING, sets the created-by
	// field's value and selected the defautl equipment distribution
	/////////////////////////////////////////////
	init: function()
	{
		nav_bar.on_log_out = function() { return event_page_log_out(); };
		user.on_login.connect(function() { event_page.show_creator_buttons(); });
		user.on_logout.connect(function() { conceal("creator-buttons"); });

		/// Get event page's state
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == "none")
			{
				manual_redirection = false;
				document.location.href = "homepage.html";
				return;
			}

			event_page.state = parseInt(this.responseText);

			/// Get public types
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function()
			{
				if (this.readyState != 4 || this.status != 200)
					return;

				/// Create public type checkboxes
				var target_audiences = JSON.parse(this.responseText);
				var target_audiences_checklist = document.getElementById("target-audiences-checklist");

				for (var i = 0; i < target_audiences.public_types.length; i++)
				{
					var public_type = target_audiences.public_types [i];

					var checkbox = document.createElement("button");
					checkbox.setAttribute("type", "button");
					checkbox.setAttribute("data-public-type-id", public_type.id);
					checkbox.setAttribute("data-public-type-name", public_type.name);
					checkbox.setAttribute("onclick", "toogle_checkbox('"+ public_type.name +"')");
					checkbox.className = "scol12 bcol12 checkbox-button";

					var icon = document.createElement("span");
					icon.setAttribute("id", public_type.name + "-checkbox-icon")
					icon.setAttribute("data-public-type-id", public_type.id);
					icon.setAttribute("data-public-type-name", public_type.name);
					icon.className = "checkbox-button-icon icon-checkbox-off scol1";
					event_page.target_audiences_checkboxes.push(icon);

					var text = document.createElement("span");
					text.className = "scol11";
					text.innerHTML = public_type.name;

					checkbox.appendChild(icon);
					checkbox.appendChild(text);
					target_audiences_checklist.appendChild(checkbox);
				}

				// Act based on event_page.state
				if (event_page.state == EventModuleState.CREATING)
				{
					document.getElementById("equipment-distribution1").style.backgroundColor = "#0b89bf";
					document.forms ["event-form"] ["created-by"].value = user.name;
					conceal("more-info-button-container");
					display("form-section");
				}
				else
				{
					event_page.get_event_information();
				}

				event_page.get_event_schedules();
				event_page.show_creator_buttons();
			};
			xmlhttp.open("GET", "php/get-target-audiences.php");
			xmlhttp.send();
		};
		xmlhttp.open("GET", "php/get-event-page-state.php");
		xmlhttp.send();
	},

	//////////////////////////////////////////////
	// Retrieves event's information from the server
	// and sets state as viewing.
	//////////////////////////////////////////////
	get_event_information: function()
	{
		// Get event's info
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			event_page.event_information = JSON.parse(this.responseText);
			event_page.select_equipment_distribution(event_page.event_information.equipment_distribution);
			event_page.select_target_audiences_by_id();
			event_page.set_viewing_state();
			display("more-info-button-container");
		};
		xmlhttp.open("GET", "php/get-event-information.php");
		xmlhttp.send();
	},

	//////////////////////////////////////////////
	// Get the event schedules from the server and
	// show they on the header of the form.
	//////////////////////////////////////////////
	get_event_schedules: function()
	{
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var event_schedules = JSON.parse(this.responseText);
			var event_schedules_area = document.getElementById("event-header-event-schedules");
			event_schedules_area.innerHTML = "";

			for (var i = 0; i < event_schedules.length; i++)
			{
				var event_schedule = event_schedules [i];
				event_schedules_area.innerHTML += schedule_date_to_string(event_schedule);
				event_schedules_area.innerHTML += " - " + schedule_time_to_string(event_schedule);
				event_schedules_area.innerHTML += "<br />";
			}
		}
		xmlhttp.open("GET", "php/get-event-schedules.php");
		xmlhttp.send();
	},

	////////////////////////////////////////////////////
	// Tells if the user has made changes to event's information.
	// True if has made, false otherwise.
	////////////////////////////////////////////////////
	has_unsaved_changes: function()
	{
		var event_information = this.generate_event_information_object();

		/// Compare new event's info with backup event's info
		if (event_information.name 					  != this.event_information.name 					||
			event_information.description			  != this.event_information.description 			||
			event_information.turnout				  != this.event_information.turnout 				||
			event_information.responsible 			  != this.event_information.responsible 			||
			event_information.image			  		  != this.event_information.image					||
			event_information.equipment_distribution  != this.event_information.equipment_distribution	||
			event_information.target_audiences.length != this.event_information.target_audiences.length)
			return true;

		/// Compare target audiences
		for (var i = 0; i < event_information.target_audiences.length; i++)
		{
			var target_audience = event_information.target_audiences [i];
			var found_target_audience = false;

			for (var j = 0; j < this.event_information.target_audiences.length; j++)
			{
				if (target_audience == this.event_information.target_audiences [j])
				{
					found_target_audience = true;
					break;
				}
			}

			if (! found_target_audience)
				return true;
		}

		return false;
	},

	////////////////////////////////////////////////////
	// It's called when the user select an equipment
	//  distribution and hightligth it.
	///////////////////////////////////////////////////
	select_equipment_distribution: function(index)
	{
		var current_distribution = document.getElementById("equipment-distribution" + this.selected_equipment_distribution);
		var new_distribution = document.getElementById("equipment-distribution" + index);

    	current_distribution.style.backgroundColor = "";
    	new_distribution.style.backgroundColor = "#0b89bf";
    	this.selected_equipment_distribution = index;
	},

	/////////////////////////////////////////////////
	// Get and save in an array the selected target
	// audiences and return it index.
	////////////////////////////////////////////////
	get_selected_target_audiences_id: function()
	{
		var selected_target_audiences = [];

		for (var i = 0; i < this.target_audiences_checkboxes.length; i++)
		{
			var checkbox = this.target_audiences_checkboxes [i];

			if (is_on(checkbox))
				selected_target_audiences.push(checkbox.getAttribute("data-public-type-id"));
		}
		return selected_target_audiences;
	},

	/////////////////////////////////////////////////
	// Get and save in an array the selected target
	// audiences and return it index.
	////////////////////////////////////////////////
	get_selected_target_audiences_name: function()
	{
		var selected_target_audiences = [];

		for (var i = 0; i < this.target_audiences_checkboxes.length; i++)
		{
			var checkbox = this.target_audiences_checkboxes [i];

			if (is_on(checkbox))
				selected_target_audiences.push(checkbox.getAttribute("data-public-type-name"));
		}
		return selected_target_audiences;
	},

	////////////////////////////////////////////////
	// Selects (sets on the checkboxes of) the target audiences
	// of the event.
	////////////////////////////////////////////////
	select_target_audiences_by_id: function()
	{
		for (var i = 0; i < this.event_information.target_audiences.length; i++)
		{
			var target_audience = this.event_information.target_audiences [i];

			for (var j = 0; j < this.target_audiences_checkboxes.length; j++)
			{
				var checkbox = this.target_audiences_checkboxes [j];
				if (checkbox.getAttribute("data-public-type-id") == target_audience)
				{
					checkbox_set_on(checkbox);
					break;
				}
			}
		}
	},

	////////////////////////////////////////////////
	// Check if the user select at least one type
	// of target audience, is call before create
	// an event. If no one is selected return false.
	////////////////////////////////////////////////
	verify_target_audience: function()
	{
		var target_audiences = this.get_selected_target_audiences_id();
		if (target_audiences.length == 0)
		{
			show_toast("Debe seleccionar al menos un tipo de publico", DarkToast);
			return false;
		}
		return true;
	},

	////////////////////////////////////////////////
	// Generates and returns a JSON Object using the
	// form's data. It doesn't verify that the form is
	// filled correctly
	////////////////////////////////////////////////
	generate_event_information_object: function()
	{
		var form = document.forms ['event-form'];
		var event_information = new Object();

		event_information.name = form ['event-name'].value;
		event_information.created_by = user.name;
		event_information.description = form ['event-description'].value;
		event_information.turnout = form ['event-turnout'].value;
		event_information.image = event_image_dialog.get_selected_image_name();
		event_information.responsible = (! form ['event-responsible'].value.length) ? user.name : form ['event-responsible'].value;
		event_information.equipment_distribution = this.selected_equipment_distribution;
		event_information.target_audiences = this.get_selected_target_audiences_id();

		return event_information;
	},

	////////////////////////////////////////////////
	// Toggles event's information
	////////////////////////////////////////////////
	toggle_more_info: function()
	{
		this.more_info_shown = ! this.more_info_shown;

		if (this.more_info_shown)
		{
			display("form-section");
			document.getElementById("more-info-button-text").innerHTML = "Menos info...";
			replace_class("more-info-button-icon", "icon-down", "icon-up");
		}
		else
		{
			conceal("form-section");
			document.getElementById("more-info-button-text").innerHTML = "Más info...";
			replace_class("more-info-button-icon", "icon-up", "icon-down");
		}
	},

	/////////////////////////////////////////////
	///	Shows all the information of an event in
	/// read-only way
	/////////////////////////////////////////////
	set_viewing_state: function()
	{
		var form = document.forms ['event-form'];
		document.getElementById("event-image-upload-input").value = "";
		form.setAttribute("action", "");
		var target_audiences = this.get_selected_target_audiences_name();

		/// Fill the fields and set them as read-only
		document.getElementById("event-image").src = "../assets/event-images/" + this.event_information.image;

		var event_name_input = form ['event-name'];
		event_name_input.readOnly = true;
		event_name_input.value = this.event_information.name;
		event_name_input.style.cursor = "not-allowed";
		document.getElementById("event-header-event-name").innerHTML = this.event_information.name;

		form ['created-by'].value = this.event_information.created_by;

		var event_description_input = form ['event-description'];
		event_description_input.readOnly = true;
		event_description_input.value = this.event_information.description;
		event_description_input.style.cursor = "not-allowed";

		var event_responsible_input = form ['event-responsible'];
		event_responsible_input.readOnly = true;
		event_responsible_input.value = this.event_information.responsible;
		event_responsible_input.style.cursor = "not-allowed";
		document.getElementById('optional-comment').style.display = "none";

		var event_turnout_input = form ['event-turnout'];
		event_turnout_input.type = "text";
		event_turnout_input.readOnly = true;
		event_turnout_input.value = this.event_information.turnout;
		event_turnout_input.style.cursor = "not-allowed";

		var selected_distribution = "img" + this.selected_equipment_distribution + ".png";
		document.getElementById("equipment-distribution" + this.event_information.equipment_distribution).click();
		document.getElementById('selected-distribution').src = "../assets/equipment-distribution/" + selected_distribution;
		document.getElementById('selected-distribution-content').style.display = "block";

		/// Hide event-creation-only elements
		document.getElementById("event-header-select-image-button").style.display = "none";
		document.getElementById("distributions-content").style.display = "none";
		document.getElementById('comment').style.display = "none";
		document.getElementById("create-event-button").style.display = "none";
		document.getElementById('target-audiences-checklist').style.display = "none";

		/// Create a list of selected_target_audiences
		var selected_target_audiences_content = document.getElementById("selected-target-audiences-content");
		remove_child_nodes(selected_target_audiences_content);
		selected_target_audiences_content.style.display = "block";
		var list = document.createElement("ul");
		list.setAttribute("id", "selected-target-audiences-list");
		selected_target_audiences_content.appendChild(list);

		for (var i = 0; i < target_audiences.length; i++)
		{
			var list_element = document.createElement("li");
			list_element.innerHTML = target_audiences [i];
			list.appendChild(list_element);
		}

		/// Select event's image in event_image_dialog
		if (this.event_information.image.search("uploads") != -1)
			event_image_dialog.set_custom_container_image("../assets/event-images/" + this.event_information.image, this.event_information.image);
		else
			event_image_dialog.select_image(parseInt(this.event_information.image.replace(/.+(\d).+/, "$1")) - 1);

		this.state = EventModuleState.VIEWING;
	},

	////////////////////////////////////////////////
	// Get all the form information and send it to
	// the server to create the event. If a custom
	// image was selected, it's send to the server too.
	////////////////////////////////////////////////
	create_event: function()
	{
		this.event_information = this.generate_event_information_object();
		this.event_information.state = 1;

		var form_data = new FormData();

		if (event_image_dialog.is_custom_image())
			form_data.append("custom_image", document.getElementById("event-image-upload-input").files [0]);

		form_data.append("event_information", JSON.stringify(this.event_information));

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText != "error")
			{
				if (this.responseText != "success")
				{
					event_page.event_information.image = this.responseText;
					event_image_dialog.set_custom_container_image("../assets/event-images/" + this.responseText, this.responseText);
				}

				event_page.set_viewing_state();
				display("more-info-button-container");
				event_page.toggle_more_info();
				show_toast("Su evento fue registrado", BlueToast);
				display("edit-event-button");
			}
			else
			{
				show_toast("Error al registrar el evento", DarkToast);
			}
		}
		xmlhttp.open("POST", "php/create-event.php");
		xmlhttp.send(form_data);
	},

	///////////////////////////////////// Edit event functions

	/////////////////////////////////
	/// Shows creator buttons only
	/// if the current user is the
	/// creator of the event.
	/////////////////////////////////
	show_creator_buttons: function()
	{
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == "yes" && event_page.event_information.state == 1)
			{
				display("edit-event-button");
				display("cancel-event-button");
			}
		}
		xhr.open("GET", "php/is-event-creator.php");
		xhr.send();
	},

	////////////////////////////////////////////////
	// Changes the state from viewing to editing. Shows
	// the editable components and save and discard changes
	// buttons.
	////////////////////////////////////////////////
	set_editing_state: function()
	{
		var form = document.forms ['event-form'];

		var event_name_input = form ['event-name'];
		event_name_input.readOnly = false;
		event_name_input.style.cursor = "";

		var event_description_input = form ['event-description'];
		event_description_input.readOnly = false;
		event_description_input.style.cursor = "";

		var event_responsible_input = form ['event-responsible'];
		event_responsible_input.readOnly = false;
		event_responsible_input.style.cursor = "";
		document.getElementById("optional-comment").style.display = "inline-block";

		var event_turnout_input = form ['event-turnout'];
		event_turnout_input.type = "number";
		event_turnout_input.readOnly = false;
		event_turnout_input.style.cursor = "";

		conceal('selected-distribution-content');
		display("distributions-content");

		display("event-header-select-image-button");
		conceal("selected-target-audiences-content");
		display("target-audiences-checklist");
		display("comment");

		conceal("edit-event-button");
		display("save-discard-changes-buttons");

		this.state = EventModuleState.EDITING;
	},

	////////////////////////////////////////////////
	// Sends new event's information to the server.
	////////////////////////////////////////////////
	save_changes: function()
	{
		if (! this.has_unsaved_changes())
		{
			show_toast("No hay cambios que guardar", DarkToast);
			conceal("save-discard-changes-buttons");
			display("edit-event-button");
			return;
		}

		var event_information = this.generate_event_information_object();
		var form_data = new FormData();

		if (event_image_dialog.is_custom_image() && event_image_dialog.custom_image_name != this.event_information.image)
			form_data.append("custom_image", document.getElementById("event-image-upload-input").files [0]);

		form_data.append("event_information", JSON.stringify(event_information));

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText != "error")
			{
				event_page.event_information = event_information;

				if (this.responseText != "success")
				{
					event_page.event_information.image = this.responseText;
					event_image_dialog.set_custom_container_image("../assets/event-images/" + this.responseText, this.responseText);
					event_page.set_viewing_state();
					document.location.reload();
				}

				conceal("save-discard-changes-buttons");
				display("edit-event-button");
				event_page.set_viewing_state();
				show_toast("Cambios guardados", BlueToast);
			}
			else
			{
				show_toast("Error al guardar los cambios", DarkToast);
			}
		}
		xmlhttp.open("POST", "php/edit-event.php");
		xmlhttp.send(form_data);
	},

	////////////////////////////////////////////////
	// If there are unsaved changes, requests confirmation
	// to discard the changes confirmation, else
	// just sets viewing state.
	////////////////////////////////////////////////
	discard_changes: function(forced = false)
	{
		if (! this.has_unsaved_changes())
		{
			event_page.set_viewing_state();
			conceal("save-discard-changes-buttons");
			display("edit-event-button");
			return;
		}

		confirm_dialog.on_accept = function()
		{
			document.getElementById("equipment-distribution" + event_page.event_information.equipment_distribution).click();

			/// Restore checked target_audiences
			var public_type_checklist = document.getElementById("target-audiences-checklist");
			for (var i = 0; i < public_type_checklist.childNodes.length; i++)
			{
				var public_type_checkbox = public_type_checklist.childNodes [i];
				var must_be_checked = false;
				var is_checked = is_on(public_type_checkbox.firstChild);

				for (var j = 0; j < event_page.event_information.target_audiences.length; j++)
				{
					if (parseInt(public_type_checkbox.firstChild.getAttribute("data-public-type-id")) ==
						event_page.event_information.target_audiences [j])
					{
						must_be_checked = true;
						break;
					}
				}

				if ((must_be_checked && ! is_checked) || (! must_be_checked && is_checked))
					public_type_checkbox.click();
			}

			event_page.set_viewing_state();
			conceal("save-discard-changes-buttons");
			display("edit-event-button");
		}

		if (forced)
			confirm_dialog.on_accept();
		else
			confirm_dialog.show("Descartar cambios", "¿Quiere descartar los cambios realizados?");
	},

	////////////////////////////////////////////////
	// Requests event cancellation. Notifies to applicant
	// and creates a request for admin users.
	////////////////////////////////////////////////
	request_event_cancellation: function()
	{
		confirm_dialog.on_accept = function()
		{
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function()
			{
				if (this.readyState != 4 || this.status != 200)
					return;

				if (this.responseText == "ok")
				{
					show_toast("Solicitud registrada", BlueToast);

					event_page.event_information.state = 6;
					if (event_page.state == EventModuleState.EDITING)
						event_page.discard_changes(true);
					else
						conceal("creator-buttons");
				}
				else
				{
					show_toast("Lo sentimos, hubo un problema al procesar su solicitud", DarkToast);
				}
			}
			xhr.open("GET", "php/request-event-cancellation.php");
			xhr.send();
		}

		if (this.state == EventModuleState.EDITING && this.has_unsaved_changes())
			confirm_dialog.show("Cancelar evento", "Los cambios sin guardar serán descartados. ¿Quiere solicitar la cancelación de su evento?");
		else
			confirm_dialog.show("Cancelar evento", "¿Quiere solicitar la cancelación de su evento?");
	}
};

user.onload.connect(function() { event_page.init(); });
