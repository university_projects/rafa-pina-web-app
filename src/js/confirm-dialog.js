var confirm_dialog =
{
	//////////////////////////////////////
	/// Shows the confirm dialog with and
	/// sets its title, its descriptive text,
	/// the text of the accept button and
	/// the text of the cancel button.
	//////////////////////////////////////
	show: function(title, text, accept_button_text = "Aceptar", cancel_button_text = "Cancelar")
	{
		close_left_menu();
		document.getElementById("confirm-dialog-text").innerHTML = text;
		document.getElementById("confirm-dialog-title").innerHTML = title;
		document.getElementById("confirm-dialog-accept-button").innerHTML = accept_button_text;
		document.getElementById("confirm-dialog-cancel-button").innerHTML = cancel_button_text;
		document.getElementById("confirm-dialog-background").style.display = "block";
	},

	/////////////////////////////////////////
	/// This function is executed when the
	/// user clicks the accept button.
	/////////////////////////////////////////
	accept: function()
	{
		this.on_accept();
		close_dialog("confirm");
	},

	/////////////////////////////////////////
	/// This function is executed when the
	/// user clicks the cancel button.
	/////////////////////////////////////////
	cancel: function()
	{
		this.on_cancel();
		close_dialog("confirm");
	},

	/////////////////////////////////////////
	/// This function is called when the user clicks
	/// over the accept button. Actually, does nothing,
	/// but you can set a custom function to do
	/// whatever you need ;).
	/////////////////////////////////////////
	on_accept: function() {},

	/////////////////////////////////////////
	/// This function is called when the user clicks
	/// over the cancel button. Actually, does nothing,
	/// but you can set a custom function to do
	/// whatever you need ;).
	/////////////////////////////////////////
	on_cancel: function() {}
};
