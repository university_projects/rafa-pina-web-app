<?php
	function register_in_common_table($db, $table_name, $object)
	{
		/// Verify it does not exist
		if ($table_name == 'nick')
			$stmt = $db->prepare('SELECT COUNT(*) FROM nick WHERE name = ?');
		else if ($table_name == 'public-type')
			$stmt = $db->prepare('SELECT COUNT(*) FROM public_type WHERE name = ?');
		else if ($table_name == 'job-title')
			$stmt = $db->prepare('SELECT COUNT(*) FROM job_title WHERE name = ?');

		$stmt->bind_param('s', $object->name);
		$stmt->execute();
		$stmt->bind_result($already_exist);
		$stmt->fetch();
		$stmt->close();

		if ($already_exist)
			return false;

		/// Register it
		if ($table_name == 'nick')
			$stmt = $db->prepare('INSERT INTO nick VALUES (NULL, ?)');
		else if ($table_name == 'public-type')
			$stmt = $db->prepare('INSERT INTO public_type VALUES (NULL, ?)');
		else if ($table_name == 'job-title')
			$stmt = $db->prepare('INSERT INTO job_title VALUES (NULL, ?)');

		$stmt->bind_param('s', $object->name);
		$stmt->execute();
		$stmt->close();

		$object->id = $db->insert_id;
	}
?>
