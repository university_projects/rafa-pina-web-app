<?php
	/////////////////////////////////////////
	/// Changes the password of a user.
	/// Logs the activity PASSWORD_CHANGED
	/////////////////////////////////////////
	require 'connect-db.inc';
	require 'log-activity.inc';

	session_start();
	$user_id = $_SESSION ['user_id'];

	if ($user_id != $_SESSION ['user_to_be_modified'])
	{
		echo '>:(';
		return;
	}

	$current_password = $_POST ['current_password'];
	$db = connect_db();

	/// Verify that the current password is correct
	$stmt = $db->prepare('SELECT COUNT(*) FROM user WHERE id = ? AND password = SHA2(?, 256)');
	$stmt->bind_param('is', $user_id, $current_password);
	$stmt->execute();
	$stmt->bind_result($current_password_correct);
	$stmt->fetch();
	$stmt->close();

	if (! $current_password_correct)
	{
		$db->close();
		echo 'wrong current password';
		return;
	}

	/// Change the password
	$new_password = $_POST ['new_password'];
	$stmt = $db->prepare('UPDATE user SET password = SHA2(?, 256) WHERE id = ?');
	$stmt->bind_param('si', $new_password, $user_id);
	$stmt->execute();
	$stmt->close();

	/// Log the activity
	log_activity($db, PASSWORD_CHANGED, $user_id);
	$db->close();

	echo 'success';
?>
