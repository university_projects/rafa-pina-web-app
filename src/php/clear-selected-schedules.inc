<?php
    require 'connect-db.inc';

    // Clears SESSION and db selected_schedule table
    function clear_selected_schedules()
    {
        $db = connect_db();
        $user_id = $_SESSION ['user_id'];
        unset($_SESSION ['selected_schedules']);

        $stmt = $db->prepare('DELETE FROM selected_schedule WHERE user_id = ?');
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $stmt->close();
    }
?>
