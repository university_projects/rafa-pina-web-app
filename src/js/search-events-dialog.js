var search_events_dialog =
{
	/////////////////////////////////////////
	/// Resets and shows the dialog, sets the
	/// focus to the event-name field
	/////////////////////////////////////////
	show: function()
	{
		close_left_menu();
		document.getElementById("search-events-dialog-background").style.display = "block";
		document.forms ["search-events-form"].reset();
		document.forms ["search-events-form"] ["event-name"].focus();
		conceal("search-events-dialog-no-events-message");
		conceal("search-events-dialog-found-events-area");
	},

	/////////////////////////////////////////
	/// Sends the form's data to the server.
	/// With the server's response, creates
	/// a list of event's names
	/////////////////////////////////////////
	search: function()
	{
		var keyword = document.forms ["search-events-form"] ["event-name"].value;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			var found_events = JSON.parse(this.responseText);

			// No found events, hide the found events area and show the no events message
			if (! found_events.length)
			{
				conceal("search-events-dialog-found-events-area");
				display("search-events-dialog-no-events-message");
				return;
			}

			// Hide the no events message, remove child nodes of found events area and show it
			conceal("search-events-dialog-no-events-message");
			var found_events_area = document.getElementById("search-events-dialog-found-events-area");
			remove_child_nodes(found_events_area);
			display(found_events_area);

			// For each found event, create a row element
			for (var i = 0; i < found_events.length; i++)
			{
				var found_event = found_events [i];

				var found_event_element = document.createElement("div");
				found_event_element.className = "scol12 zebra-row-clickable";
				found_event_element.innerHTML = found_event.name;
				found_event_element.setAttribute("onclick", "go_to_event(" + found_event.id + ")");

				found_events_area.appendChild(found_event_element);
			}
		}
		xmlhttp.open("GET", "php/search-events.php?event-name=" + keyword);
		xmlhttp.send();
	}
};
