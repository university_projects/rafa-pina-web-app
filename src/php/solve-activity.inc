<?php
	/////////////////////////////////////////
	/// Mark an activity as solve, i.e., sets
	/// its state as 0 and solver_id.
	/////////////////////////////////////////
	function solve_activity($db, $activity_id, $solver_id)
	{
		$stmt = $db->prepare('UPDATE activity SET state = 0, solver_id = ? WHERE id = ?');
		$stmt->bind_param('ii', $solver_id, $activity_id);
		$stmt->execute();
		$stmt->close();
	}
?>
