function Mouse()
{
    this.is_down = false; /// Tells if the mouse left button is down

    /////////////////////////////////////////
    /// This function must be called in the
    /// end of the html file.
    /////////////////////////////////////////
    this.init = function()
    {
        document.body.onmousedown = function() {  mouse.is_down = true;  }
        document.body.onmouseup = function() {  mouse.is_down = false;  }
    }
}

var mouse = new Mouse();
