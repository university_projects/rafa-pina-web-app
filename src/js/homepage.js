function redirect(url)
{
	document.location.href = url;
}

/////////////////////////////////////////
/// Stores the id of the event in $_SESSION ['event-id']
/// and redirects to event.html
/////////////////////////////////////////
function go_to_event(event_id)
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState != 4 || this.status != 200)
			return;

		redirect("event.html");
	};
	xmlhttp.open("POST", "php/go-to-event.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("event-id=" +  event_id);
}
