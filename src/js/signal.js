function Signal()
{
    this.callbacks = [];            ///< The list of callbacks to call when the signal is emitted

    /////////////////////////////////////////
    /// Connects a callback to the signal.
    /// You can set the id of the callback if
	/// you want to disconnect
    /////////////////////////////////////////
    this.connect = function(callback, id = "")
    {
        this.callbacks.push({"id": id, "callback": callback, "enabled": true});
        return this.id_generator;
    }

	/////////////////////////////////////////
	/// Disables a callback. Disbaled
	/// callbacks are not called when the signal
	/// is emitted.
	/////////////////////////////////////////
	this.disable = function(callback_id)
	{
		for (var i = 0; i < this.callbacks.length; i++)
			if (this.callbacks [i].id == callback_id)
				this.callbacks [i].enabled = false;
	}

	/////////////////////////////////////////
	/// Enables a callback. Enabaled
	/// callbacks are called when the signal
	/// is emitted.
	/////////////////////////////////////////
	this.enable = function(callback_id)
	{
		for (var i = 0; i < this.callbacks.length; i++)
			if (this.callbacks [i].id == callback_id)
				this.callbacks [i].enabled = true;
	}

    /////////////////////////////////////////
    /// Disconnects a previously connected
    /// callback from the signal.
    /// \param id Is the id of the connection
    /////////////////////////////////////////
    this.disconnect = function(id)
    {
        for (var i = 0; i < this.callbacks.length; i++)
        {
            if (this.callbacks [i].id == id)
            {
                this.callbacks.splice(i, 1);
                return;
            }
        }
    }

    /////////////////////////////////////////
    /// Emits the signal, it means, calls to
    /// all the connected callbacks
    /////////////////////////////////////////
    this.emit = function()
    {
        for (var i = 0; i < this.callbacks.length; i++)
			if (this.callbacks [i].enabled)
            	this.callbacks [i].callback();
    }
}
