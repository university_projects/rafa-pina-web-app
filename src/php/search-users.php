<?php
	require 'connect-db.inc';

	/// Verify it's an admin user
	session_start();
	if (! $_SESSION ['user_is_admin'])
	{
		echo '>:(';
		return;
	}

	$users_reference = '%'.$_GET ['users_reference'].'%';
	$db = connect_db();
	$stmt = $db->prepare('SELECT id, CONCAT(name, " ", father_last_name, " ", mother_last_name), rfc FROM user
						  WHERE rfc LIKE ? OR name LIKE ? OR father_last_name LIKE ? OR mother_last_name LIKE ?');
	$stmt->bind_param('ssss', $users_reference, $users_reference, $users_reference, $users_reference);
	$stmt->execute();
	$stmt->bind_result($id, $full_name, $rfc);

	$response = new stdClass();
	$response->found_users = array();
	while ($stmt->fetch())
		array_push($response->found_users, array('id' => $id, 'full_name' => $full_name, 'rfc' => $rfc));

	$stmt->close();
	$db->close();
	echo json_encode($response);
?>
