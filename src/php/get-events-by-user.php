<?php
	require 'connect-db.inc';
	session_start();

	$user_id = $_SESSION ['user_id'];
	$db = connect_db();

	$stmt = $db->prepare('SELECT id, name FROM event_description WHERE user_id = ? ORDER BY id DESC');
	$stmt->bind_param('i', $user_id);
	$stmt->execute();
	$stmt->bind_result($id, $name);

	$events = array();
	while ($stmt->fetch())
		array_push($events, array('id' => $id, 'name' => $name));

	$stmt->close();
	$db->close();
	echo json_encode($events);
?>
