
function toogle_checkbox(name)
{
    var name = name;
    var element =  document.getElementById( name + "-checkbox-icon");

    if (is_on(name))
    {
        replace_class(element, "icon-checkbox-on", "icon-checkbox-off");
        element.style.color = "#777";
    }
    else
    {
        replace_class(element, "icon-checkbox-off", "icon-checkbox-on");
        element.style.color = "#0b84db";
    }
}

///////////////////////////////////////////////////////
//  Returns true if the checkbox is on
//////////////////////////////////////////////////////
function is_on(element)
{
    if (typeof(element) == "string")
        element =  document.getElementById(element + "-checkbox-icon");

    return (element.className.search("icon-checkbox-on") != -1);
}

function checkbox_set_on(element)
{
	replace_class(element, "icon-checkbox-off", "icon-checkbox-on");
	element.style.color = "#0b84db";
}
