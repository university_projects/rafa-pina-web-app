<?php
	/////////////////////////////////////////
	/// Mark a activity as read, so it leaves to
	/// be a notification. Deletes the notification
	/// from $_SESSION ['notifications_id']
	///
	/// Requires in $_POST:
	/// + notification_id
	/////////////////////////////////////////
	require 'connect-db.inc';

	$notification_id = $_POST ['notification_id'];
	$db = connect_db();

	$stmt = $db->prepare('UPDATE activity SET state = 0 WHERE id = ?');
	$stmt->bind_param('i', $notification_id);
	$stmt->execute();

	$stmt->close();
	$db->close();

	session_start();
	array_splice($_SESSION ['notifications_id'], array_search($notification_id, $_SESSION ['notifications_id'], 1));
?>
