<?php
	require 'connect-db.inc';
	$db = connect_db();

    $stmt = $db->prepare('SELECT id, name FROM public_type');
    $stmt->execute();
    $stmt->bind_result($id, $name);

	$response = new stdClass();
	$response->public_types = array();

    while ($stmt->fetch())
		array_push($response->public_types, array('id' => $id, 'name' => $name));

    $stmt->close();
	$db->close();

	echo json_encode($response);
?>
