<?php
    /////////////////////////////////////////
    /// Returns an array with the schedules
    /// of the event with id equal to $event_id.
    /// The returned array contains more arrays
    /// with the following fields: start_date,
    /// end_date, start_hour, end_hour.
    /// $db must be a previosly opened mysqli object.
    /////////////////////////////////////////
    function get_event_schedules($db, $event_id)
    {
        $stmt = $db->prepare ('SELECT start_date, end_date, start_hour - 6, end_hour - 5
                               FROM event_schedule
                               WHERE event_description_id = ?');
        $stmt->bind_param ('i', $event_id);
        $stmt->execute ();
        $stmt->bind_result ($start_date, $end_date, $start_hour, $end_hour);

        $event_schedules = array ();
        while ($stmt->fetch ())
        {
            array_push ($event_schedules, array ('start_date' => $start_date, 'end_date' => $end_date,
                                                 'start_hour' => $start_hour, 'end_hour' => $end_hour));
        }

        $stmt->close();
        return $event_schedules;
    }
?>
