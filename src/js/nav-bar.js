var nav_bar =
{
	/////////////////////////////////////////
	/// Enables or disables the login action
	/// from the top bar.
	///
	/// If enable is false, the action is changed
	/// to logout.
	/////////////////////////////////////////
	enable_login: function(enable)
	{
		if (enable)
		{
			document.getElementById("nav-bar-log-in").style.display = "block";
			document.getElementById("nav-bar-log-out").style.display = "none";
			document.getElementById("nav-bar-notifications").style.display = "none";
			document.getElementById("nav-bar-empty-space").style.display = "block";

			document.getElementById("left-menu-log-in").style.display = "block";
			document.getElementById("left-menu-log-out").style.display = "none";
			document.getElementById("left-menu-my-profile").style.display = "none";
			document.getElementById("left-menu-notifications").style.display = "none";
		}
		else
		{
			document.getElementById("nav-bar-log-in").style.display = "none";
			document.getElementById("nav-bar-log-out").style.display = "block";
			document.getElementById("nav-bar-notifications").style.display = "block";
			document.getElementById("nav-bar-empty-space").style.display = "none";

			document.getElementById("left-menu-log-in").style.display = "none";
			document.getElementById("left-menu-log-out").style.display = "block";
			document.getElementById("left-menu-my-profile").style.display = "block";
			document.getElementById("left-menu-notifications").style.display = "block";
		}
	},

	/////////////////////////////////////////
	/// Sets the user name.
	/////////////////////////////////////////
	set_user_name: function(user_name)
	{
		var user_name_element = document.getElementById("nav-bar-user-name");
		user_name_element.innerHTML = user_name;

		if (user_name != "Invitado")
		{
			if (location.pathname.substring(5) != "profile-page.html")
			{
				user_name_element.className = user_name_element.className.replace("nav-bar-item", "nav-bar-clickable-item");
				user_name_element.setAttribute("onclick", "redirect('profile-page.html')");
			}

			document.getElementById("nav-bar-log-out").style.display = "block";
			document.getElementById("nav-bar-log-in").style.display = "none";
		}
		else
		{
			user_name_element.className = user_name_element.className.replace("nav-bar-clickable-item", "nav-bar-item");
			user_name_element.setAttribute("onclick", "");
			document.getElementById("nav-bar-log-out").style.display = "none";
			document.getElementById("nav-bar-log-in").style.display = "block";
		}
	},

	/////////////////////////////////////////
	/// Logs out the current session.
	/////////////////////////////////////////
	log_out: function()
	{
		if (! this.on_log_out())
			return;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			user.set_name("Invitado");
			show_toast("Hasta luego", DarkToast);
		};
		xmlhttp.open("GET", "php/log-out.php");
		xmlhttp.send();
	},

	/////////////////////////////////////////
	/// This function is called when the 'log_out'
	/// function is called, you can set this member
	/// to execute some custom code instead.
	///
	/// Your function have to return a boolean value.
	/// True to continuea with the execution of the
	/// 'log_out' function, false otherwise.
	/////////////////////////////////////////
	on_log_out: function() {  return true;  }
};
