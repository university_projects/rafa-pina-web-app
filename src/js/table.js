/////////////////////////////////////////
/// Class which helps you to fill, sort
/// and apply filters to a table.
/////////////////////////////////////////
function Table(table, js_object_name, sortable = true)
{
	this.table = (typeof(table) == "string") ? document.getElementById(table) : table;	/// HTML table element
	this.data = [];																		/// Data used to fill the table
	this.js_object_name = js_object_name;												/// JS object's name. The name of this object

	this.filter = "";				/// String filter
	this.filtered_column = 0;		/// The column index to which the filter is applied.
	this.min_filter = null;			/// Used to apply range filter
	this.max_filter = null;			/// Used to apply range filter

	this.sortable = sortable;		/// Tells if the table is sortable
	this.sorted_column = null;		/// Column index which is sorted
	this.ascending_order = true;	/// Tells if the sort is ascending or not

	/////////////////////////////////////////
	/// Adds a header to the table.
	/////////////////////////////////////////
	this.add_header = function(header_title, column_type = "string")
	{
		var tr;

		if (! this.table.childNodes.length)
		{
			tr = document.createElement("tr");
			this.table.appendChild(tr);
		}
		else
		{
			tr = this.table.childNodes [0];
		}

		var th = document.createElement("th");
		th.innerHTML = header_title + " ";

		var span_sort_icon = document.createElement("span");
		th.appendChild(span_sort_icon);

		tr.appendChild(th);

		if (sortable)
		{
			th.className = "clickable-header";
			th.setAttribute("onclick", js_object_name + ".sort_by(" + (tr.childNodes.length - 1) + ", '" + column_type  + "')");
		}
	}

	/////////////////////////////////////////
	/// Adds a row to the table. Row data must be
	/// an array.
	/////////////////////////////////////////
	this.add_row = function(row_data)
	{
		var tr = document.createElement("tr");

		for (var i = 0; i < row_data.length; i++)
		{
			var td = document.createElement("td");
			td.innerHTML = row_data [i];
			tr.appendChild(td);
		}

		this.table.appendChild(tr);
		this.data.push(row_data);
	}

	/////////////////////////////////////////
	/// Adds a list of rows contained in an array.
	/////////////////////////////////////////
	this.add_rows = function(rows)
	{
		for (var i = 0; i < rows.length; i++)
			this.add_row(rows [i]);
	}

	/////////////////////////////////////////
	/// Fills the table usin this.data
	/////////////////////////////////////////
	this.fill = function(data = this.data)
	{
		if (! data.length)
		{
			display(this.table.id + "-error-message");
			return;
		}

		conceal(this.table.id + "-error-message");

		for (var i = 0; i < data.length; i++)
		{
			var row = data [i];
			var tr = document.createElement("tr");

			for (var j = 0; j < row.length; j++)
			{
				var td = document.createElement("td");
				td.innerHTML = row [j];
				tr.appendChild(td);
			}

			this.table.appendChild(tr);
		}
	}

	/////////////////////////////////////////
	/// Clears all the rows of the table.
	/////////////////////////////////////////
	this.clear = function(delete_headers = false)
	{
		var rows = this.table.childNodes;
		for (var i = rows.length - 1; i >= ((delete_headers) ? 0 : 1); i--)
			this.table.removeChild(rows [i]);
	}

	/////////////////////////////////////////
	/// Clears the data of the table
	/////////////////////////////////////////
	this.clear_data = function()
	{
		this.clear();
		this.data = [];
	}

	/////////////////////////////////////////
	/// Sorts this.data taking into account this.sorted_column
	/// and this.ascending_order
	/////////////////////////////////////////
	this.sort_data = function(column_type)
	{
		var table_object = this;
		var column = this.sorted_column;

		if (column_type == "string")
		{
			this.data.sort(function(a, b)
			{
				if (table_object.ascending_order)
					return a [column] > b [column];
				else
					return a [column] < b [column];
			});
		}
		else
		{
			this.data.sort(function(a, b)
			{
				if (table_object.ascending_order)
					return a [column] - b [column];
				else
					return b [column] - a [column];
			});
		}
	}

	/////////////////////////////////////////
	/// Sorts the table data by a column index
	/////////////////////////////////////////
	this.sort_by = function(column_index, column_type)
	{
		var headers = this.table.childNodes [0];

		/// Same column?, invert ascending_order
		if (this.sorted_column == column_index)
		{
			this.ascending_order = ! this.ascending_order;
		}
		else if (this.sorted_column != null)
		{
			this.ascending_order = true;
			headers.childNodes [this.sorted_column].getElementsByTagName("span") [0].className = "";
		}

		this.sorted_column = column_index;

		/// Change the sort icon in header
		if (this.ascending_order)
			headers.childNodes [column_index].getElementsByTagName("span") [0].className = "icon-down";
		else
			headers.childNodes [column_index].getElementsByTagName("span") [0].className = "icon-up";

		/// Actually, sorts and fills the data
		this.clear();
		this.sort_data(column_type);

		if (this.min_filter == null)
			this.apply_filter(this.filtered_column, this.filter);
		else
			this.apply_range_filter(this.filtered_column, this.min_filter, this.max_filter);
	}

	/////////////////////////////////////////
	/// Applies a string filter to a given column
	/////////////////////////////////////////
	this.apply_filter = function(column_index, filter)
	{
		this.min_filter = null;
		this.max_filter = null;

		this.filter = filter;
		this.filtered_column = column_index;

		this.clear();
		var regExp = new RegExp(filter, "i");
		var filtered_data = [];

		for (var i = 0; i < this.data.length; i++)
		{
			var row = this.data [i];

			if (regExp.test(row [column_index]))
				filtered_data.push(row);
		}

		this.fill(filtered_data);
	}

	/////////////////////////////////////////
	/// Applies a number range filter to the table.
	/////////////////////////////////////////
	this.apply_range_filter = function(column_index, min, max)
	{
		console.log("applying range filter: " +  min + " to " + max);
		this.filtered_column = column_index;
		this.min_filter = parseInt(min);
		this.max_filter = parseInt(max);

		this.clear();
		var filtered_data = [];

		for (var i = 0; i < this.data.length; i++)
		{
			var row = this.data [i];
			var column_value = parseInt(row [column_index]);

			if (column_value >= min && column_value <= max)
				filtered_data.push(row);
		}

		this.fill(filtered_data);
	}
}
