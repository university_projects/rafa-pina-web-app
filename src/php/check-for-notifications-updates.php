<?php
	/////////////////////////////////////////
	/// Tells if the are new notifications.
	/// This script is called by the client
	/// every x time. Basically, verifies that
	/// the notifications' id of the user
	/// are in the array $_SESSION ['notifications_id']
	/////////////////////////////////////////
	require 'connect-db.inc';

	session_start();
	if (! isset($_SESSION ['user_id']))
		return;

	$current_notifications = $_SESSION ['notifications_id'];
	$user_id = $_SESSION ['user_id'];
	$db = connect_db();

	if ($_SESSION ['user_is_admin'])
	{
		$stmt = $db->prepare('SELECT id FROM activity
							  WHERE ((applicant_id = ? AND type <= 50) OR type >= 51) AND state = 1');
	}
	else
	{
		$stmt = $db->prepare('SELECT id FROM activity
							  WHERE applicant_id = ? AND type <= 50 AND state = 1');
	}

	$stmt->bind_param('i', $user_id);
	$stmt->execute();
	$stmt->bind_result($notification_id);

	$new_notifications = array();
	while ($stmt->fetch())
		array_push($new_notifications, $notification_id);

	$stmt->close();
	$db->close();

	if (count($current_notifications) != count($new_notifications))
	{
		echo 'different size';
		return;
	}

	foreach ($new_notifications as $new_notification)
	{
		if (array_search($new_notification, $current_notifications) === false)
		{
			echo 'not found';
			return;
		}
	}

	echo 'no changes';
?>
