<?php
	require 'connect-db.inc';
	require 'register-in-common-table.inc';

	/// Verify that it's an admin user
	session_start();
	if (! $_SESSION ['user_is_admin'])
	{
		echo '>:(';
		return;
	}

	$db = connect_db();
	$user = json_decode($_POST ['user_data']);

	/// Register job title if it is custom
	if ($user->job_title->id == -1)
		register_in_common_table($db, 'job-title', $user->job_title);

	/// Register nick if it is custom
	if ($user->nick->id == -1)
		register_in_common_table($db, 'nick', $user->nick);

	/// Register user
	$stmt = $db->prepare('INSERT INTO user VALUES (NULL, ?, ?, ?, ?, SHA2(?, 256), ?, ?, ?, ?, ?)');
	$stmt->bind_param('ssssssiiii', $user->rfc, $user->name, $user->father_last_name, $user->mother_last_name, $user->rfc, $user->email,
									$user->is_admin, $user->notifications_by_email, $user->job_title->id, $user->nick->id);

	if (!$stmt->execute())
		echo $db->error;
	else
		echo 'success';

	$stmt->close();
	$db->close();

?>
