var search_users_dialog =
{
	/////////////////////////////////////////
	/// Resets and shows the dialog. Also, sets
	/// the focus to users-reference field
	/////////////////////////////////////////
	show: function()
	{
		conceal("no-found-users-message");
		remove_child_nodes("found-users-section");
		conceal("found-users-section");
		display("search-users-dialog-background");

		var form = document.forms ["search-users-form"];
		form.reset();
		form ["users-reference"].focus();
	},

	/////////////////////////////////////////
	/// Sends the form data to the server.
	/// With the server's response, fills the
	/// founds-users-section.
	/////////////////////////////////////////
	search: function()
	{
		var found_users_section = document.getElementById("found-users-section");
		if (found_users_section.hasChildNodes())
			remove_child_nodes(found_users_section);
		else
			conceal("no-found-users-message");

		var users_reference = document.forms ["search-users-form"] ["users-reference"].value;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState != 4 || this.status != 200)
				return;

			if (this.responseText == ">:(")
			{
				show_toast(this.responseText, DarkToast);
				return;
			}

			var found_users = JSON.parse(this.responseText).found_users;

			if (! found_users.length)
			{
				display("no-found-users-message");
				return;
			}


			display(found_users_section);
			for (var i = 0; i < found_users.length; i++)
			{
				var found_user = found_users [i];
				var found_user_element = document.createElement("div");
				found_user_element.className = "scol12 zebra-row-clickable";
				found_user_element.innerHTML = found_user.full_name + " - " + found_user.rfc;
				found_user_element.setAttribute("onclick", "user_control.set_state(user_control_state.MODIFYING_PROFILE, false, " +
				 											found_user.id + "); close_dialog('search-users');");
				found_users_section.appendChild(found_user_element);
			}
		}
		xmlhttp.open("GET", "php/search-users.php?users_reference=" + users_reference);
		xmlhttp.send();
	}
};
